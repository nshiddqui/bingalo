<?php

use Cake\Core\Configure;

return [
    'HybridAuth' => [
        'providers' => [
            'Google' => [
                'enabled' => true,
                'keys' => [
                    'id' => '869511632481-9rn2rtdgjm3fuvb6lbn8n5nqcuuk6uj2.apps.googleusercontent.com',
                    'secret' => 'GxjgsdZpje_A78phj5BZJHZF'
                ]
            ],
            'Facebook' => [
                'enabled' => true,
                'keys' => [
                    'id' => '132300071391277',
                    'secret' => '09c09b96a11bfc0da6d4b90786a49a1f'
                ],
                'scope' => 'email'
            ],
            'Twitter' => [
                'enabled' => true,
                'keys' => [
                    'key' => 'L0DVT1zigEivAuxFxufQ3KPj4',
                    'secret' => 'XoznP9wEvNu5v0IlrKjgcZmuVH5Cr5NP2qjj5M8Q98WDOOv8HZ'
                ],
                'includeEmail' => true // Only if your app is whitelisted by Twitter Support
            ],
            "Apple" => [
                "enabled" => true,
                "keys" => [
                    "id" => "com.bingalo.service.app",
                    "team_id" => "8Z95EA4YXC",
                    "key_id" => "Q252NLB4Z7",
                    "key_content" => "-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgmxZ1AJA8wvU24uG5gsyjRjsjtPlkLNUrZ7en9EJqjDugCgYIKoZIzj0DAQehRANCAATzLeWLSpqZXDG2LQGpjc8SDWZwoRS8yHzWGxYerp72MzevesB9cAOekvsMftFszk9QhXOCFF5o0CIWyCUo5bvT\n-----END PRIVATE KEY-----"
                ],
                "scope" => "name email",
                "verifyTokenSignature" => true
            ]
        ],
        'debug_mode' => Configure::read('debug'),
        'debug_file' => LOGS . 'hybridauth.log',
    ]
];

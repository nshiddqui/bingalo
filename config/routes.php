<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/*
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    // $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
    //     'httpOnly' => true,
    // ]));

    /*
     * Apply a middleware to the current route scope.
     * Requires middleware to be registered through `Application::routes()` with `registerMiddleware()`
     */
    // $routes->applyMiddleware('csrf');

    /*
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'home']);

    $routes->connect('/search-keyword', ['controller' => 'Pages', 'action' => 'searchKeyword']);

    $routes->connect('/product/*', ['controller' => 'Pages', 'action' => 'product']);

    $routes->connect('/login/*', ['controller' => 'Pages', 'action' => 'login']);

    $routes->connect('/signup/*', ['controller' => 'Pages', 'action' => 'signup']);

    $routes->connect('/logout/*', ['controller' => 'Pages', 'action' => 'logout']);

    $routes->connect('/forgot-password/*', ['controller' => 'Pages', 'action' => 'forgotPassword']);


    $routes->connect('/sell-your-stuff/*', ['controller' => 'Authorized', 'action' => 'sellYourStuff']);

    $routes->connect('/change-password', ['controller' => 'Authorized', 'action' => 'changePassword']);

    $routes->connect('/chat/*', ['controller' => 'Authorized', 'action' => 'chat']);

    $routes->connect('/profile/', ['controller' => 'Authorized', 'action' => 'profile']);

    $routes->connect('/profile/*', ['controller' => 'Pages', 'action' => 'profile']);
    /*
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/view/*', ['controller' => 'Pages', 'action' => 'view']);


    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    $routes->connect('/mark-as-sold/*', ['controller' => 'Authorized', 'action' => 'markAsSold']);

    $routes->connect('/delete-product/*', ['controller' => 'Authorized', 'action' => 'deleteProduct']);
    $routes->connect('/product-bump/*', ['controller' => 'Authorized', 'action' => 'addUpdatedProductBump']);
    $routes->connect('/updateProfile/*', ['controller' => 'Authorized', 'action' => 'updateProfile']);

    $routes->connect(
        '/authenticated',
        ['controller' => 'Pages', 'action' => 'authenticated']
    );

    $routes->connect('/reset/*', ['controller' => 'Pages', 'action' => 'reset']);

    $routes->connect('/payments/paypal/*', ['controller' => 'Pages', 'action' => 'paypal']);

    $routes->connect('/payment-confirmation/paypal/*', ['controller' => 'Pages', 'action' => 'paypalConfirmation']);

    $routes->connect('/payment-canceled/*', ['controller' => 'Pages', 'action' => 'cancleTransaction']);

    $routes->connect('/venmo/*', ['controller' => 'Pages', 'action' => 'venmo']);

    $routes->connect('/card/*', ['controller' => 'Pages', 'action' => 'card']);

    $routes->connect('/gpay/*', ['controller' => 'Pages', 'action' => 'gpay']);

    /*
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *
     * ```
     * $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);
     * $routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);
     * ```
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('admin', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Users', 'action' => 'dashboard']);
    $routes->connect('/dashboard', ['controller' => 'Users', 'action' => 'dashboard']);
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->connect('/api/v1/{controller}', ['action' => 'index', 'api' => 'api', 'apiVersion' => 'v1'], [DashedRoute::class]);
    $routes->connect('/api/v1/{controller}/{action}/*', ['api' => 'api', 'apiVersion' => 'v1'], [DashedRoute::class]);
    $routes->fallbacks(DashedRoute::class);
});
/*
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * Router::scope('/api', function (RouteBuilder $routes) {
 *     // No $routes->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */

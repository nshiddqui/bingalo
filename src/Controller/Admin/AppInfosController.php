<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * AppInfos Controller
 *
 * @property \App\Model\Table\AppInfosTable $AppInfos
 *
 * @method \App\Model\Entity\AppInfo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AppInfosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        $appInfo = $this->AppInfos->get(1, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $appInfo = $this->AppInfos->patchEntity($appInfo, $this->getAppInfoData());
            if ($this->AppInfos->save($appInfo)) {
                $this->Flash->success(__('The app info has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The app info could not be saved. Please, try again.'));
        }
        $this->set(compact('appInfo'));
    }

    public function getAppInfoData() {
        $data = $this->request->getData();
        $this->loadComponent('S3');
        if (!empty($data['logo_image']['tmp_name'])) {
            $data['logo_image'] = $this->S3->upload('logo_image');
        } else {
            unset($data['logo_image']);
        }
        return $data;
    }

}

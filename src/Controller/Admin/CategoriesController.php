<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Routing\Router;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 *
 * @method \App\Model\Entity\Category[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Categories')
            ->queryOptions([
                'contain' => [
                    'SubCategories' => function ($q) {
                        return $q->select(['count' => $q->func()->count('*'), 'category_id'])->group('category_id');
                    }
                ],
                'order' => [
                    'Categories.position'
                ]
            ])
            ->column('Categories.id', ['label' => '#', 'width' => '30px'])
            ->column('Categories.category_image', ['label' => 'Image', 'width' => '80px'])
            ->column('Categories.category_name', ['label' => 'English Category Name'])
            ->column('Categories.herbew_category_name', ['label' => 'Herbew Category Name'])
            ->column('Categories.position', ['label' => 'Category Position'])
            ->column('SubCategories', ['database' => false, 'label' => 'Total Sub Category'])
            ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '150px']);

        $this->DataTables->createConfig('SubCategories')
            ->queryOptions([
                'conditions' => [
                    'SubCategories.category_id' => $this->request->getQuery('category_id')
                ]
            ])
            ->options(['ajax' => ['data' => "%f%function(data){data.category_id = getUrlParameter('category_id');}%f%"]])
            ->column('SubCategories.id', ['label' => '#', 'width' => '30px'])
            ->column('SubCategories.name', ['label' => 'English Category Name'])
            ->column('SubCategories.herbew_name', ['label' => 'Herbew Category Name'])
            ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '150px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Categories);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Categories');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $category = $this->Categories->get($id, [
            'contain' => [],
        ]);

        $this->set('category', $category);
        $this->DataTables->setViewVars('SubCategories');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $category = $this->Categories->newEntity();
        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->getCategoriesData());
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $this->set(compact('category'));
    }

    public function subadd($id = null) {
        if (is_null($id)) {
            $category = $this->Categories->SubCategories->newEntity();
        } else {
            $category = $this->Categories->SubCategories->get($id, [
                'contain' => [],
            ]);
        }
        if ($this->request->is('post') || $this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['category_id'] = $this->request->getQuery('category_id');
            $category = $this->Categories->SubCategories->patchEntity($category, $data);
            if ($this->Categories->SubCategories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'view', $this->request->getQuery('category_id'), '?' => ['category_id' => $this->request->getQuery('category_id')]]);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $this->set(compact('category'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $category = $this->Categories->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->getCategoriesData());
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $this->set(compact('category'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Categories->get($id);
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deletesub($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Categories->SubCategories->get($id);
        if ($this->Categories->SubCategories->delete($category)) {
            $this->Flash->success(__('The sub category has been deleted.'));
        } else {
            $this->Flash->error(__('The sub category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'view', $category->category_id, '?' => ['category_id' => $category->category_id]]);
    }


    public function getCategoriesData() {
        $data = $this->request->getData();
        $this->loadComponent('S3');
        if (!empty($data['category_image']['tmp_name'])) {
            $data['category_image'] = $this->S3->upload('category_image');
        } else {
            unset($data['category_image']);
        }
        return $data;
    }
}

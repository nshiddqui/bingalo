<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Routing\Router;

/**
 * SponsoredLocations Controller
 *
 * @property \App\Model\Table\SponsoredLocationsTable $SponsoredLocations
 *
 * @method \App\Model\Entity\SponsoredLocation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SponsoredLocationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('SponsoredLocations')
            ->queryOptions([
                'order' => ['SponsoredLocations.created_at DESC']
            ])
            ->column('SponsoredLocations.id', ['label' => '#', 'width' => '30px'])
            ->column('SponsoredLocations.image', ['label' => 'Image', 'width' => '130px', 'class' => 'text-center'])
            ->column('SponsoredLocations.location_name', ['label' => 'Location Name'])
            ->column('SponsoredLocations.location_address', ['label' => 'Location Address'])
            ->column('SponsoredLocations.website', ['label' => 'Website'])
            ->column('SponsoredLocations.latitude', ['label' => 'Latitude'])
            ->column('SponsoredLocations.longitude', ['label' => 'Longitude'])
            ->column('SponsoredLocations.created_at', ['label' => 'Created Date'])
            ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '150px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->SponsoredLocations);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('SponsoredLocations');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Sponsored Location id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sponsoredLocation = $this->SponsoredLocations->get($id, [
            'contain' => [],
        ]);

        $this->set('sponsoredLocation', $sponsoredLocation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sponsoredLocation = $this->SponsoredLocations->newEntity();
        if ($this->request->is('post')) {
            $sponsoredLocation = $this->SponsoredLocations->patchEntity($sponsoredLocation, $this->getData());
            if ($this->SponsoredLocations->save($sponsoredLocation)) {
                $this->Flash->success(__('The sponsored location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sponsored location could not be saved. Please, try again.'));
        }
        $this->set(compact('sponsoredLocation'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sponsored Location id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sponsoredLocation = $this->SponsoredLocations->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sponsoredLocation = $this->SponsoredLocations->patchEntity($sponsoredLocation, $this->getData());
            if ($this->SponsoredLocations->save($sponsoredLocation)) {
                $this->Flash->success(__('The sponsored location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sponsored location could not be saved. Please, try again.'));
        }
        $this->set(compact('sponsoredLocation'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sponsored Location id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sponsoredLocation = $this->SponsoredLocations->get($id);
        if ($this->SponsoredLocations->delete($sponsoredLocation)) {
            $this->Flash->success(__('The sponsored location has been deleted.'));
        } else {
            $this->Flash->error(__('The sponsored location could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getData()
    {
        $data = $this->request->getData();
        $this->loadComponent('S3');
        if (!empty($data['image']['tmp_name'])) {
            $data['image'] = $this->S3->upload('image');
        } else {
            unset($data['image']);
        }
        return $data;
    }
}

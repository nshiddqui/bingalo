<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Auth->allow(['logout', 'login']);
        parent::beforeFilter($event);
    }

    public function initialize() {
        parent::initialize();
        $this->Users->hasMany('UserVerification')
                ->setForeignKey('user_id')
                ->setConditions(['verified_status' => 1]);
        $this->Users->hasMany('Product')
                ->setForeignKey('uploaded_by_user_id');
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Users')
                ->queryOptions([
                    'contain' => [
                        'UserVerification' => function ($q) {
                            return $q->select(['verified_by', 'user_id']);
                        },
                        'Product' => function ($q) {
                            return $q->select(['product_location', 'uploaded_by_user_id'])->order(['created_at DESC']);
                        }
                    ]
                ])
                ->databaseColumn('Users.id')
                ->column('Users.image', ['label' => 'Profile Picture', 'width' => '90px', 'class' => 'text-center'])
                ->column('Users.full_name', ['label' => 'Full Name'])
                ->column('Users.email', ['label' => 'Email'])
                ->column('Users.phone', ['label' => 'Mobile Number'])
                ->column('verified_by', ['label' => 'Verified By', 'database' => false, 'width' => '130px',])
                ->column('product_location', ['label' => 'Location', 'database' => false, 'width' => '130px',])
                ->column('actions', ['label' => 'Actions', 'database' => false, 'width' => '130px']);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        if ($this->request->is('api')) {
            $this->viewVars = [];
            $conditions = [];
            if ($this->request->getQuery('q')) {
                $conditions['or'] = [
                    'full_name LIKE' => "%" . $this->request->getQuery('q') . "%"
                ];
            }
            $datas = $this->Users->find('all', ['conditions' => $conditions, 'fields' => [
                    'full_name', 'id'
            ]]);
            $response = ['results' => []];
            foreach ($datas as $data) {
                array_push($response['results'], ['id' => $data->id, 'text' => $data->full_name]);
            }
            $this->set($response);
        } else if ($this->request->is('post')) {
            $data = $this->request->getData();
            $to = $this->Users->get($data['email']);
            $email = new Email('default');
            $email->setFrom(['admin@bingalo.com' => 'Bingalo Admin'])
                    ->setTo($to->email)
                    ->setSubject($data['subject'])
                    ->send($data['message']);
            $this->Flash->success(__('The mail has been sent to user.'));
            return $this->redirect($this->referer());
        } else {
            $this->DataTables->setViewVars('Users');
        }
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $to = $this->Users->get($data['email']);
            $email = new Email('default');
            $email->setFrom(['admin@bingalo.com' => 'Bingalo Admin'])
                    ->setTo($to->email)
                    ->setSubject($data['subject'])
                    ->send($data['message']);
            $this->Flash->success(__('The mail has been sent to user.'));
            return $this->redirect($this->referer());
        }

        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $user = $this->Users->get($id, [
            'contain' => [
                'UserVerification' => function ($q) {
                    return $q->select(['verified_by', 'user_id']);
                },
                'Product' => function ($q) {
                    return $q->select(['product_location', 'uploaded_by_user_id'])->limit(1)->order(['created_at DESC']);
                }
            ],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (empty($data['password'])) {
                unset($data['password']);
            }
            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login() {
        $this->viewBuilder()->setLayout(false);
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['status'] === true) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('This user not activated, please contact our administrator.'));
                }
            } else {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function active($id, $code) {
        if ($this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
        $this->request->allowMethod(['post']);
        $this->loadModel('Users');
        $users = $this->Users->get($id);
        $users->status = $code;
        if ($this->Users->save($users)) {
            $this->Flash->success(__('The user has been ' . ($code == '1' ? 'Activate' : 'Deactivated') . '.'));
        } else {
            $this->Flash->error(__('The user could not be  ' . ($code == '1' ? 'Activate' : 'Deactivated') . '. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function dashboard() {
        $this->loadModel('BumpProducts');
        $this->loadModel('Product');
        $today_users = $this->Users->find('all', [
                    'conditions' => [
                        'DATE(Users.created_at)' => date('Y-m-d')
                    ]
                ])->count();
        $week_users = $this->Users->find('all', [
                    'conditions' => [
                        'YEARWEEK(Users.created_at, 1) = YEARWEEK(CURDATE(), 1)'
                    ]
                ])->count();
        $total_users = $this->Users->find('all')->count();
        $today_products = $this->Product->find('all', [
                    'conditions' => [
                        'DATE(Product.created_at)' => date('Y-m-d')
                    ]
                ])->count();
        $week_products = $this->Product->find('all', [
                    'conditions' => [
                        'YEARWEEK(Product.created_at, 1) = YEARWEEK(CURDATE(), 1)'
                    ]
                ])->count();
        $total_products = $this->Product->find('all')->count();
        $today_bump_products = $this->BumpProducts->find('all', [
                    'conditions' => [
                        'DATE(BumpProducts.created_at)' => date('Y-m-d')
                    ]
                ])->count();
        $week_bump_products = $this->BumpProducts->find('all', [
                    'conditions' => [
                        'YEARWEEK(BumpProducts.created_at, 1) = YEARWEEK(CURDATE(), 1)'
                    ]
                ])->count();
        $total_bump_products = $this->BumpProducts->find('all')->count();
        $this->set(compact('today_users', 'week_users', 'total_users', 'today_products', 'week_products', 'total_products', 'today_bump_products', 'week_bump_products', 'total_bump_products'));
    }

}

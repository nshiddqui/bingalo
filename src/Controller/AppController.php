<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Http\Client;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Cache\Cache;
use Cake\I18n\I18n;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $privateCredential = [];
    public $publicCredential = [
        "apiKey" => "AIzaSyDoyjxMTlmfCnQvfEU1mcIbPjiZDxdYLRo",
        "authDomain" => "bingalo.firebaseapp.com",
        "databaseURL" => "https://bingalo.firebaseio.com",
        "projectId" => "bingalo",
        "storageBucket" => "bingalo.appspot.com",
        "messagingSenderId" => "869511632481",
        "appId" => "1:869511632481:web:372df613d3b6a0c9427c20"
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('Captcha');

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        $this->loadComponent('Auth', [
            'loginAction' => '/',
            'authError' => 'Did you really think you are allowed to see that?',
            'authenticate' => [
                'Form' => ['userModel' => 'Admins', 'fields' => ['username' => 'email']],
                'HybridAuth'
            ],
            'storage' => 'Session'
        ]);

        $this->loadComponent('Flash');



        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeFilter(Event $event) {
        if ($this->request->is('mobile') || isset($_COOKIE['mobile_user'])) {
            return $this->redirect('https://m.bingalo.com');
        }
        $this->request->addDetector('api', ['param' => 'api', 'value' => 'api']);
        $this->loadModel('Pages');
        if ($this->request->is('api')) {
            $this->loadComponent('Api');
        }
        if ($auth = $this->Auth->user()) {
            $this->set('authUser', $auth);
        } else {
            $this->loadComponent('Cookie');
            $details =  $this->Cookie->read('RememberMe');
            if (!empty($details)) {
                $this->loadModel('Curl');
                $user = $this->Curl->login($details);
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->referer());
                }
            }
        }
        if ($this->request->getParam('prefix') === 'admin') {
            $this->loadAdmin();
            $this->set('page_lists', $this->Pages->find('list'));
        } else if ($this->Auth->user('role') == '1') {
            $this->redirect('/admin/');
        } else {
            $this->set('page_status', $this->Pages->find('list', ['conditions' => ['status' => 1]])->toArray());
            $this->loadModel('AppInfos');
            $this->set('app_info_data', $this->AppInfos->get(1));
            $this->set('localInformation', $this->getLatLng());
        }
        $this->set('googleApiKey', 'AIzaSyDaMTjANiWv7gcYvQR6fh1jClrHbZ9wdxE');
        $this->set('firebaseSetup', $this->publicCredential);
        $this->set('is_mobile', $this->request->is('mobile'));
        # We check if we have a language set
        if (isset($_COOKIE['lang']) && !empty($_COOKIE['lang'])) {
            I18n::setLocale($_COOKIE['lang']);
        } else {
            # If we don't have one, we will set the default one (in my case it's English)
            I18n::setLocale('en');
        }
        parent::beforeFilter($event);
    }

    protected function getLatLng() {
        $ip = $this->request->clientIp();
        if ($ip === '::1') {
            $ip = '103.232.113.107';
        }
        $session = $this->getRequest()->getSession();
        if (($localInformation = $session->read('localInformation')) && !empty($localInformation)) {
            return $localInformation;
        }
        $http = new Client();
        if (isset($_COOKIE['Location']) && !empty($latLng = $_COOKIE['Location'])) {
            $response = $http->get(
                'https://maps.google.com/maps/api/geocode/json',
                [
                    'latlng' => $latLng,
                    'key' => 'AIzaSyCehnlNlZBui59vgOGNYN8XoN5rjYE9HJE'
                ]
            );
            $location = $response->getJson();
            if (isset($location['results']) && !empty($location['results'])) {
                $data = $location['results'][0];
                $address = [];
                foreach ($data['address_components'] as $address_components) {
                    $address[$address_components['types'][0]] = [
                        'short_name' => $address_components['short_name'],
                        'long_name' => $address_components['long_name']
                    ];
                }
                $localInformation = [
                    'ip' => $ip,
                    'type' => 'ipv4',
                    'continent_code' => '',
                    'continent_name' => '',
                    'country_code' => $address['country']['short_name'],
                    'country_name' => $address['country']['long_name'],
                    'region_code' => $address['administrative_area_level_1']['short_name'],
                    'region_name' => $address['administrative_area_level_1']['long_name'],
                    'city' => $address['locality']['long_name'],
                    'zip' => $address['postal_code']['long_name'],
                    'latitude' => explode(',', $latLng)[0],
                    'longitude' =>  explode(',', $latLng)[1],
                    'location' => [
                        'geoname_id' => '',
                        'capital' => $address['locality']['long_name'],
                        'languages' => []
                    ],
                    'country_flag' => '',
                    'country_flag_emoji' => '',
                    'country_flag_emoji_unicode' => '',
                    'calling_code' => '',
                    'full_address' => $data['formatted_address'],
                    'is_eu' => ''
                ];
                $session->write('localInformation', $localInformation);
                return $localInformation;
            }
        }
        if (($localInformation = Cache::read('localInformation' . $ip)) === false) {
            $response = $http->get(
                'http://api.ipstack.com/' . $ip,
                [
                    'access_key' => 'c6fe2de8508525ee325a4997a7d8a4c0',
                    'format' => '1'
                ]
            );
            $localInformation = $response->getJson();
            Cache::write('localInformation' . $ip, $localInformation);
        }
        return $localInformation;
    }

    private function loadAdmin() {
        $this->Auth->setConfig('loginAction', [
            'controller' => 'Users',
            'action' => 'login',
            'prefix' => 'admin'
        ]);
        $this->Auth->setConfig('loginRedirect', [
            'controller' => 'Users',
            'action' => 'index',
            'prefix' => 'admin'
        ]);
        $this->viewBuilder()->setLayout('admin');
        if ($this->Auth->user() && $this->Auth->user('role') != '1') {
            throw new UnauthorizedException(__('You are not alowed to access this page'));
        }
    }
}

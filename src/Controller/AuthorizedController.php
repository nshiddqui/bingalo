<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Routing\Router;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 */
class AuthorizedController extends AppController
{

    public function sellYourStuff($id = null)
    {
        $this->loadModel('Curl');
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $category = explode('#', $data['product_category']);
            if (!empty($id)) {
                $data['product_id'] = $id;
            }
            $data['product_category'] = $category[0];
            $data['product_price'] = str_replace(',', '', $data['product_price']);
            $data['parent_category'] = $category[1];
            $data['uploaded_by_user_id'] = $this->Auth->user('id');
            $data['token'] = $this->Auth->user('token');
            $data['product_is_sold'] = 0;
            $imagesFile = $data['image'];
            $data['image'] = [];
            if (!empty($imagesFile)) {
                foreach ($imagesFile as $images) {
                    if (move_uploaded_file($images['tmp_name'], WWW_ROOT . $images['name'])) {
                        $data['image'][] = fopen(WWW_ROOT . $images['name'], 'r');
                    }
                }
            }
            if ($this->Curl->sellYourStuff($data)) {
                if (!empty($id)) {
                    $this->Flash->success(__('Your product updated successful'));
                } else {
                    $this->Flash->success(__('Sell your stuff successful'));
                }
            } else {
                $this->Flash->error(__('Your session is expired.'));
                return $this->redirect('/logout');
            }
            return $this->redirect('/');
        }
        $this->loadModel('Categories');
        $this->set('categories', $this->Categories->getCategories());
        $this->set('model_categories', $this->Categories->find('all', ['contain' => ['SubCategories']])->toArray());
        if (!empty($id)) {
            $data = $this->Curl->geProductDetails($id);
            $this->set(compact('data'));
        }
    }

    public function changeProfilePic()
    {
        $this->loadModel('Curl');
        $data = $this->request->getData();
        $imagesFile = $data['image'];
        $data['user_id'] = $this->Auth->user('id');
        if (!empty($imagesFile)) {
            if (move_uploaded_file($imagesFile['tmp_name'], WWW_ROOT . $imagesFile['name'])) {
                $data['image'] = fopen(WWW_ROOT . $imagesFile['name'], 'r');
            }
        }
        $sell_products = $this->Curl->profile_pic($data);
        $this->redirect($this->referer());
    }

    public function deleteImage($id)
    {
        $this->loadModel('Curl');
        $sell_products = $this->Curl->deleteimage($id);
        $this->redirect($this->referer());
    }

    public function chat($product_id = null)
    {
        if (!empty($product_id)) {
            $this->loadModel('Curl');
            $data = $this->Curl->geProductDetails($product_id);
            $this->set(compact('data'));
        }
    }

    public function changePassword()
    {
        $this->loadModel('Curl');
        $response = $this->Curl->changePassword($this->request->getData(), $this->Auth->user('id'));
        $this->Flash->success(__($response));
        $this->redirect($this->referer());
    }

    public function profile()
    {
        $this->loadModel('Curl');
        $this->loadModel('UserVerification');
        $sell_products = $this->Curl->getProductsByUserId($this->Auth->user('id'), 0);
        $sold_products = $this->Curl->getProductsByUserId($this->Auth->user('id'), 1);
        $favorite_products = $this->Curl->getProductsByUserId($this->Auth->user('id'), 4);
        $user_verfy = $this->UserVerification->find('all', ['conditions' => ['user_id' => $this->Auth->user('id')]]);
        $this->set(compact('sell_products', 'sold_products', 'user_verfy', 'favorite_products'));
    }

    public function markAsSold($id)
    {
        $this->loadModel('Curl');
        $sell_products = $this->Curl->chageProductIsSoldStatus($id);
        $this->redirect($this->referer());
    }

    public function deleteProduct($id)
    {
        $this->loadModel('Curl');
        $sell_products = $this->Curl->deleteProduct($id);
        $this->redirect('/');
    }

    public function addUpdatedProductBump($id)
    {
        $this->loadModel('Curl');
        $sell_products = $this->Curl->addUpdatedProductBump($id, $this->Auth->user('id'));
        $this->redirect($this->referer());
    }

    public function updateProfile()
    {
        $this->request->allowMethod(['post']);
        $this->loadModel('Curl');
        $data = $this->request->getData();
        $latlng = $this->getLatLng();
        $data['user_id'] = $this->Auth->user('id');
        $data['lat'] = $latlng['latitude'];
        $data['lng'] = $latlng['longitude'];
        $data['token'] = $this->Auth->user('token');
        if (!empty($data['image'])) {
            if (move_uploaded_file($data['image']['tmp_name'], WWW_ROOT . $data['image']['name'])) {
                $data['image'] = fopen(WWW_ROOT . $data['image']['name'], 'r');
            }
        }
        $update_profile = $this->Curl->updateProfile($data);
        if ($update_profile) {
            $this->Auth->setUser($update_profile);
            $this->Flash->success(__('Profile updated successful.'));
        } else {
            $this->Flash->error(__('Unable to update your profile.'));
        }
        $this->redirect($this->referer());
    }

    public function favourite($id)
    {
        $this->loadModel('Favourite');
        $data = $this->Favourite->find('all', ['conditions' => ['user_id' => $this->Auth->user('id'), 'product_id' => $id]]);
        if ($data->count()) {
            $this->Favourite->delete($data->first());
            $this->Flash->error(__('Product has been removed from favourite'));
        } else {
            $favourite = $this->Favourite->newEntity();
            $favourite = $this->Favourite->patchEntity($favourite, ['user_id' => $this->Auth->user('id'), 'product_id' => $id]);
            $this->Favourite->save($favourite);
            $this->Flash->success(__('Product has been added on favourite'));
        }
        return $this->redirect($this->referer());
    }

    public function authenticateFacebook()
    {
        Configure::load('hybridauth');
        $config = Configure::read('HybridAuth');
        $config['callback'] = Router::url(['controller' => 'Authorized', 'action' => 'authenticateFacebook', '_ssl' => true], true);
        $oauth = new \Hybridauth\Hybridauth($config);
        $adapter = $oauth->authenticate('Facebook');

        $user_profile = $adapter->getUserProfile();
        if (!empty($user_profile)) {
            $this->loadModel('UserVerification');
            $exists =  $this->UserVerification->find('all', [
                'conditions' => [
                    'user_id' => $this->Auth->user('id'),
                    'verified_by' => 'facebook'
                ]
            ]);
            if ($exists->count()) {
                $existData = $exists->first();
                $verification = $this->UserVerification->patchEntity($existData, [
                    'verified_status' => '1'
                ]);
                $this->UserVerification->save($verification);
            } else {
                $verification = $this->UserVerification->newEntity();
                $verification = $this->UserVerification->patchEntity($verification, [
                    'user_id' => $this->Auth->user('id'),
                    'verified_by' => 'facebook',
                    'verified_status' => '1',
                    'created_at' => date('Y-m-d h:i:s')
                ]);
                $this->UserVerification->save($verification);
            }
        }
        $this->redirect('/profile');
    }
    public function authenticateGoogle()
    {
        Configure::load('hybridauth');
        $config = Configure::read('HybridAuth');
        $config['callback'] = Router::url(['controller' => 'Authorized', 'action' => 'authenticateGoogle', '_ssl' => true], true);
        $oauth = new \Hybridauth\Hybridauth($config);
        $adapter = $oauth->authenticate('Google');

        $user_profile = $adapter->getUserProfile();
        if (!empty($user_profile)) {
            $this->loadModel('UserVerification');
            $exists =  $this->UserVerification->find('all', [
                'conditions' => [
                    'user_id' => $this->Auth->user('id'),
                    'verified_by' => 'google'
                ]
            ]);
            if ($exists->count()) {
                $existData = $exists->first();
                $verification = $this->UserVerification->patchEntity($existData, [
                    'verified_status' => '1'
                ]);
                $this->UserVerification->save($verification);
            } else {
                $verification = $this->UserVerification->newEntity();
                $verification = $this->UserVerification->patchEntity($verification, [
                    'user_id' => $this->Auth->user('id'),
                    'verified_by' => 'google',
                    'verified_status' => '1',
                    'created_at' => date('Y-m-d h:i:s')
                ]);
                $this->UserVerification->save($verification);
            }
        }
        $this->redirect('/profile');
    }

    public function authenticateTwitter()
    {
        Configure::load('hybridauth');
        $config = Configure::read('HybridAuth');

        $config['callback'] = Router::url(['controller' => 'Authorized', 'action' => 'authenticateTwitter', '_ssl' => true], true);

        $oauth = new \Hybridauth\Hybridauth($config);
        $adapter = $oauth->authenticate('Twitter');

        $user_profile = $adapter->getUserProfile();
        if (!empty($user_profile)) {
            $this->loadModel('UserVerification');
            $exists =  $this->UserVerification->find('all', [
                'conditions' => [
                    'user_id' => $this->Auth->user('id'),
                    'verified_by' => 'twitter'
                ]
            ]);
            if ($exists->count()) {
                $existData = $exists->first();
                $verification = $this->UserVerification->patchEntity($existData, [
                    'verified_status' => '1'
                ]);
                $this->UserVerification->save($verification);
            } else {
                $verification = $this->UserVerification->newEntity();
                $verification = $this->UserVerification->patchEntity($verification, [
                    'user_id' => $this->Auth->user('id'),
                    'verified_by' => 'twitter',
                    'verified_status' => '1',
                    'created_at' => date('Y-m-d h:i:s')
                ]);
                $this->UserVerification->save($verification);
            }
        }
        $this->redirect('/profile');
    }

    public function sendEmailOTP()
    {
        $this->loadModel('UserVerification');
        $this->loadModel('Curl');
        $this->Curl->updateVerification([
            'user_id' => $this->Auth->user('id'),
            'email_id' => $this->request->getQuery('email'),
            'verify_method' => $this->request->getQuery('verify_method')
        ]);
        die;
    }

    public function verfyOtp($verify_method)
    {
        $this->loadModel('UserVerification');
        $data = $this->UserVerification->find('all', ['conditions' => [
            'user_id' => $this->Auth->user('id'),
            'verify_method' => $verify_method
        ]]);
        if ($data->count()) {
            $existData = $data->first();
            if ($existData->otp == $this->request->getData('otp')) {
                $verification = $this->UserVerification->patchEntity($existData, [
                    'verified_status' => '1'
                ]);
                $this->UserVerification->save($verification);
            }
        }
        $this->redirect('/profile');
    }

    public function sendOTP($email)
    {
        $this->loadModel('Users');
        $existUser = $this->Users->find('all', ['conditions' => [
            'email' => $email
        ]]);
        if ($existUser->count()) {
            echo 0;
        } else {
            $this->loadModel('Curl');
            $this->Curl->sendOTP($email);
            echo 1;
        }
        die;
    }

    public function changeEmail()
    {
        $this->loadModel('UserOtp');
        $data = $this->UserOtp->find('all', ['conditions' => [
            'send_to' => $this->request->getData('change_emails'),
            'otp' => $this->request->getData('change_email_otp')
        ]]);
        if ($data->count()) {
            $this->loadModel('Users');
            $user = $this->Users->get($this->Auth->user('id'));
            $user->email = $this->request->getData('change_emails');
            $this->Users->save($user);
            $authUser = $this->Auth->user();
            $authUser['email'] = $this->request->getData('change_emails');
            $this->Auth->setUser($authUser);
            $this->Flash->error(__('Your email has been changed.'));
        } else {
            $this->Flash->error(__('Invalid OTP.'));
        }
        $this->redirect('/profile');
    }
}

<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\Email;

class NotifyComponent extends Component
{
    public function initialize(array $config)
    {
        $this->Controller = $this->_registry->getController();
        parent::initialize($config);
    }

    public function sendWelcomeEmail($user)
    {
        if (is_numeric($user)) {
            $this->Controller->loadModel('Users');
            $user = $this->Controller->Users->get($user);
        }
        if (empty($user->email)) {
            return false;
        }
        $email = new Email('default');
        $email->setFrom("no-reply@mail.bingalo.com", "Bingalo");
        $email->setSubject("Welcome to Bingalo");
        $email->viewBuilder()->setTemplate('welcome');
        $email->setEmailFormat('html');
        $email->setViewVars(compact('user'));
        $email->setTo($user->email);
        $email->send();
        return true;
    }

    public function sendResetPasswordEmail($user)
    {
        if (is_numeric($user)) {
            $this->Controller->loadModel('Users');
            $user = $this->Controller->Users->get($user);
        }
        if (empty($user->email)) {
            return false;
        }
        $email = new Email('default');
        $email->setFrom("no-reply@mail.bingalo.com", "Bingalo");
        $email->setSubject("Bingalo Password Reset");
        $email->viewBuilder()->setTemplate('reset_password');
        $email->setEmailFormat('html');
        $email->setViewVars(compact('user'));
        $email->setTo($user->email);
        $email->send();
        return true;
    }
}

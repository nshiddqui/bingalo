<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Braintree;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found
     * @throws \Cake\View\Exception\MissingTemplateException In debug mode.
     */
    public function display(...$path)
    {
        if (!$path) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function home()
    {
        $filterData = $this->request->getQueryParams();
        $product = false;
        $filterdCategories = [];
        $this->loadModel('Curl');
        $this->loadModel('Ads');
        $this->loadModel('TopCities');
        $top_cities = $this->TopCities->find('all',[
            'order' => [
                'position'
            ]
        ]);
        $session = $this->getRequest()->getSession();
        $this->set('categories', $this->Curl->getCategories());
        if ($this->request->getQuery('page')) {
            $this->Curl->offset = ($this->request->getQuery('page') - 1) * $this->Curl->limit;
        }
        if ($this->request->getQuery('parent_category')) {
            $filterdCategories = explode(',', $this->request->getQuery('parent_category'));
            $filterData['parent_category'] = $this->request->getQuery('parent_category');
        }
        if ($this->request->getQuery('name')) {
            $filterData['name'] = $this->request->getQuery('name');
            if (env('HTTP_CACHE_CONTROL') !== 'max-age=0') {
                $this->Curl->updateTrendingRank($filterData['name']);
            }
            $searchRecentWise = array_slice((array) $session->read('searchRecentWise'), 0, 5);
            array_push($searchRecentWise, $filterData['name']);
            $session->write('searchRecentWise', array_unique($searchRecentWise));
        }
        if (empty($this->request->getQuery('max_distance'))) {
            $filterData['max_distance'] = 10;
        }
        if (empty($this->request->getQuery('address'))) {
            $filterData['address'] = $this->getLatLng()['city'] . ' ' . $this->getLatLng()['zip'];
        }
        $filterData['lat'] = $this->request->getQuery('product_search_lat');
        $filterData['lng'] = $this->request->getQuery('product_search_lng');
        if (!empty($filterData)) {
            $product = $this->Curl->filterProduct($filterData);
            if (isset($filterData['parent_category'])) {
                unset($filterData['parent_category']);
            }
        } else {
            $product = $this->Curl->searchNearbyProducts($this->getLatLng());
        }
        $this->set('products', $product);
        if ($this->request->is('ajax')) {
            if ($this->request->is('mobile')) {
                $this->render('/element/filtered_product_mobile');
            } else {
                $this->render('/element/filtered_product');
            }
        }
        $topBanner = $this->Ads->find('all', [
            'conditions' => [
                'NOW() BETWEEN start_Date AND end_date',
                'position' => '3'
            ]
        ]);
        $bottomBanner = $this->Ads->find('all', [
            'conditions' => [
                'NOW() BETWEEN start_Date AND end_date',
                'position' => '2'
            ]
        ]);
        $this->set('top_cities', $top_cities);
        $this->set('filterdCategories', $filterdCategories);
        $this->set('filter', $filterData);
        $this->set('searchRankWise', $this->Curl->getSearchRankItem());
        $this->set('searchRecentWise', (array) $session->read('searchRecentWise'));
        $this->set('topListings', $this->Curl->getTopListings());
        $this->set('topBanner', $topBanner);
        $this->set('bottomBanner', $bottomBanner);
        if ($this->request->is('mobile')) {
            return $this->render('/Pages/mobile', false);
        }
    }

    public function searchKeyword()
    {
        $searchTerm = trim($this->request->getQuery('term'));
        $data = [];
        if ($searchTerm !== '') {
            $this->loadModel('Curl');
            $searchKeywords = $this->Curl->searchKeyword($searchTerm, $this->getLatLng());
            foreach ($searchKeywords as $searchKeyword) {
                array_push($data, [
                    'label' => $searchKeyword['product_title'],
                    'value' => $searchKeyword['product_title']
                ]);
            }
        }
        echo json_encode($data);
        die;
    }

    public function product($id)
    {
        $this->loadModel('Curl');
        $this->loadModel('UserVerification');
        $data = $this->Curl->geProductDetails($id);
        $products = $this->Curl->getProductsByUserId($data['uploaded_by_user_id']['id']);
        if ($this->Auth->user('id')) {
            $this->loadModel('Favourite');
            $this->set('is_fav', $this->Favourite->exists(['user_id' => $this->Auth->user('id'), 'product_id' => $data['id']]));
        } else {
            $this->set('is_fav', false);
        }
        // if (!isset($data['images'][0]['image_url']) || empty($data['images'][0]['image_url'])) {
        //     $this->Flash->error('Product Not Found');
        //     return $this->redirect('/');
        // }
        $user_verfy = $this->UserVerification->find('all', ['conditions' => ['user_id' => $data['uploaded_by_user_id']['id']]]);
        $this->set(compact('data', 'products', 'user_verfy'));
    }

    public function login()
    {
        $this->request->allowMethod(['post']);
        $this->loadModel('Curl');
        $data = $this->request->getData();
        $data['email'] = strtolower($data['email']);
        $user = $this->Curl->login($this->request->getData());
        if ($user) {
            $this->_setCookie();
            $this->Auth->setUser($user);
            $this->Flash->success(__('Login successful'));
        } else {
            $this->Flash->error(__('Invalid email & password'));
        }
        $this->redirect($this->referer());
    }

    public function signup()
    {
        $this->request->allowMethod(['post']);
        $this->loadModel('Curl');
        $latlng = $this->getLatLng();
        $data = $this->request->getData();
        $data['lat'] = $latlng['latitude'];
        $data['lng'] = $latlng['longitude'];
        $data['phone'] = '';
        if (!empty($data['image'])) {
            if (move_uploaded_file($data['image']['tmp_name'], WWW_ROOT . $data['image']['name'])) {
                $data['image'] = fopen(WWW_ROOT . $data['image']['name'], 'r');
            }
        }
        $this->Curl->signup($data);
        $user = $this->Curl->login([
            'email' => $data['email'],
            'password' => $data['password']
        ]);
        if ($user) {
            $this->_setCookie();
            $this->Auth->setUser($user);
            $this->Flash->success(__('Login successful'));
        }
        $this->redirect($this->referer());
    }

    public function logout()
    {
        $this->loadComponent('Cookie');
        $this->Cookie->delete('RememberMe');
        $this->redirect($this->Auth->logout());
    }

    public function profile($id)
    {
        $this->loadModel('Curl');
        $this->loadModel('UserVerification');
        $sell_products = $this->Curl->getProductsByUserId($id, 0);
        $sold_products = $this->Curl->getProductsByUserId($id, 1);
        $favorite_products = $this->Curl->getProductsByUserId($id, 4);
        $auth_user = $this->Curl->getUserProfile($id);
        $user_verfy = $this->UserVerification->find('all', ['conditions' => ['user_id' => $id]]);
        $this->set(compact('sell_products', 'sold_products', 'auth_user', 'user_verfy', 'favorite_products'));
        $this->render('/Authorized/profile');
    }

    public function authenticated()
    {
        $user = $this->Auth->identify();
        if ($user) {
            $loginType = ['Facebook' => '0', 'Google' => '1', 'Twitter' => '2', 'Apple' => '3'];
            $this->loadModel('Curl');
            $latlng = $this->getLatLng();
            $user = $this->Curl->socialLogin([
                'full_name' => $user->displayName,
                'email' => $user->email,
                'social_id' => $user->identifier,
                'photo_link' => $user->photoURL,
                'social_login_type' => $loginType[Configure::read('provider')],
                'lat' => $latlng['latitude'],
                'lng' => $latlng['longitude'],
            ]);
            $this->Auth->setUser($user);
            $this->Flash->success(__('Login successful'));

            return $this->redirect($this->Auth->redirectUrl());
        } else {
            $this->Flash->error(__('Invalid failed'));
        }

        return $this->redirect('/');
    }

    protected function _setCookie()
    {
        if (!$this->request->getData('remember_me')) {
            return false;
        }
        $this->loadComponent('Cookie');
        $data = [
            'email' => $this->request->getData('email'),
            'password' => $this->request->getData('password')
        ];
        $this->Cookie->write('RememberMe', $data, true, '+1 year');
        return true;
    }

    public function forgotPassword()
    {
        $this->request->allowMethod(['post']);
        $email = $this->request->getData('email');
        if (!empty($email)) {
            $this->loadModel('Users');
            $user = $this->Users->find('all', [
                'conditions' => [
                    'email' => $this->request->getData('email')
                ]
            ]);
            if ($user->count()) {
                $userData = $user->first();
                $userData->enc_id = base64_encode(base64_encode(base64_encode($userData->id)));
                $this->loadComponent('Notify');
                $this->Notify->sendResetPasswordEmail($userData);
                $this->Flash->success(__('Please Check email for further instructions'));
            } else {
                $this->Flash->error(__('Provided email does not exist.'));
            }
        }
        $this->redirect($this->referer());
    }


    public function view($id = null)
    {
        // $this->viewBuilder()->setLayout(false);
        if ($id == 1) {
            $this->render('faq');
        } else {
            $page = $this->Pages->get($id, [
                'contain' => [],
            ]);
            $this->set(compact('page', 'id'));
        }
    }

    public function checkEmail($email)
    {
        $this->loadModel('Users');
        $existUser = $this->Users->find('all', ['conditions' => [
            'email' => $email
        ]]);
        if ($existUser->count()) {
            echo 0;
        } else {
            echo 1;
        }
        die;
    }

    public function reset($id)
    {
        $this->viewBuilder()->setLayout(false);
        $id = base64_decode(base64_decode(base64_decode($id)));
        if ($id === false) {
            $this->redirect('/');
        }
        $this->loadModel('Users');
        $user = $this->Users->get($id);
        if ($this->request->is('post')) {
            $user->password = md5($this->request->getData('password'));
            $this->Users->save($user);
            $this->Flash->success(__('Your password has been changes, please login with new password.'));
            $this->redirect('/');
        }
    }

    public function paypal($amount, $product_id)
    {
        if (!in_array($amount, ['20', '35', '50'])) {
            $this->Flash->error(__('Invalid amount.'));
            $this->redirect($this->referer());
        }
        $this->loadComponent('Paypal');
        try {
            $order = [
                'product_id' => $product_id,
                'description' => 'Bingalo Bump Product Subscription',
                'currency' => 'USD',
                'return' => Router::url('/payment-confirmation/paypal/', true),
                'cancel' => Router::url('/payment-canceled', true),
                'shipping' => $amount,
                'items' => [
                    [
                        'name' => 'Bingalo Package',
                        'description' => 'Bingalo Bump Product Subscription',
                        'tax' => 0.00,
                        'subtotal' => 0.00,
                        'qty' => 1,
                    ]
                ]
            ];
            $response = $this->Paypal->setExpressCheckout($order);
            $this->request->getSession()->write('checkout_details', $order);
            $this->redirect($response);
        } catch (\Exception $e) {
            $this->Flash->error($e->getMessage());
        }
        $this->redirect($this->referer());
    }

    public function cancleTransaction()
    {
        $this->Flash->success(__('Payment Cancelled'));
        $this->redirect($this->referer());
    }

    public function paypalConfirmation()
    {
        $days = [
            '20.00' => '3',
            '35.00' => '7',
            '50.00' => '15'
        ];
        $this->loadComponent('Paypal');
        $this->loadModel('Payments');
        $this->loadModel('Product');
        $this->loadModel('BumpProducts');
        try {
            $order = $this->request->getSession()->read('checkout_details');
            $response = $this->Paypal->doExpressCheckoutPayment($order, $this->request->getQuery('token'), $this->request->getQuery('PayerID'));
            $payment = $this->Payments->newEntity([
                'txnid' => $response['PAYMENTINFO_0_TRANSACTIONID'],
                'payment_amount' => $response['PAYMENTINFO_0_AMT'],
                'payment_status' => $response['PAYMENTINFO_0_PAYMENTSTATUS'],
                'itemid' => $order['product_id']
            ]);
            $this->Payments->save($payment);
            $product = $this->Product->get($order['product_id']);
            $product->bump_data = 1;
            $this->Product->save($product);
            $bump_data = $this->BumpProducts->newEntity([
                'product_id' => $order['product_id'],
                'bump_date' => date('Y-m-d h:i:s'),
                'bump_days' => $days[$response['PAYMENTINFO_0_AMT']],
                'bump_user_id' => $this->Auth->user('id'),
                'bump_price' => $response['PAYMENTINFO_0_AMT'],
                'payment_mode' => 'Paypal'
            ]);
            $this->BumpProducts->save($bump_data);
            $this->Flash->success(__('Product bump successfully'));
            $this->request->getSession()->delete('checkout_details');
        } catch (\App\Controller\Component\PaypalRedirectException $e) {
            $this->redirect($e->getMessage());
        } catch (\Exception $e) {
            $this->Flash->error($e->getMessage());
        }
        return $this->redirect('/');
    }

    public function venmo()
    {
        $days = [
            '20' => '3',
            '35' => '7',
            '50' => '15'
        ];
        $this->loadComponent('Paypal');
        $this->loadModel('Payments');
        $this->loadModel('Product');
        $this->loadModel('BumpProducts');
        $gateway = new Braintree\Gateway([
            'environment' => 'sandbox',
            'merchantId' => '8qvhbp66n65srzv5',
            'publicKey' => '6s33t5mnp54hbfz3',
            'privateKey' => '51899b14e549990066390248be3d61bc'
        ]);

        $result_venmo = $gateway->transaction()->sale([
            'amount' => $this->request->getQuery('amount'),
            'paymentMethodNonce' => $this->request->getQuery('payerID'),
            'options' => [
                'submitForSettlement' => true,
                'venmo' => [
                    'profileId' => ''
                ]
            ],
            'deviceData' => urldecode($this->request->getQuery('deviceData'))
        ]);
        // $payment = $this->Payments->newEntity([
        //     'txnid' => $result_venmo->transaction->id,
        //     'payment_amount' => $this->request->getQuery('amount'),
        //     'payment_status' => 'paid',
        //     'itemid' => $order['product_id']
        // ]);
        // $this->Payments->save($payment);
        // $product = $this->Product->get($order['product_id']);
        // $product->bump_data = 1;
        // $this->Product->save($product);
        // $bump_data = $this->BumpProducts->newEntity([
        //     'product_id' => $order['product_id'],
        //     'bump_date' => date('Y-m-d h:i:s'),
        //     'bump_days' => $days[$response['PAYMENTINFO_0_AMT']],
        //     'bump_user_id' => $this->Auth->user('id'),
        //     'bump_price' => $response['PAYMENTINFO_0_AMT'],
        //     'payment_mode' => 'Paypal'
        // ]);
        // $this->BumpProducts->save($bump_data);
        $this->Flash->success(__('Payment done successfully'));
        $this->redirect('/');
    }

    public function card()
    {
        $this->loadModel('Payments');
        $this->loadModel('Product');
        $this->loadModel('BumpProducts');
        $days = [
            '20' => '3',
            '35' => '7',
            '50' => '15'
        ];
        $gateway = new Braintree\Gateway([
            'environment' => 'sandbox',
            'merchantId' => '8qvhbp66n65srzv5',
            'publicKey' => '6s33t5mnp54hbfz3',
            'privateKey' => '51899b14e549990066390248be3d61bc'
        ]);

        $result = $gateway->transaction()->sale([
            'amount' => $this->request->getQuery('amount'),
            'paymentMethodNonce' => $this->request->getQuery('payerID'),
            'options' => [
                'submitForSettlement' => true
            ],
            'deviceData' => []
        ]);

        if ($result->success) {
            $payment = $this->Payments->newEntity([
                'txnid' => $result->transaction->id,
                'payment_amount' => $this->request->getQuery('amount'),
                'payment_status' => 'paid',
                'itemid' => $this->request->getQuery('productId')
            ]);
            $this->Payments->save($payment);
            $product = $this->Product->get($this->request->getQuery('productId'));
            $product->bump_data = 1;
            $this->Product->save($product);
            $bump_data = $this->BumpProducts->newEntity([
                'product_id' => $this->request->getQuery('productId'),
                'bump_date' => date('Y-m-d h:i:s'),
                'bump_days' => $days[$this->request->getQuery('amount')],
                'bump_user_id' => $this->Auth->user('id'),
                'bump_price' => $this->request->getQuery('amount'),
                'payment_mode' => 'Card'
            ]);
            $this->BumpProducts->save($bump_data);
            $this->Flash->success(__('Product bump successfully'));
        } else if ($result->transaction) {
            $this->Flash->success(__($result->transaction->processorResponseText));
        } else {
            foreach ($result->errors->deepAll() as $error) {
                $this->Flash->success(__($error->message));
            }
        }
        $this->redirect('/');
    }

    public function gpay()
    {
        $this->loadModel('Payments');
        $this->loadModel('Product');
        $this->loadModel('BumpProducts');
        $days = [
            '20' => '3',
            '35' => '7',
            '50' => '15'
        ];
        $gateway = new Braintree\Gateway([
            'environment' => 'sandbox',
            'merchantId' => '8qvhbp66n65srzv5',
            'publicKey' => '6s33t5mnp54hbfz3',
            'privateKey' => '51899b14e549990066390248be3d61bc'
        ]);

        $result = $gateway->transaction()->sale([
            'amount' => $this->request->getQuery('amount'),
            'paymentMethodNonce' => $this->request->getQuery('payerID'),
            'options' => [
                'submitForSettlement' => true
            ],
            'deviceData' => []
        ]);

        if ($result->success) {
            $payment = $this->Payments->newEntity([
                'txnid' => $result->transaction->id,
                'payment_amount' => $this->request->getQuery('amount'),
                'payment_status' => 'paid',
                'itemid' => $this->request->getQuery('productId')
            ]);
            $this->Payments->save($payment);
            $product = $this->Product->get($this->request->getQuery('productId'));
            $product->bump_data = 1;
            $this->Product->save($product);
            $bump_data = $this->BumpProducts->newEntity([
                'product_id' => $this->request->getQuery('productId'),
                'bump_date' => date('Y-m-d h:i:s'),
                'bump_days' => $days[$this->request->getQuery('amount')],
                'bump_user_id' => $this->Auth->user('id'),
                'bump_price' => $this->request->getQuery('amount'),
                'payment_mode' => 'GPay'
            ]);
            $this->BumpProducts->save($bump_data);
            $this->Flash->success(__('Product bump successfully'));
        } else if ($result->transaction) {
            $this->Flash->success(__($result->transaction->processorResponseText));
        } else {
            foreach ($result->errors->deepAll() as $error) {
                $this->Flash->success(__($error->message));
            }
        }
        $this->redirect('/');
    }
}

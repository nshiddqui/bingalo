<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AppInfo Entity
 *
 * @property int $id
 * @property string $logo_image
 * @property string|null $faqs
 * @property string|null $terms_of_services
 * @property string|null $about_us
 * @property string|null $contact_us
 * @property string|null $press
 * @property string|null $facebook
 * @property string|null $twitter
 * @property string|null $instagram
 * @property string|null $linkedin
 * @property string|null $youtube
 * @property string|null $pinterest
 * @property string|null $vimeo
 * @property string|null $appstore
 * @property string|null $playstore
 * @property string|null $privacy_policy
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class AppInfo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'logo_image' => true,
        'facebook' => true,
        'twitter' => true,
        'instagram' => true,
        'linkedin' => true,
        'youtube' => true,
        'pinterest' => true,
        'vimeo' => true,
        'appstore' => true,
        'playstore' => true,
        'created' => true,
        'modified' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReportedContent Entity
 *
 * @property int $id
 * @property string $post_id
 * @property string $reported_user_id
 * @property string $reported_reason
 * @property string $reported_desc
 * @property \Cake\I18n\FrozenTime $created_at
 * @property string $reported_by
 *
 * @property \App\Model\Entity\Post $post
 * @property \App\Model\Entity\ReportedUser $reported_user
 */
class ReportedContent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'post_id' => true,
        'reported_user_id' => true,
        'reported_reason' => true,
        'reported_desc' => true,
        'created_at' => true,
        'reported_by' => true,
        'post' => true,
        'reported_user' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SponsoredLocation Entity
 *
 * @property int $id
 * @property string $location_name
 * @property string $location_address
 * @property float $latitude
 * @property float $longitude
 * @property string $image
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $updated_at
 */
class SponsoredLocation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'location_name' => true,
        'location_address' => true,
        'website' => true, 
        'latitude' => true,
        'longitude' => true,
        'image' => true,
        'created_at' => true,
        'updated_at' => true,
    ];
}

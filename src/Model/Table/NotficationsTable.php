<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Notfications Model
 *
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 *
 * @method \App\Model\Entity\Notfication get($primaryKey, $options = [])
 * @method \App\Model\Entity\Notfication newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Notfication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Notfication|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notfication saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notfication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Notfication[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Notfication findOrCreate($search, callable $callback = null, $options = [])
 */
class NotficationsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('notfications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'notification_to',
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('notification_title')
                ->maxLength('notification_title', 255)
                ->requirePresence('notification_title', 'create')
                ->notEmptyString('notification_title');

        $validator
                ->scalar('notification_description')
                ->maxLength('notification_description', 255)
                ->requirePresence('notification_description', 'create')
                ->notEmptyString('notification_description');

        $validator
                ->integer('notification_to')
                ->requirePresence('notification_to', 'create')
                ->notEmptyString('notification_to');

        $validator
                ->integer('notification_by')
                ->requirePresence('notification_by', 'create')
                ->notEmptyString('notification_by');

        $validator
                ->integer('notification_type')
                ->requirePresence('notification_type', 'create')
                ->notEmptyString('notification_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {

        return $rules;
    }

}

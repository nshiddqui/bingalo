<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\FacebooksTable&\Cake\ORM\Association\BelongsTo $Facebooks
 * @property \App\Model\Table\GooglesTable&\Cake\ORM\Association\BelongsTo $Googles
 * @property \App\Model\Table\TwittersTable&\Cake\ORM\Association\BelongsTo $Twitters
 * @property \App\Model\Table\ApplesTable&\Cake\ORM\Association\BelongsTo $Apples
 * @property \App\Model\Table\FavouriteTable&\Cake\ORM\Association\HasMany $Favourite
 * @property \App\Model\Table\NotificationCountTable&\Cake\ORM\Association\HasMany $NotificationCount
 * @property \App\Model\Table\PostImagesTable&\Cake\ORM\Association\HasMany $PostImages
 * @property \App\Model\Table\UserCardsTable&\Cake\ORM\Association\HasMany $UserCards
 * @property \App\Model\Table\UserReadNotificationTable&\Cake\ORM\Association\HasMany $UserReadNotification
 * @property \App\Model\Table\UserVerificationTable&\Cake\ORM\Association\HasMany $UserVerification
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Facebooks', [
            'foreignKey' => 'facebook_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Googles', [
            'foreignKey' => 'google_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Twitters', [
            'foreignKey' => 'twitter_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Apples', [
            'foreignKey' => 'apple_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Favourite', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('NotificationCount', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('PostImages', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('UserCards', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('UserReadNotification', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('UserVerification', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('full_name')
            ->maxLength('full_name', 255)
            ->requirePresence('full_name', 'create')
            ->notEmptyString('full_name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 255)
            ->requirePresence('phone', 'create')
            ->notEmptyString('phone');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->scalar('lat')
            ->maxLength('lat', 255)
            ->requirePresence('lat', 'create')
            ->notEmptyString('lat');

        $validator
            ->scalar('lng')
            ->maxLength('lng', 255)
            ->requirePresence('lng', 'create')
            ->notEmptyString('lng');

        $validator
            ->scalar('updated_at')
            ->maxLength('updated_at', 255)
            ->requirePresence('updated_at', 'create')
            ->notEmptyString('updated_at');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->requirePresence('image', 'create')
            ->notEmptyFile('image');

        $validator
            ->scalar('image_name')
            ->maxLength('image_name', 255)
            ->requirePresence('image_name', 'create')
            ->notEmptyFile('image_name');

        $validator
            ->integer('language')
            ->notEmptyString('language');

        $validator
            ->scalar('token')
            ->maxLength('token', 255)
            ->requirePresence('token', 'create')
            ->notEmptyString('token');

        $validator
            ->scalar('push_token')
            ->maxLength('push_token', 255)
            ->requirePresence('push_token', 'create')
            ->notEmptyString('push_token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['facebook_id'], 'Facebooks'));
        $rules->add($rules->existsIn(['google_id'], 'Googles'));
        $rules->add($rules->existsIn(['twitter_id'], 'Twitters'));
        $rules->add($rules->existsIn(['apple_id'], 'Apples'));

        return $rules;
    }
}

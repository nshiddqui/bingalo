<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        $this->Html->link($result['product']->product_title, ['controller' => 'product', 'action' => 'view', $result['product']->id], ['target' => '_BLANK']),
        $this->Html->link($result['user']->full_name, ['controller' => 'users', 'action' => 'view', $result['user']->id], ['target' => '_BLANK']),
        $result->payment_mode,
        '$ ' . $result->bump_price,
        $result->bump_days,
        $result->created_at,
//        $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $result->id], ['escape' => false, 'class' => 'btn btn-info']) . '&nbsp;&nbsp;&nbsp;&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['escape' => false, 'class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $result['product']->product_title)])
    ]);
}
echo $this->DataTables->response();

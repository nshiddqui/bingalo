<?php
$this->assign('title', 'Categories Management');
?>
<?= $this->Html->css('categories', ['block' => true]) ?>
<?= $this->Html->script('categories', ['block' => true]) ?>
<?= $this->Form->create($category, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add Categories') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <label for="category-image">
                        <?= $this->Html->image((!empty($category['category_image']) ? $category['category_image'] : 'not-found.png'), ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Banner Image For Categories</p>
                    <?php
                    echo $this->Form->control('category_image', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*', 'required' => true]);
                    ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->control('category_name', ['label' => 'English Category Name']) ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->control('herbew_category_name', ['label' => 'Herbew Category Name']) ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->control('position', ['label' => 'Category Position']) ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__('Add')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>
<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        h($result->name),
        h($result->herbew_name),
        $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'subadd', $result->id, '?' => ['category_id' => $this->request->getQuery('category_id')]], ['escape' => false, 'class' => 'btn btn-info']) . '&nbsp;' .
            $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'deletesub', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->name), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();

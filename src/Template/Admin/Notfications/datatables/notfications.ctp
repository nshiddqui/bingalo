<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        (!empty($result['user']) ? $result['user']->full_name : 'All Users'),
        $result->notification_title,
        h($result->notification_description),
        !empty($result->notification_link) ? $this->Html->link('View Link', $result->notification_link, ['taget' => '_BLANK']) : '',
        $result->notification_date,
        $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->notification_title), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();

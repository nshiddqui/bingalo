<?php
$this->assign('title', 'Notifications Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Notifications</h3>
        <?= $this->Html->link('Send Notification', ['action' => 'send'], ['class' => 'pull-right btn btn-primary']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Notfications') ?>
    </div>
</div>
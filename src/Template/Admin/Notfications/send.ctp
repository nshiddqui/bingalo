<?php
$this->assign('title', 'Notification Management');
?>
<?= $this->html->css('notfications', ['block' => true]) ?>
<?= $this->html->script('notifications', ['block' => true]) ?>
<?= $this->Form->create($notification, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Send Notification') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <label for="image">
                        <?= $this->Html->image((!empty($notification['image']) ? $notification['image'] : 'not-found.png'), ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                </div>
                <div class="col-md-8">
                    <div class="hidden"><?= $this->Form->control('image', ['type' => 'file', 'accept' => 'image/*']) ?></div>
                    <?= $this->Form->control('user_type', ['options' => ['all' => 'All User', 'custom' => 'Custom User']]) ?>
                    <?= $this->Form->hidden('to_user_id[]') ?>
                    <div id="UserType" style="display: none">
                        <div class="form-group">
                            <label for="select-data-user">Select User</label>
                            <select id="select-data-user" name="to_user_id[]" multiple=""></select>
                        </div>
                    </div>
                    <?php
                    echo $this->Form->control('notification_title');
                    echo $this->Form->control('notification_link');
                    echo $this->Form->control('notification_description', ['type' => 'textarea']);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__('Send')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>
<script>
    $(document).ready(function() {
        $('#select-data-user').select2({
            width: '100%',
            placeholder: 'Select an User',
            ajax: {
                url: '/admin/api/v1/users/',
                dataType: 'json',
                delay: 250,
                cache: true
            }
        });
        $('#user-type').change();
        $('#user-type').on('change', function() {
            var val = $(this).val();
            if (val === 'custom') {
                $('#UserType').show();
            } else {
                $('#UserType').hide();
            }
        });
    });
</script>
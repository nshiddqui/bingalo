<?php
$this->assign('title', 'Manage Page');
?>
<?= $this->Html->css('https://www.jqueryscript.net/demo/Sliding-Switch-jQuery-simpleToggler/css/ui-toggle.css', ['block' => true]) ?>
<?= $this->Html->script('https://www.jqueryscript.net/demo/Sliding-Switch-jQuery-simpleToggler/js/jquery.toggler.js', ['block' => true]) ?>
<?= $this->Html->css('floara', ['block' => true]) ?>
<?= $this->Html->script('floara', ['block' => true]) ?>
<?= $this->Form->create($page) ?>
<div class="box" style="border-radius: 15px;">
    <div class="box-header">
        <h3>Update <?= $page->page_name ?> Page</h3><span class="pull-right"><?= $this->Form->control('status', ['type' => 'checkbox', 'label' => false]) ?></span>
    </div>
    <div class="box-body text-left">
        <div class="row">
            <div class="col-md-12">
                <?= $this->Form->control('text', ['label' => false]); ?>
            </div>
            <div class="col-md-12 text-center">
                <?= $this->Form->submit('Update Page', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<script>
    new FroalaEditor('#text', {
        height: 900,
        imageUploadURL: '<?= $this->Url->build(['action' => 'upload_image']) ?>'
    });
    $('#status').checkToggler({
        labelOn: 'Show',
        labelOff: 'Hide'
    });
</script>
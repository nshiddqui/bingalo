<?php
$this->assign('title', 'Sponsored Locations Management');
?>
<?= $this->Html->css('sponsored_location', ['block' => true]) ?>
<?= $this->Html->script('sponsored_location', ['block' => true]) ?>
<?= $this->Form->create($sponsoredLocation, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add Sponsored Locations') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="image">
                        <?= $this->Html->image('not-found.png', ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Image For Sponsored Location</p>
                    <?php
                    echo $this->Form->control('image', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*', 'required' => false]);
                    ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('location_name') ?>
                    <?= $this->Form->control('location_address') ?>
                    <?= $this->Form->control('website') ?>
                    <?= $this->Form->control('latitude') ?>
                    <?= $this->Form->control('longitude') ?>
                </div>


            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__('Add')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>
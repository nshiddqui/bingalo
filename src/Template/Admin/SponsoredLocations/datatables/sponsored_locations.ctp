<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        $this->Html->link($this->Html->image($result->image, ['height' => '50', 'width' => '50']), $result->image, ['fancybox' => true, 'escape' => false]),
        h($result->location_name),
        h($result->location_address),
        !empty($result->website) ? $this->Html->link('Link', $result->website, ['target' => '_BLANK']) : '',
        h($result->latitude),
        h($result->longitude),
        $result->created_at,
        $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $result->id], ['escape' => false, 'class' => 'btn btn-info']) . '&nbsp;' .
            $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->client_name), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();

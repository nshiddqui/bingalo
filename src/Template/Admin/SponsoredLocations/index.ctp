<?php
$this->assign('title', 'Sponsored Locations Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Sponsored Locations</h3>
        <?= $this->Html->link('Add Sponsored Locations', ['action' => 'add'], ['class' => 'pull-right btn btn-primary']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('SponsoredLocations') ?>
    </div>
</div>
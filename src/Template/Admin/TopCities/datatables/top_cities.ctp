<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        $result->id,
        h($result->name),
        h($result->herbew_name),
        h($result->lat),
        h($result->lng),
        $result->position,
        $result->created,
        $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $result->id], ['escape' => false, 'class' => 'btn btn-info']) . '&nbsp;' .
        $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->name), 'escape' => false, 'class' => 'btn btn-danger'])
    ]);
}
echo $this->DataTables->response();

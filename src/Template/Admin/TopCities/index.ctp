<?php
$this->assign('title', 'Top Cities Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Top Cities</h3>
        <?= $this->Html->link('Add Top Cities', ['action' => 'add'], ['class' => 'pull-right btn btn-primary']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('TopCities') ?>
    </div>
</div>
<?php
$this->assign('title', 'Dashboard Summary');
?>
<style>
    .br-20{
        border-radius: 20px;
    }
</style>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <h3>Users Summary</h3>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua br-20 text-center">
                    <div class="inner">
                        <h3><?= $today_users ?></h3>
                        <p>Users Registered Today</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green br-20 text-center">
                    <div class="inner">
                        <h3><?= $week_users ?></h3>
                        <p>Users Registered This Week</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow br-20 text-center">
                    <div class="inner">
                        <h3><?= $total_users ?></h3>
                        <p>All Users</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <h3>Product Summary</h3>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua br-20 text-center">
                    <div class="inner">
                        <h3><?= $today_products ?></h3>
                        <p>Product Posted Today</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green br-20 text-center">
                    <div class="inner">
                        <h3><?= $week_products ?></h3>
                        <p>Product Posted This Week</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow br-20 text-center">
                    <div class="inner">
                        <h3><?= $total_products ?></h3>
                        <p>All Products</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <h3>Bumped Items Summary</h3>
        <hr>
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua br-20 text-center">
                    <div class="inner">
                        <h3><?= $today_bump_products ?></h3>
                        <p>Bumped Items Today</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green br-20 text-center">
                    <div class="inner">
                        <h3><?= $week_bump_products ?></h3>
                        <p>Bump items this week</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow br-20 text-center">
                    <div class="inner">
                        <h3><?= $total_bump_products ?></h3>
                        <p>All Bump Items</p>
                    </div>
                </div>
            </div>
            <!-- ./col -->
        </div>
    </div>
</div>
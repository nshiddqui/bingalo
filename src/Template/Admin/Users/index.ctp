<?php
$this->assign('title', 'Users');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Users</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Users') ?>
    </div>
</div>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create() ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Mail</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->hidden('email', ['id' => 'email']) ?>
                <?= $this->Form->control('subject') ?>
                <?= $this->Form->control('message', ['type' => 'textarea']) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?= $this->Form->submit('Send', ['class' => 'btn btn-primary']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php
$this->assign('bodyClass', 'detail-page');
if (!empty($authUser)) {
    $loggin = true;
}
if (!isset($auth_user)) {
    $auth_user = $authUser;
}
$rating = $auth_user['rating']['rating'];
if ($auth_user['id'] != $authUser['id']) { ?>
    <?php if (isset($authUser)) { ?>
        <script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-firestore.js"></script>
        <script>
            firebase.initializeApp(FIREBASE_SETUP);
            var db = firebase.firestore();
            var ConversationsRef = db.collection("Conversations");

            function chatWithUser() {
                var newUserData = {
                    isRead: {
                        <?= $authUser['id'] ?>: true,
                        <?= $auth_user['id'] ?>: true
                    },
                    userIDs: [],
                    lastMessage: '',
                    timestamp: (Math.floor(Date.now() / 1000)),
                    id: '<?= $authUser['id'] . '-' . $auth_user['id'] ?>'
                };
                newUserData['userIDs'].push('<?= $authUser['id'] ?>');
                newUserData['userIDs'].push('<?= $auth_user['id'] ?>');
                ConversationsRef.doc('<?= $authUser['id'] . '-' . $auth_user['id'] ?>').set(newUserData, {
                    merge: true
                }).then(function() {
                    window.location.href = '/chat/';
                });
            }
        </script>
    <?php } ?>
<?php
    unset($authUser);
}
$icons = [];
if (!empty($user_verfy)) {
    foreach ($user_verfy as $user_verification) {
        $icons[] = $this->Html->image($user_verification->verified_by . '-icon.png', ['style' => 'height:20px;margin: 0 1.8px']);
    }
}
?>
<style>
    @charset "UTF-8";

    :root {
        --star-size: 30px;
        --star-color: #bcbcbc;
        --star-background: #fc0;
    }

    .Stars {
        margin-bottom: 10px;
    }

    .Stars span img {
        height: 13px;
    }

    .tab-profile {
        text-align: center;
    }

    span {
        font-size: inherit;
        position: inherit;
        top: inherit;
    }

    .report-item img {
        cursor: pointer;
        height: 60px;
    }

    .cursor-pointer {
        cursor: pointer;
    }

    #url {
        border-radius: 20px;
    }

    .sell-tab ul li.active a {
        color: #2678bb;
    }

    .product-image-preview {
        width: 100%;
        padding-bottom: 100%;
        background-size: cover;
        background-position: center;
    }

    .dist-tag {
        position: absolute;
        top: 20px;
        overflow: hidden;
        left: -40px;
        background: #0087ff;
        width: 140px;
        text-align: center;
        font-size: 18px;
        color: #fff;
        transform: rotate(310deg);
        padding-top: 2px;
    }

    .sell-tab ul li a {
        font-weight: bold;
    }

    .profile-grid-products div[data-src] {
        margin: 50px auto;
        padding: 0;
    }

    .image img[data-src] {
        margin: 40px auto;
        padding: 0;
    }

    [data-src] {
        width: 2rem !important;
        height: 2rem !important;
    }

    @media only screen and (max-width: 600px) {
        .tab-profile {
            width: 49%;
        }
    }
</style>
<div class="details bg-white">
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12 p-2 text-center px-0">
                <div class="row">
                    <div class="col-md-12 py-2 px-0 mt-4 bg-white">
                        <div class="image" style="position:relative;width: 100px;height: 100px;margin: auto;display: flex;">
                            <?php if (isset($authUser) && !empty($authUser)) { ?>
                                <label for="profile-pic-image"><?= $this->Html->image('upload-pic.png', ['style' => 'height: 25px; width: 25px; position: absolute; right: 0px; top: 6px; cursor: pointer;']); ?></label>
                                <div class="d-none">
                                    <?= $this->Form->create(null, ['type' => 'file', 'id' => 'profile-picture-form', 'url' => ['controller' => 'Authorized', 'action' => 'changeProfilePic']]) ?>
                                    <?= $this->Form->control('image', ['type' => 'file', 'id' => 'profile-pic-image', 'accept' => 'image/*', 'label' => false]) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            <?php } ?>
                            <?= $this->Html->image(!empty($auth_user['image']) ? $auth_user['image']  : 'profile.png', ['class' => 'rounded-circle', 'style' => 'height:100px;width: 100px;object-fit: cover;']) ?>
                        </div>
                        <h3 style="font-size: 18px; font-weight: bold; margin-top: 20px;margin-bottom: 0;"><?= $auth_user['full_name'] ?></h3>
                        <?php if (isset($authUser) && !empty($authUser)) { ?>
                            <a href="javascript: buildTrust()" class=""><?= $this->Html->image((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'he_build_trust.png' : 'btn_build_trust.png'), ['style' => 'height: 35px;']) ?></a>
                        <?php } ?>
                        <div class="Stars">
                            <?php if ($rating >= 1) { ?>
                                <span><?= $this->Html->image('rate_star_filled.png') ?></span>
                            <?php } else if ($rating > 0 && $rating < 1) { ?>
                                <span><?= $this->Html->image('rate_star_half_filled.png') ?></span>
                            <?php } else { ?>
                                <span><?= $this->Html->image('rate_star_unfilled.png') ?></span>
                            <?php } ?>
                            <?php if ($rating >= 2) { ?>
                                <span><?= $this->Html->image('rate_star_filled.png') ?></span>
                            <?php } else if ($rating > 1 && $rating < 2) { ?>
                                <span><?= $this->Html->image('rate_star_half_filled.png') ?></span>
                            <?php } else { ?>
                                <span><?= $this->Html->image('rate_star_unfilled.png') ?></span>
                            <?php } ?>
                            <?php if ($rating >= 3) { ?>
                                <span><?= $this->Html->image('rate_star_filled.png') ?></span>
                            <?php } else if ($rating > 2 && $rating < 3) { ?>
                                <span><?= $this->Html->image('rate_star_half_filled.png') ?></span>
                            <?php } else { ?>
                                <span><?= $this->Html->image('rate_star_unfilled.png') ?></span>
                            <?php } ?>
                            <?php if ($rating >= 4) { ?>
                                <span><?= $this->Html->image('rate_star_filled.png') ?></span>
                            <?php } else if ($rating > 3 && $rating < 4) { ?>
                                <span><?= $this->Html->image('rate_star_half_filled.png') ?></span>
                            <?php } else { ?>
                                <span><?= $this->Html->image('rate_star_unfilled.png') ?></span>
                            <?php } ?>
                            <?php if ($rating >= 5) { ?>
                                <span><?= $this->Html->image('rate_star_filled.png') ?></span>
                            <?php } else if ($rating > 4 && $rating < 5) { ?>
                                <span><?= $this->Html->image('rate_star_half_filled.png') ?></span>
                            <?php } else { ?>
                                <span><?= $this->Html->image('rate_star_unfilled.png') ?></span>
                            <?php } ?>
                        </div>
                        <?php if (!isset($authUser) || empty($authUser)) { ?>
                            <?php if (!empty($icons)) { ?>
                                <p style="font-size: 15px; color: #2678bb; margin-bottom: 0; font-weight: bold;"><?= __('Verified with') ?></p>
                                <div class="">
                                    <?= implode(' ', $icons) ?>
                                </div>
                            <?php } else { ?>
                                <p style="font-size: 15px; color: #2678bb; margin-bottom: 0; font-weight: bold;"><?= __('Not verified yet') ?></p>
                            <?php } ?>
                        <?php }  ?>
                        <?php if (!isset($authUser) || empty($authUser)) { ?>
                            <div style="width: 400px;margin: auto;">
                                <?= $this->Html->link(__('Report'), '#flag-seller', ['class' => 'btn btn-primary btn-round rounded-pill mt-4', 'data-toggle' => 'modal', 'style' => 'background:#2678bb;width:45%', 'login' => true]) ?>
                                <?= $this->Html->link(__('Message Seller'), '#', ['class' => 'btn btn-primary btn-round rounded-pill mt-4', 'download-app' => true, 'style' => 'background:#2678bb;width:45%']) ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php /*
                    <?php if (isset($authUser) && !empty($authUser)) { ?>
                        <div class="col-md-12 py-2 px-0 mt-4 p-4 bg-white">
                            <h3 style="text-align: left; font-size: 15px; color: #2678bb; padding: 0 15px; font-weight: bold;">Build Trust</h3>
                            <?= $this->Html->image('facebook-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'url' => $this->Url->build(['controller' => 'Authorized', 'action' => 'authenticateFacebook', 'auth'])]) ?>
                            <?= $this->Html->image('google-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'url' => $this->Url->build(['controller' => 'Authorized', 'action' => 'authenticateGoogle', 'auth'])]) ?>
                            <?= $this->Html->image('twitter-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'url' => $this->Url->build(['controller' => 'Authorized', 'action' => 'authenticateTwitter', 'auth'])]) ?>
                            <?= $this->Html->image('email-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'onclick' => 'buildEmail()']) ?>
                            <?= $this->Html->image('phone-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'onclick' => 'buildPhone()']) ?>
                        </div>
                    <?php } ?>
                    <div class="col-md-12 py-2 px-0 mt-4 p-4 bg-white">
                        <h3 style="text-align: left; font-size: 15px; color: #2678bb; padding: 0 15px; font-weight: bold;">Share Profile</h3>
                        <?= $this->Form->control('url', ['value' => $this->Url->build('/profile/' . $authUser['id'], true), 'readonly' => true, 'label' => false, 'class' => 'jgh']) ?>
                    </div>
                    */ ?>
                </div>
            </div>
            <div class="col-md-12 p-2 px-0">
                <div class="row">
                    <div class="col-md-12 py-2 px-0 mt-4 pb-5 pt-3 bg-white">
                        <h3 class="font-weight-bold" style="font-size: 20px;"><?= __('Product Listed') ?></h3>
                        <div class="row mt-4">
                            <div class="col-md-12 px-0">
                                <div class="sell-tab">
                                    <ul>
                                        <li class="active tab-profile" action="sell"><a href="#"><?= __('Selling') ?> (<?= count($sell_products) ?>)</a></li>
                                        <li action="favorite" class="tab-profile"><a href="#"><?= __('Favorites') ?> (<?= count($favorite_products) ?>)</a></li>
                                        <li action="bought" class="tab-profile"><a href="#"><?= __('Bought') ?> (0)</a></li>
                                        <li action="sold" class="tab-profile"><a href="#"><?= __('Sold') ?> (<?= count($sold_products) ?>)</a></li>
                                    </ul>
                                </div>
                                <div class="profile-grid-products listings" id="sell">
                                    <div class="container full-container">
                                        <?= $this->element('short_product', ['products' => $sell_products]) ?>
                                    </div>
                                </div>
                                <div class="profile-grid-products listings" style="display:none" id="favorite">
                                    <div class="container full-container">
                                        <?= $this->element('short_product', ['products' => $favorite_products]) ?>
                                    </div>
                                </div>
                                <div class="profile-grid-products listings" style="display:none" id="bought">
                                    <div class="container full-container">
                                        <?= $this->element('short_product', ['products' => []]) ?>
                                    </div>
                                </div>
                                <div class="profile-grid-products listings" style="display:none" id="sold">
                                    <div class="container full-container">
                                        <?= $this->element('short_product', ['products' => $sold_products]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="build-trust" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Build Trust') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-2 mb-5">
                    <div class="row text-center">
                        <div class="col-12">
                            <?= $this->Html->image('facebook-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'url' => $this->Url->build(['controller' => 'Authorized', 'action' => 'authenticateFacebook', 'auth'])]) ?>
                            <?= $this->Html->image('google-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'url' => $this->Url->build(['controller' => 'Authorized', 'action' => 'authenticateGoogle', 'auth'])]) ?>
                            <?= $this->Html->image('twitter-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'url' => $this->Url->build(['controller' => 'Authorized', 'action' => 'authenticateTwitter', 'auth'])]) ?>
                            <?= $this->Html->image('email-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'onclick' => 'buildEmail()']) ?>
                            <?= $this->Html->image('phone-icon.png', ['style' => 'height: 50px; margin: 0 10px;cursor:pointer', 'onclick' => 'buildPhone()']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="build-email" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Email Verification') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-2">
                    <div class="row text-center">
                        <div class="col-12">
                            <?= $this->Form->create(null, ['id' => 'form-email']) ?>
                            <?= $this->Form->control('email', ['placeholder' => __('Enter Email'), 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->button(__('Send OTP'), ['label' => false]) ?>
                            <?= $this->Form->end() ?>
                            <?= $this->Form->create(null, ['id' => 'otp-email', 'style' => 'display:none;']) ?>
                            <?= $this->Form->control('otp', ['placeholder' => __('Enter OTP'), 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->button(__('Verify OTP'), ['label' => false]) ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="build-phone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Mobile Number Verification') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-2">
                    <div class="row text-center">
                        <div class="col-12">
                            <?= $this->Form->create(null, ['url' => ['Authorized', 'action' => 'verfyOtp', 'email'], 'id' => 'form-phone']) ?>
                            <?= $this->Form->control('phone', ['placeholder' => 'Enter Mobile Number', 'type' => 'number', 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->button('Send OTP', ['label' => false]) ?>
                            <?= $this->Form->end() ?>
                            <?= $this->Form->create(null, ['url' => ['Authorized', 'action' => 'verfyOtp', 'phone'], 'id' => 'otp-phone', 'style' => 'display:none;']) ?>
                            <?= $this->Form->control('otp', ['placeholder' => 'Enter OTP', 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->button('Verify OTP', ['label' => false]) ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="flag-seller" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel">Report</h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-5">
                    <div class="row">
                        <div class="col-12">
                            <div class="row pl-3">
                                <div class="col-md-2 p-0">
                                    <?= $this->Html->image($auth_user['image'] != '' ? $auth_user['image'] : 'profile.png', ['class' => 'rounded-circle', 'style' => 'height:100px;width: 100px;']) ?>
                                </div>
                                <div class="col-md-10">
                                    <h5 style="font-weight: bold;color:#54585d;margin-top: 32px;"><?= $auth_user['full_name'] ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 px-4 mt-3">
                            <h5 class="font-weight-bold"><?= __('Reason for reporting the seller?') ?></h5>
                            <div class="row text-center report-item">
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select21']) ?>
                                        <?= $this->Html->image('eye-glasses.png', ['select' => 'report-select21']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('They Missed our meeting') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select22']) ?>
                                        <?= $this->Html->image('Group 280.png', ['select' => 'report-select22']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __("Trouble at meetup") ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select23']) ?>
                                        <?= $this->Html->image('unlike.png', ['select' => 'report-select23']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Rude or Inappropriate') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select24']) ?>
                                        <?= $this->Html->image('Group 2.png', ['select' => 'report-select24']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Problem with item') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select25']) ?>
                                        <?= $this->Html->image('Group 273.png', ['select' => 'report-select25']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Messaging Problem') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select26']) ?>
                                        <?= $this->Html->image('Line_2.png', ['select' => 'report-select26']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __("Made a lower offer") ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select27']) ?>
                                        <?= $this->Html->image('Group 279.png', ['select' => 'report-select27']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __("Shipping Problems") ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select28']) ?>
                                        <?= $this->Html->image('Group 8.png', ['select' => 'report-select28']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('None of the above') ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 px-4 mt-3">
                            <?= $this->Form->control('flag_note', ['type' => 'textarea', 'rows' => '5', 'label' => false, 'placeholder' => __('Note'), 'style' => 'background: #E5E7EB; border-radius: 20px;']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center border-0 mb-4">
                <button type="button" class="btn btn-primary btn-round rounded-pill w-25 m-auto" id="submit-flag-form"><?= __("Flag User") ?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="thank-you-flag-seller" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Report') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-5">
                    <div class="row text-center">
                        <div class="col-12">
                            <?= $this->Html->image('report_selected.png') ?>
                            <p class="mt-3" style="font-size: 15px;"><?= __('Thank you for reporting.') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var select_report = false;
    var select_flag = false;
    $(document).ready(function() {
        $('#profile-pic-image').on('change', function() {
            $('#profile-picture-form').submit();
        });
        $('.tab-profile').on('click', function(ev) {
            ev.preventDefault();
            $('.tab-profile').removeClass('active');
            $(this).addClass('active');
            $('.profile-grid-products').hide();
            $('#' + $(this).attr('action')).show();
            resizeAllGridItems();
        });
        $('#form-email').on('submit', function(ev) {
            ev.preventDefault();
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Authorized', 'action' => 'sendEmailOTP']) ?>',
                data: {
                    email: $('#email').val(),
                    verify_method: 'email'
                },
                type: 'GET',
                success: function() {
                    $('#form-email').hide();
                    $('#otp-email').show();
                }
            });
        });
        $('#form-phone').on('submit', function(ev) {
            ev.preventDefault();
            $.ajax({
                url: '<?= $this->Url->build(['controller' => 'Authorized', 'action' => 'sendEmailOTP']) ?>',
                data: {
                    email: $('#phone').val(),
                    verify_method: 'phone'
                },
                type: 'GET',
                success: function() {
                    $('#form-phone').hide();
                    $('#otp-phone').show();
                }
            });
        });

        $('img[select]').on('click', function() {
            $('[id^="report-select"]').hide();
            select_report = $(this).attr('select').replace(/[^\d.]/g, '');
            select_flag = $(this).attr('select').replace(/[^\d.]/g, '');
            $('#' + $(this).attr('select')).show();
        });

        $('#submit-report-form').on('click', function() {
            if (select_report == false) {
                alert('Please select report reason.');
                return
            }
            select_report = false;
            $('#note').val('');
            $('[id^="report-select"]').hide();
            $('#report-listing').modal('hide');
            $('#thank-you-report-listing').modal();
        });
        $('#submit-flag-form').on('click', function() {
            if (select_flag == false) {
                alert('Please select report reason.');
                return
            }
            select_flag = false;
            $('#flag-note').val('');
            $('[id^="flag-select"]').hide();
            $('#flag-seller').modal('hide');
            $('#thank-you-flag-seller').modal();
        });

    });

    function buildTrust() {
        $('#build-trust').modal();
    }

    function buildEmail() {
        $('#build-email').modal();
    }
</script>
<?php
$this->assign('bodyClass', 'edit-page');
echo $this->Html->css('jquery.tagit');
echo $this->Html->css('tagit.ui-zendesk');
echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js');
echo $this->Html->script('tag-it.min');
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.min.js');
?>
<style>
    #map {
        height: 300px;
        width: calc(100% - 26px);
    }

    .pac-item span {
        font-size: 14px;
        top: 0;

    }

    .input.location .input.text {
        width: calc(100% - 26px);
    }

    .usd.icon {
        position: absolute;
        top: 39%;
        left: 9px;
        font-style: normal;
    }

    .input.price-usd {
        position: relative;
    }

    .input.price-usd input {
        padding-left: 25px;
    }

    .form-product .input .input {
        width: 100%;
    }

    .input.cancle {
        width: 40%;
        margin: auto;
        padding-top: 10px;
        display: block;
    }

    .input.cancle a {
        text-align: center;
        background: red;
        color: #fff;
        text-transform: uppercase;
        border-radius: 50px;
        width: 100%;
        margin: auto;
        display: block;
        outline: none;
        cursor: pointer;
        padding: 6px;
    }

    .specification-span {
        display: block;
        margin: 5px 0;
        color: #8c8c8c;
        font-size: 10px;
    }

    button,
    input,
    optgroup,
    select,
    textarea,
    .form-product .edit-photo ul li {
        border-radius: 3px;
    }

    .sell-tab ul li.active a {
        color: #2678bb;
    }

    .sell-tab ul li a {
        font-weight: bold;
        font-size: 13px;
        padding: 0 5px 0;
    }

    li.child-cat {
        text-align: left;
        padding: 7px 0;
        border-bottom: 1px solid #ddd;
        cursor: pointer;
    }

    li.child-cat:hover {
        color: #2678bb;
    }

    .profile-grid-products {
        display: none;
    }

    .tab-profile {
        min-width: 30%;
    }

    .profile-grid-products.listings {
        padding-top: 5px;
        background: #fff;
    }
</style>
<div class="top-band">
    <h3><?= __(isset($data['id']) ? 'Update Your Product' : 'Sell Your Stuff') ?></h3>
</div>

<div class="form-product">
    <div class="container">
        <h4><?= __('Photos') ?></h4>
        <?= $this->Form->create(null, ['type' => 'file', 'id' => 'sell-your-stuff-form']) ?>
        <?= $this->Form->hidden('lat', ['id' => 'lat', 'value' => isset($data['product_lat']) ? $data['product_lat'] : $localInformation['latitude']]) ?>
        <?= $this->Form->hidden('lng', ['id' => 'lng', 'value' => isset($data['product_lng']) ? $data['product_lng'] : $localInformation['longitude']]) ?>
        <?= $this->Form->hidden('product_city', ['id' => 'product_city', 'value' => isset($data['product_city']) ? $data['product_city'] : $localInformation['city']]) ?>
        <?= $this->Form->hidden('product_state', ['id' => 'product_state', 'value' => isset($data['product_state']) ? $data['product_state'] : $localInformation['region_name']]) ?>
        <?= $this->Form->hidden('product_country', ['id' => 'product_country', 'value' => isset($data['product_country']) ? $data['product_country'] : $localInformation['country_name']]) ?>
        <div class="edit-photo">
            <ul id="media-list">
                <?php
                $images_count = isset($data['images']) ? count($data['images']) : 0;
                if (isset($data['images']) && !empty($data['images'])) {
                    foreach ($data['images'] as $images) { ?>
                        <li style="padding: 0;position:relative">
                            <?= $this->Html->link($this->Html->image('btn_icon_cross.png', ['style' => 'height: 30px;']), ['controller' => 'Authorized', 'action' => 'deleteImage', $images['id']], ['style' => 'position: absolute; right: -5px; top: -13px; font-size: 30px; font-weight: bold; padding: 0; line-height: .7; }', 'escape' => false]) ?>
                            <?= $this->Html->image($images['image_url'], ['style' => 'width: 100%; height: 100%; object-fit: cover;']) ?>
                        </li>
                <?php }
                }
                ?>
                <?php
                $first = true;
                for ($i = $images_count + 1; $i < 11; $i++) { ?>
                    <li class="add" style="position: relative;">
                        <?= $this->Html->image('btn_icon_cross.png', ['style' => 'right: -15px; height: 30px; position: absolute; top: -15px;display:none;cursor: pointer;', 'id' => 'removefile-' . $i, 'onclick' => 'removeFile(' . $i . ')']) ?>
                        <label for="picupload-<?= $i ?>">
                            <img src="/images/add-camera.png" style="display: <?= $first ? 'flex' : 'none' ?> ;object-fit: cover; height: 100%; width: 100%;" alt="">
                            <label style="background: #ececec;display: <?= $first ? 'none' : 'flex' ?> ;"><?= $i ?></label>
                        </label>
                        <?= $this->Form->control('image[]', ['type' => 'file', 'label' => false, 'accept' => 'image/*', 'click-type' => 'type2', 'id' => 'picupload-' . ($i), 'class' => 'picupload']) ?>
                    </li>
                    <?php
                    if ($first) {
                        $first = false;
                    }
                    ?>
                <?php } ?>
            </ul>
        </div>

        <div class="input">
            <?= $this->Form->control('product_title', ['label' => __('Title'), 'value' => isset($data['product_title']) ? $data['product_title'] : '']) ?>
        </div>

        <div class="input price-usd">
            <i class="usd icon">&#8362; </i>
            <?= $this->Form->control('product_price', ['label' => __('Price'), 'step' => '.01', 'value' => isset($data['product_price']) ? $data['product_price'] : '']) ?>
        </div>
        <div class="input specification">
            <label><?= __('Specifications') ?></label>
            <span class="specification-span"><?= __('Add Unique Specifics About Your Listing') ?></span>
            <?= $this->Form->control('product_specifications', ['id' => 'myTags', 'label' => false, 'value' => isset($data['product_specifications']) ? $data['product_specifications'] : '']) ?>
        </div>
        <div class="input" style="width: calc(100% - 26px);">
            <?= $this->Form->control('product_condition', ['options' => [
                'Brand New#Not Touched. In Original Packaging' => __('Brand New-Not Touched. In Original Packaging'),
                'New#Open Box (Packaging Is Off, Never Used)' => __('New-Open Box (Packaging Is Off, Never Used)'),
                'Used#Like-New (Excellent Condition. Signs Of Minor Use)' => __('Used-Like-New (Excellent Condition. Signs Of Minor Use)'),
                'Used#Very Good (Signs Of Significant Use. Functions Properly)' => __('Used-Very Good (Signs Of Significant Use. Functions Properly)'),
                'Used#Acceptable (May Have Minor Scratches & Dents. Still Works Well)' => __('Used-Acceptable (May Have Minor Scratches & Dents. Still Works Well)')
            ], 'label' => __('Conditions'), 'style' => 'height: 40px;', 'value' => isset($data['product_condition']) ? $data['product_condition'] : '']) ?>
        </div>
        <div class="input" style="width: calc(100% - 26px);">
            <label for="product-category"><?= __("Choose a Category") ?><br>
                <?= $this->Form->control('product_category', ['options' => $categories, 'style' => 'pointer-events: none;', 'label' => false, 'valuess' => isset($data['product_category']) ? $data['product_category'] . '#' . $data['parent_category'] : '']) ?>
            </label>
        </div>

        <div class="input textarea">
            <?= $this->Form->control('product_description', ['type' => 'textarea', 'cols' => '30', 'rows' => '10', 'label' => __('Description'), 'value' => isset($data['product_description']) ? $data['product_description'] : '']) ?>
        </div>

        <div class="input location">
            <?= $this->Form->control('product_location', ['label' => __('Location'), 'value' =>  isset($data['product_location']) ? $data['product_location'] : $localInformation['city'] . ', ' . $localInformation['region_name']]) ?>
            <div id="map"></div>
        </div>
        <div class="form-submit" style="text-align:center">
            <div class="input submit" style="display: inline-block;">
                <?= $this->Form->submit(__('Post')) ?>
            </div>
            <div class="input cancle" style="display: inline-block;">
                <?= $this->Html->link(__('Cancel'), '/') ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="modal fade" id="select-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="height:500px">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Choose a Category') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 20px;">
                    &times;
                </button>
            </div>
            <div class="modal-body px-0">
                <div class="container px-0">
                    <div class="row text-center">
                        <div class="col-12 px-0">
                            <div class="sell-tab">
                                <ul id="cat-list" style="white-space: nowrap;max-width: 100%;display: inline-block; overflow: auto; overflow-y: hidden;">
                                    <?php
                                    foreach ($model_categories as $model_categorie) {
                                        if ($model_categorie->category_name == 'See All') {
                                            continue;
                                        }
                                    ?>
                                        <li class="tab-profile" data-value="<?= $model_categorie->category_name  ?>" action="cat-<?= $model_categorie->id ?>"><a href="#">
                                                <?= $this->Html->image($model_categorie->category_image, ['style' => 'height:30px;margin-bottom: 5px;']) ?><br>
                                                <?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $$model_categorie->herbew_category_name : $model_categorie->category_name) ?>
                                            </a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php
                            foreach ($model_categories as $model_categorie) { ?>
                                <?php if ($model_categorie->category_name == 'See All') {
                                    continue;
                                } ?>
                                <?php if (!empty($model_categorie['sub_categories'])) {  ?>
                                    <div class="profile-grid-products listings" id="cat-<?= $model_categorie->id ?>">
                                        <div class="container full-container">
                                            <ul>
                                                <?php foreach ($model_categorie['sub_categories'] as $sub_categories) {  ?>
                                                    <li class="child-cat" data-val="<?= $sub_categories->name . '#' . $model_categorie->category_name ?>" onclick="$('#product-category').val(this.getAttribute('data-val'));$('#product-category').change();$('#select-category').modal('hide');"><?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $sub_categories->herbew_name : $sub_categories->name) ?></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {

        new AutoNumeric('#product-price', 'dotDecimalCharCommaSeparator');

        //-------------------------------
        // Minimal
        //-------------------------------
        $('#myTags').tagit({
            allowSpaces: true
        });
        $('#file').on('change', function() {
            var file = $(this).get(0).files[0];

            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewImg").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        });

    });
</script>
<script>
    let marker;
    let map;

    function initialize() {
        map = new google.maps.Map(document.getElementById("map"), {
            center: {
                lat: -33.8688,
                lng: 151.2195
            },
            zoom: 13,
            mapTypeId: "roadmap",
        });
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById("product-location"), {
                types: ["geocode"]
            }
        );
        autocomplete.addListener("place_changed", () => {
            const place = autocomplete.getPlace();
            // Clear out the old markers.
            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();
            if (place.address_components) {
                for (var ac = 0; ac < place.address_components.length; ac++) {
                    var component = place.address_components[ac];
                    if (component.types.includes('sublocality') || component.types.includes('locality')) {
                        if ($('#product_state').val() !== component.long_name) {
                            $('#product_city').val(component.long_name);
                        }
                    } else if (component.types.includes('administrative_area_level_1')) {
                        if ($('#product_state').val() !== component.long_name) {
                            $('#product_state').val(component.long_name);
                        }
                    } else if (component.types.includes('country')) {
                        $('#product_country').val(component.long_name);
                    }
                };
            }
            placeMarker(place.geometry.location, place.name);
            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
            map.fitBounds(bounds);
        });
        if ($('#lat').val().length && $('#lng').val().length) {
            let pos = new google.maps.LatLng($('#lat').val(), $('#lng').val());
            const bounds = new google.maps.LatLngBounds();
            bounds.extend(pos);
            placeMarker(pos);
            map.fitBounds(bounds);
            var listener = google.maps.event.addListener(map, "idle", function() {
                if (map.getZoom() > 16)
                    map.setZoom(16);
                google.maps.event.removeListener(listener);
            });
        }
    }

    function placeMarker(location, address = null) {
        // Clear out the old markers.
        if (marker) {
            marker.setMap(null);
        }
        $('#lat').val(location.lat());
        $('#lng').val(location.lng());
        // Create a marker for each place.
        marker = new google.maps.Marker({
            map: map,
            icon: {
                url: "https://maps.google.com/mapfiles/ms/icons/blue-dot.png"
            },
            position: location,
            title: address,
            draggable: true,
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            var markerPosition = marker.getPosition()
            $('#lat').val(markerPosition.lng());
            $('#lng').val(markerPosition.lat());
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    var lastUpload = 0;
    $(function() {
        var names = [];
        var max_file_length = <?= 10 - $images_count ?>;
        var skip = <?= $images_count ?>;
        $('body').on('change', '.picupload', function(event) {
            var id = $(this).attr('id');
            var activeId = id.split('-')[1];
            lastUpload = activeId;
            var next_id = 'picupload-' + (parseInt(activeId) + 1);
            $('label[for="' + next_id + '"] img').css('display', 'flex');
            $('label[for="' + next_id + '"] label').css('display', 'none');
            var files = event.target.files[0];
            var picReader = new FileReader();
            picReader.fileName = event.target.files.name;
            picReader.addEventListener("load", function(event) {
                var picFile = event.target;
                $('label[for="' + id + '"] img').attr('src', picFile.result);
                $('label[for="' + id + '"]').attr('for', id + '-1');
                $('#removefile-' + activeId).show();
                processImage();
            });
            picReader.readAsDataURL(files);
        });
    });

    function removeFile(id) {
        $('#picupload-' + id).val('');
        if (id == lastUpload) {
            var current_id = 'picupload-' + (parseInt(lastUpload));
            var next_id = 'picupload-' + (parseInt(lastUpload) + 1);
            $('#removefile-' + id).hide();
            $('label[for="' + current_id + '-1"] img').attr('src', '/images/add-camera.png');
            $('label[for="' + current_id + '-1"]').attr('for', 'picupload-' + lastUpload);
            $('label[for="' + next_id + '"] img').css('display', 'none');
            $('label[for="' + next_id + '"] label').css('display', 'flex');
            lastUpload--;
        } else {
            var intId = (parseInt(lastUpload));
            var dataSource = [];
            for (i = id; i < intId; i++) {
                var current_id = 'picupload-' + i;
                var next_id = 'picupload-' + (i + 1);
                dataSource.push({
                    current: $('#' + next_id),
                    id: current_id
                });
                $('label[for="' + current_id + '-1"] img').attr('src', $('label[for="' + next_id + '-1"] img').attr('src'));
            }
            $('#picupload-' + id).attr('id', 'picupload-' + lastUpload);
            $.each(dataSource, function(ev, src) {
                src.current.attr('id', src.id);
            });
            $('#removefile-' + lastUpload).hide();
            $('label[for="picupload-' + lastUpload + '-1"] img').attr('src', '/images/add-camera.png');
            $('label[for="picupload-' + lastUpload + '-1"]').attr('for', 'picupload-' + lastUpload);
            $('label[for="picupload-' + (intId + 1) + '"] img').css('display', 'none');
            $('label[for="picupload-' + (intId + 1) + '"] label').css('display', 'flex');
            lastUpload--;
        }
        processImage();
    }

    function processImage() {
        $('#media-list label img').each(function() {
            if ($(this).attr('src') == '/images/add-camera.png') {
                $(this).css('position', 'relative');
            } else {
                $(this).css('position', 'absolute');
            }
        });
    }
</script>
<script>
    $(document).ready(function() {
        $('#sell-your-stuff-form').on('submit', function(ev) {
            var error = false;
            if ($('#picupload-1').val() === '' && skip == 0) {
                alertify.error('Please select at least one image of your product.');
                error = true;
            }
            if ($('#product-title').val() === '') {
                alertify.error('Please enter title for your product.');
                error = true;
            }
            if ($('#product-price').val() === '') {
                alertify.error('Please enter price for your product.');
                error = true;
            }
            if ($('#product-description').val() === '') {
                alertify.error('Please enter brief description about your product.');
                error = true;
            }
            if ($('#product-location').val() === '') {
                alertify.error('Please select your location.');
                error = true;
            }
            if (error) {
                ev.preventDefault();
                return false;
            }
        });
        $('.tab-profile').on('click', function(ev) {
            ev.preventDefault();
            $('.tab-profile').removeClass('active');
            $(this).addClass('active');
            $('.profile-grid-products').hide();
            $('#' + $(this).attr('action')).show();
        });
        $('.tab-profile[action="cat-2"]').click();
        $('#product-category').on('click', function(ev) {
            ev.preventDefault();
            jQuery.noConflict();
            $('#select-category').modal('show');
        });
    });

    function moveCat(type) {
        if (type === 'left') {
            $('#cat-list').scrollLeft($('#cat-list').scrollLeft() - 100);
        } else {
            $('#cat-list').scrollLeft($('#cat-list').scrollLeft() + 100);
        }
    }
</script>
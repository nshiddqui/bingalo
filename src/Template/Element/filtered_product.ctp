<?php if (!empty($products)) { ?>
    <?php
    $emptyImage = '/images/not-found.png';
    foreach ($products as $product) {
        if (!isset($product['images'][0]['image_url']) || empty($product['images'][0]['image_url'])) {
            continue;
        }
    ?>
        <div class="item" data-lat="<?= $product['product_lat'] ?>" data-lng="<?= $product['product_lng'] ?>">
            <div class="content">
                <a href="<?= $this->Url->build('/product/' . $product['id']) ?>">
                    <?= $this->Html->image(($product['images'][0]['image_url'] != '' ? $product['images'][0]['image_url'] : $emptyImage), ['alt' => $product['images'][0]['image_name']]) ?>
                    <div class="desc">
                        <h4 class="price">&#8362; <?= $this->Number->format($product['product_price']) ?></h4>
                        <h3 class="name"><?= rtrim($product['product_title']) ?></h3>
                        <?php if (!empty($product['product_city'] . $product['product_state'])) { ?>
                            <p><?= $product['product_city'] . ', ' . $product['product_state'] ?></p>
                        <?php } else { ?>
                            <p>NA</p>
                        <?php } ?>
                    </div>
                    <?php
                    $banner = null;
                    if (!empty($product['bump_data']) && !isset($bump)) {
                        $banner = 'BUMPED';
                    } else if ($product['product_is_sold'] == '1') {
                        $banner = 'SOLD';
                    } else if (date('Y-m-d', strtotime($product['created_at'])) == date('Y-m-d')) {
                        $banner = 'NEW';
                    }

                    if (!empty($banner)) {
                    ?>
                        <div class="dist-tag"><?= __($banner) ?></div>
                    <?php } ?>
                </a>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>
    <?= $this->Html->image('no-product-found.png', ['style' => 'width:100%;position:absolute']) ?>
<?php } ?>
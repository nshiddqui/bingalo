<div class="row" style="max-height: 678px;overflow:auto;">
    <?php foreach ($products as $product) {
        if (!isset($product['images'][0]['image_url']) || empty($product['images'][0]['image_url'])) {
            continue;
        }
    ?>
        <div class="col-2 p-1">
            <div class="card p-0" style="border-color: #2a80c7;background: #F0F1F3">
                <div class="card-body p-0">
                    <a href="<?= $this->Url->build('/product/' . $product['id']) ?>">
                        <div class="row p-0">
                            <div class="col-md-12 p-0 text-center" style="background: #E5E7EB;overflow:hidden;">
                                <?php
                                $banner = null;
                                if ($product['product_is_sold'] == '1') {
                                    $banner = 'SOLD';
                                } else if (date('Y-m-d', strtotime($product['created_at'])) == date('Y-m-d')) {
                                    $banner = 'NEW';
                                }

                                if (!empty($banner)) {
                                ?>
                                    <div class="dist-tag"><?= __($banner) ?></div>
                                <?php } ?>
                                <div class="product-image-preview" lazy="1" data-src="<?= (@$product['images'][0]['image_url'] != '' ? $product['images'][0]['image_url'] : '') ?>"></div>
                            </div>
                            <div class="col-md-12 p-2" style="height: 95px;overflow: hidden;">
                                <p class="font-weight-bold mb-0 text-dark" style="font-size: 15px;"><?= rtrim($product['product_title']) ?></p>
                                <h6 style="font-weight: bold;color:#1470d6;">&#8362; <?= $this->Number->format($product['product_price']) ?></h6>
                                <p class="font-weight-bold mb-0 text-secondary" style="font-size: 10px;height: 30px;overflow: hidden;"><?= $product['product_description']; ?></p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
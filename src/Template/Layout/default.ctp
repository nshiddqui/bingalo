<?php
$cakeDescription = 'Bingalo';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css') ?>
    <?= $this->Html->css('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css') ?>
    <?= $this->Html->css('style1') ?>
    <?= $this->Html->css('responsive') ?>
    <?= $this->Html->css('alertify.min') ?>
    <?= $this->Html->script('jquery.min') ?>
    <?= $this->Html->script('bootstrap/bootstrap.min') ?>
    <?= $this->Html->script('alertify.min') ?>
    <?= $this->Html->script('https://code.jquery.com/ui/1.12.1/jquery-ui.js') ?>
    <?= $this->Html->script('https://www.google.com/recaptcha/api.js') ?>
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js') ?>
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js') ?>
    <?= $this->Html->script('https://maps.googleapis.com/maps/api/js?key=' . $googleApiKey . '&libraries=places'); ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <style>
        .pac-container {
            z-index: 9999999;
        }

        .pac-item span {
            font-size: 14px;
            top: 0;

        }

        header .right .msg img {
            height: 31px;
            margin-top: -1px;
            width: 33px;
        }

        .login-popup.language-popup .head h2 {
            font-size: 20px;
            font-weight: bold;
        }

        .g-recaptcha>div {
            margin: auto;
        }

        #loginForm div.form-group.checkbox {
            display: inline-block;
        }

        img[data-src] {
            object-fit: contain;
        }

        div[data-src] {
            background-size: contain;
            background-image: url("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCAAKAAoDASIAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAf/xAAUEAEAAAAAAAAAAAAAAAAAAAAA/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwC+gCv/2Q==");
        }

        [data-src] {
            display: inline-block;
            width: 2rem;
            height: 2rem;
            vertical-align: text-bottom;
            background-color: currentColor;
            border-radius: 50%;
            opacity: 0;
            /* -webkit-animation: spinner-grow .75s linear infinite;
            animation: spinner-grow .75s linear infinite; */
        }

        .filter .list-grid a[data-src] {
            border-radius: 50% !important;
        }

        .profile img[data-src] {
            margin: 0;
            padding: 0;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <script>
        var BASE_URL = '<?= $this->Url->build('/', true) ?>';
        var LOCAL_INFORMATION = <?= json_encode($localInformation) ?>;
        var RECENT_ITEMS = <?= json_encode(!empty($searchRecentWise) ? $searchRecentWise : []) ?>;
        var TRENDING_ITEMS = <?= json_encode(!empty($searchRankWise) ? $searchRankWise : []) ?>;
        var IS_LOGIN = <?= isset($authUser) ? 1 : 0 ?>;
        var FIREBASE_SETUP = <?= json_encode($firebaseSetup) ?>;
        const lang = '<?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'hb' : 'en') ?>';
    </script>
</head>

<body class="<?= $this->fetch('bodyClass') ? $this->fetch('bodyClass') : 'search' ?>">
    <?php if (isset($authUser)) { ?>
        <div class="modal fade" id="change-email" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Change Your Email') ?></h5>
                        <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="display: contents;">
                            <span aria-hidden="true" style="font-size: 30px;position: absolute; right: 15px;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container px-2">
                            <div class="row text-center">
                                <div class="col-12">
                                    <?= $this->Form->create(null, ['id' => 'form-change-email']) ?>
                                    <p style="font-size: 15px; color: #a5a5a5;"><?= __('Your Current Login Email Address is') ?> <a href="#" class="text-primary"><?= $authUser['email'] ?></a> <?= __('to change it, please enter a new Email Address below') ?></p>
                                    <?= $this->Form->control('change_email_id', ['type' => 'email', 'onchange' => "$('#change-emails').val(this.value)", 'placeholder' => __('Enter Your New Email'), 'label' => false, 'required' => true]) ?>
                                    <?= $this->Form->button(__('Send OTP'), ['label' => false]) ?>
                                    <?= $this->Form->end() ?>
                                    <?= $this->Form->create(null, ['id' => 'otp-change-email', 'url' => ['controller' => 'Authorized', 'action' => 'changeEmail'], 'style' => 'display:none;']) ?>
                                    <?= $this->Form->control('change_emails', ['type' => 'hidden']) ?>
                                    <?= $this->Form->control('change_email_otp', ['placeholder' => __('Enter OTP'), 'label' => false, 'required' => true]) ?>
                                    <?= $this->Form->button(__('Verify OTP'), ['label' => false]) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Change Your Password') ?></h5>
                        <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 15px;display: contents;">
                            <span aria-hidden="true" style="font-size: 30px;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container px-2">
                            <div class="row text-center">
                                <div class="col-12">
                                    <?= $this->Form->create(null, ['url' => '/change-password']) ?>
                                    <?= $this->Form->control('old_password', ['placeholder' => __('Old Password'), 'type' => 'password', 'label' => false, 'required' => true]) ?>
                                    <?= $this->Form->control('new_password', ['placeholder' => __('New Password'), 'type' => 'password', 'label' => false, 'required' => true]) ?>
                                    <?= $this->Form->control('confirm_password', ['placeholder' => __('Confirm Password'), 'type' => 'password', 'label' => false, 'required' => true]) ?>
                                    <?= $this->Form->button(__('Change Password'), ['label' => false]) ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="login-popup">
            <div class="overlay"></div>
            <div class="popup">
                <div class="close"><?= $this->Html->image('cross.png', ['style' => 'height:15px']) ?></div>
                <div class="signup-form show">
                    <?= $this->Form->create(null, ['url' => '/updateProfile', 'type' => 'file']) ?>
                    <div class="d-none">
                        <?= $this->Form->control('image', ['type' => 'file']) ?>
                    </div>
                    <div style="text-align: center;width: 100%;">
                        <label for="image"><?= $this->Html->image('upload-pic.png', ['style' => 'height: 25px; width: 25px; position: absolute; right: 140px; top: 40px; cursor: pointer;']); ?></label>
                        <?= $this->Html->image((!empty($authUser['image']) ? $authUser['image'] : 'profile-upload.png'), ['style' => 'height: 100px;width: 100px;border-radius: 100%;object-fit: cover;', 'id' => 'image-preview']) ?>
                    </div>
                    <?= $this->Form->control('full_name', ['placeholder' => __('Full Name'), 'value' => $authUser['full_name'], 'label' => false]) ?>
                    <div class="position-relative">
                        <?= $this->Form->control('email', ['placeholder' => __('Your Email'), 'readonly' => true, 'value' => $authUser['email'], 'label' => false]) ?>
                        <a href="#change-email" data-toggle="modal" class="position-absolute btn btn-primary" onclick="$('.close').click();" style="top: 0;right: 0;"><?= __('Change Email') ?></a>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <?= $this->Form->radio('language', ['0' => 'English'], ['class' => 'form-check-input', 'checked' => ($authUser['language'] == 0)]) ?>
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <?= $this->Form->radio('language', ['1' => 'Hebrew'], ['class' => 'form-check-input', 'checked' => ($authUser['language'] == 1)]) ?>
                        </label>
                    </div>
                    <div class="input">
                        <?= $this->Form->submit(__('Update Profile')) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="login-popup overflow-scoll">
            <div class="overlay"></div>
            <div class="popup">
                <div class="close"><?= $this->Html->image('cross.png', ['style' => 'height:15px']) ?></div>
                <h5 class="text-center font-weight-bold mb-4"><?= __('Login') ?></h5>
                <div class="social-login">
                    <?php
                    echo $this->Form->postLink(
                        '<h4 style="color: #000;"><img src="/images/google.png">' . __('Continue with Google') . '</h4>',
                        ['controller' => 'authenticated', '?' => ['provider' => 'Google'], '_ssl' => true],
                        ['escape' => false, 'class' => 'google-login']
                    );
                    echo $this->Form->postLink(
                        '<h4><img src="/images/fb (2).png">' . __('Continue with Facebook') . '</h4>',
                        ['controller' => 'authenticated', '?' => ['provider' => 'Facebook'], '_ssl' => true],
                        ['escape' => false, 'class' => 'fb-login']
                    );
                    echo $this->Form->postLink(
                        '<h4><img src="/images/twitter (2).png">' . __('Continue with Twitter') . '</h4>',
                        ['controller' => 'authenticated', '?' => ['provider' => 'Twitter'], '_ssl' => true],
                        ['escape' => false, 'class' => 'twitter-login']
                    );
                    echo $this->Form->postLink(
                        '<h4><img src="/images/apple (2).png">' . __('Continue with Apple') . '</h4>',
                        ['controller' => 'authenticated', '?' => ['provider' => 'Apple'], '_ssl' => true],
                        ['escape' => false, 'class' => 'apple-login']
                    );
                    ?>
                </div>
                <hr>
                <div class="signin-form show">
                    <?= $this->Form->create(null, ['url' => '/login', 'id' => 'loginForm']) ?>
                    <?= $this->Form->control('email', ['placeholder' => __('Your Email'), 'label' => false, 'required' => true]) ?>
                    <?= $this->Form->control('password', ['placeholder' => __('Password'), 'label' => false, 'required' => true]) ?>
                    <label for="remember-me" style="cursor: pointer;"><?= $this->Form->control('remember_me', ['type' => 'checkbox', 'label' => false, 'style' => 'width: 19px; height: 19px;']) ?> <?= __('Remember me') ?></label>
                    <label class="float-right" style="cursor: pointer;"><?= $this->Html->image('invisible.png', ['style' => 'height: 15px;', 'id' => 'show-password-image']) ?> <div id="show-password" class="d-inline-block" current="show" lang="<?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'hb' : 'en') ?>"><?= __('Show Password') ?></div></label>
                    <div class="text-center my-2">
                        <div class="g-recaptcha" data-sitekey="6LcJ-jccAAAAAKLmfa65fV74PDwTBP6lTGZsfGsO"></div>
                    </div>
                    <div class="input">
                        <?= $this->Form->submit(__('Sign In')) ?>
                    </div>
                    <?= $this->Form->end() ?>
                    <div class="mt-1 text-center">
                        <?= $this->Html->link(__('Forgot Password?'), '#', ['class' => 'text-black font-weight-bold', 'id' => 'forgot-login']) ?>
                        <p class="mt-3 text-black font-weight-bold"><?= __("You don't have account?") ?> <?= $this->Html->link(__('Click Here'), '#', ['id' => 'registerd-modal']) ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="signup-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="popup modal-dialog modal-dialog-centered p-0" role="document">
                <div class="modal-content">
                    <div class="modal-header border-0">
                        <div class="w-100" id="adds-signup">
                            <?= $this->Html->image('btn_cancel.png', ['style' => 'height: 20px;cursor: pointer;', 'class' => 'mr-2', 'onclick' => "$('#adds-signup').remove();"]) ?>
                            <?= $this->Html->image('install_applogo.png', ['style' => 'height: 60px;']) ?>
                            <?= $this->Html->image('btn_install.png', ['style' => 'height: 40px;cursor: pointer;', 'class' => 'float-right mt-3']) ?>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="close" data-dismiss="modal" style="margin-top: -20px;"><?= $this->Html->image('cross.png', ['style' => 'height:15px']) ?></div>
                        <p class="text-center" style="font-size: 17px;"><?= __('Already a member?') ?> <?= $this->Html->link(__('Login'), '#', ['id' => 'login-model', 'class' => 'font-weight-bold']) ?></p>
                        <hr>
                        <h5 class="text-center font-weight-bold pb-3 m-auto"><?= __('Sign Up') ?></h5>
                        <div class="social-login">
                            <?php
                            echo $this->Form->postLink(
                                '<h4 style="color: #000;"><img src="/images/google.png">' . __('Continue with Google') . '</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Google'], '_ssl' => true],
                                ['escape' => false, 'class' => 'google-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/fb (2).png">' . __('Continue with Facebook') . '</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Facebook'], '_ssl' => true],
                                ['escape' => false, 'class' => 'fb-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/twitter (2).png">' . __('Continue with Twitter') . '</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Twitter'], '_ssl' => true],
                                ['escape' => false, 'class' => 'twitter-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/apple (2).png">' . __('Continue with Apple') . '</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Apple'], '_ssl' => true],
                                ['escape' => false, 'class' => 'apple-login']
                            );
                            ?>
                        </div>
                        <hr>
                        <div class="signup-form show">
                            <?= $this->Form->create(null, ['url' => '/signup', 'type' => 'file']) ?>
                            <div class="d-none">
                                <?= $this->Form->control('image', ['type' => 'file']) ?>
                            </div>
                            <div style="text-align: center;width: 100%;">
                                <label for="image" style="border-radius: 100%;cursor: pointer;">
                                    <?= $this->Html->image('profile-upload.png', ['style' => 'height: 100px;width: 100px;border-radius: 100%;', 'id' => 'image-preview']) ?>
                                </label>
                            </div>
                            <?= $this->Form->control('email', ['placeholder' => __('Email address'), 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->control('password', ['placeholder' => __('Password'), 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->control('full_name', ['placeholder' => __('Full Name'), 'label' => false, 'required' => true]) ?>
                            <div class="input">
                                <?= $this->Form->submit(__('Sign Up'), ['style' => 'background: #2170b7; color: #fff; padding-top: 7px; cursor: pointer; width: 100%; padding: 5px 10px; border: 1px solid #ddd; margin-bottom: 10px; border-radius: 4px; font-size: 14px;']) ?>
                            </div>
                            <?= $this->Form->end() ?>
                            <div class="terms">
                                <p><?= __('By clicking on "Sign Up", you agree to the name') ?> <a href="<?= (isset($page_status[3]) ? '/pages/view/3' : '#') ?>"><?= __('Terms & Conditions') ?></a> <?= __('and') ?> <a href="<?= (isset($page_status[2]) ? '/pages/view/2' : '#') ?>"><?= __('Privacy Policy') ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="forgot-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="popup modal-dialog modal-dialog-centered p-0" role="document" style="max-height: 300px;min-height: calc(100% - 17.5rem);">
                <div class="modal-content border-0">
                    <div class="modal-body">
                        <div class="w-100" id="adds-forgot">
                            <?= $this->Html->image('btn_cancel.png', ['style' => 'height: 20px;cursor: pointer;', 'class' => 'mr-2', 'onclick' => "$('#adds-forgot').remove();"]) ?>
                            <?= $this->Html->image('install_applogo.png', ['style' => 'height: 60px;']) ?>
                            <?= $this->Html->image('btn_install.png', ['style' => 'height: 40px;cursor: pointer;', 'class' => 'float-right mt-3']) ?>
                        </div>
                        <hr>
                        <div class="close" data-dismiss="modal"><?= $this->Html->image('cross.png', ['style' => 'height:15px']) ?></div>
                        <h3 class="text-center font-weight-bold" style="font-size: 25px;margin-top: 30px;">
                            <?= __('Forgot Password') ?>
                        </h3>
                        <p class="text-center font-weight-bold" style="font-size: 17px;"><?= __('Please enter a valid e-mail address. we will send you further instruction to change your Password') ?></p>
                        <hr>
                        <?= $this->Form->create(null, ['url' => 'forgot-password']) ?>
                        <?= $this->Form->control('email', ['placeholder' => __('Email address'), 'label' => false, 'required' => true]) ?>
                        <div class="input">
                            <?= $this->Form->submit(__('Send reset link'),  ['class' => 'btn btn-dark w-100 p-2 rounded-pill mb-3', 'style' => 'background: black;color:white']) ?>
                        </div>
                        <?= $this->Form->end() ?>
                        <p class="text-center font-weight-bold"><?= $this->Html->link(__('Back to login'), '#', ['id' => 'back-to-login', 'class' => 'text-black']) ?></p>
                    </div>
                </div>
            </div>
        </div>
        <script>
            document.getElementById("loginForm").addEventListener("submit", function(evt) {

                var response = grecaptcha.getResponse();
                if (response.length == 0) {
                    //reCaptcha not verified
                    if (lang == 'en') {
                        alert("Bingalo.com cannot Log you in as you have not Filled out the \"I'm not a Robot\" Field.");
                    } else {
                        alert('Bingalo.com לא יכול להתחבר מכיוון שלא מילאת את השדה "אני לא רובוט"');
                    }
                    evt.preventDefault();
                    return false;
                }
                //captcha verified
                //do the rest of your validations here

            });
        </script>
    <?php } ?>
    <div class="login-popup language-popup">
        <div class="overlay"></div>
        <div class="popup">
            <div class="head">
                <h2><?= __('Select Language') ?></h2>
            </div>
            <div class="inner">
                <div class="btns">
                    <a href="javascript:translateLanguage('en');" class="choose-language"><?= __('English') ?></a>
                    <a href="javascript:translateLanguage('en_HB');" class="choose-language"><?= __('Hebrew') ?></a>
                </div>
                <div class="dowload-app">
                    <p><?= __("Download our app. It's awesome") ?></p>
                    <div class="app-btn">
                        <a href="<?= $app_info_data->playstore ?>">
                            <img src="images/google-play.png" alt="">
                        </a>
                        <a href="<?= $app_info_data->appstore ?>">
                            <img src="images/app-store.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="login-popup download-app">
        <div class="overlay"></div>
        <div class="popup" style="border-radius: 15px;">
            <div class="inner">
                <!-- Modal content -->
                <div class="available-modal-content">
                    <span class="available-close" style="position: absolute;top: 5px;right: 5px;" onclick="$('.login-popup.download-app').removeClass('visible');"><?= $this->Html->image('Group 116.png', ['style' => 'height:25px']) ?></span>
                    <p style="font-weight: bolder;font-size: 15px;margin-right: 25px;text-align: center;margin-bottom: 5px;"><?= __('Use this Features in the Bingalo App') ?> </p>
                    <p style="font-size: 12px;color:#c1c1c1;margin-right: 25px;text-align: center;"><?= __('To use this features on Bingalo, please download the app') ?></p>
                    <div style="margin-right:25px;text-align:center;padding:5px;">
                        <?= $this->Html->image('img_appstore.png', ['url' => 'https://apps.apple.com/us/app/bingalo/id1351229487', 'style' => 'height: 40px; display: block; margin: auto;margin-top:4px;']) ?>
                        <?= $this->Html->image('img_playstore.png', ['style' => 'height: 40px; display: block; margin: auto;margin-top:4px;']) ?>
                    </div>
                </div>
                <!-- <div class="dowload-app" style="text-align: center;">
                    <h5>Download our app to use this feature.</h5>
                    <div class="app-btn" style="text-align: center;">
                        <a href="<?= $app_info_data->playstore ?>">
                            <img src="/images/google-play.png" style="width: 45%;" alt="">
                        </a>
                        <a href="<?= $app_info_data->appstore ?>">
                            <img src="/images/app-store.png" style="width: 45%;" alt="">
                        </a>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <header>
        <div class="container">
            <div class="row">
                <div class="logo">
                    <h5><a href="/"><?= $this->Html->image((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'logo_herbew.png' : 'logo.png'), ['style' => 'height:35px;']) ?></a></h5>
                </div>


                <div class="search-field">
                    <form action="/" id="queryForm">
                        <div class="input">
                            <?= $this->Html->image('search-icon-2.png') ?>
                            <input type="text" autocomplete="off" name="name" id="searchItem" value="<?= isset($filter['name']) ? $filter['name'] : '' ?>" placeholder="<?= __('What are you looking for?') ?>">
                        </div>
                        <div class="input">
                            <?= $this->Html->image('map-marker.png') ?>
                            <input type="text" name="address" id="product-search-location" value="<?= isset($filter['address']) ? $filter['address'] : '' ?>" placeholder="<?= __('Enter city or zip code') ?>">
                            <input type="hidden" id="product-search-location-lat" value="<?= isset($filter['product_search_lat']) ? $filter['product_search_lat'] : '' ?>" name="product_search_lat">
                            <input type="hidden" id="product-search-location-lng" value="<?= isset($filter['product_search_lng']) ? $filter['product_search_lng'] : '' ?>" name="product_search_lng">
                        </div>
                        <div class="input search">
                            <?= $this->Html->image('search.png', ['onclick' => "$('#queryForm').submit()"]) ?>
                            <input type="submit" name="" value="" id="">
                        </div>
                    </form>
                </div>
                <div class="right">
                    <div class="msg"><a href="/chat/" download-app><?= $this->Html->image('msg-icon.png') ?></a></div>
                    <div class="profile">
                        <a href="/profile" login="1"><?= $this->Html->image((isset($authUser['image']) && !empty($authUser['image']) ? $authUser['image'] : 'profile.png'), ['style' => 'object-fit: cover;']) ?></a>
                        <?php if (isset($authUser)) { ?>
                            <a href="#" login="1" id="click-profile-dropdown"><?= $this->Html->image('drop-down-arrow.png', ['style' => 'opacity: 0.4;height: 25px;width: 25px;']) ?></a>
                            <div id="pop-dropdown-home">
                                <ul>
                                    <li><a href="/profile"><?= __('View Your Profile') ?></a></li>
                                    <li><a href="javascript:void(0)" id="update-profile"><?= __('Update Your Profile') ?></a></li>
                                    <li><a href="javascript:void(0)" id="language-change"><?= __('Change My Language') ?></a></li>
                                    <li><a href="#change-password" data-toggle="modal"><?= __('Change Your Password') ?></a></li>
                                    <li><a href="/logout"><?= __('Logout') ?></a></li>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="sell-btn">
                        <a href="/sell-your-stuff" login="1"><?= __('Sell Your Stuff') ?> <?= $this->Html->image('camera-icon.png') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>
    <footer>

        <div class="top">
            <div class="logo">
                <h3><?= $this->Html->image((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'logo_herbew.png' : 'logo.png'), ['style' => 'height:35px;']) ?></h3>
            </div>
            <div class="right-band">
                <ul>
                    <li><a href="<?= $app_info_data->appstore ?>"><?= $this->Html->image('apple.png') ?> <?= __('App Store') ?></a></li>
                    <li><a href="<?= $app_info_data->playstore ?>"><?= $this->Html->image('google-play-2.png') ?> <?= __('Google Pay') ?></a></li>
                    <li><a href="#" id="language-change" onclick="$('.login-popup.language-popup').addClass('visible');"><?= $this->Html->image('msg-icon.png') ?> <?= (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'Herbew' : 'English') ?></a></li>
                    <li><a href="#"><?= $this->Html->image('globe.png') ?> <?= __('United States') ?></a></li>
                </ul>
            </div>
        </div>

        <div class="middle-band text-center">
            <div class="inner">
                <ul class="social">
                    <li><a href="<?= $app_info_data->facebook ?>"><?= $this->Html->image('fb.png') ?></a></li>
                    <li><a href="<?= $app_info_data->twitter ?>"><?= $this->Html->image('twitter.png') ?></a></li>
                    <li><a href="<?= $app_info_data->instagram ?>"><?= $this->Html->image('instagram.png') ?></a></li>
                </ul>
                <ul class="nav">
                    <li><a href="<?= (isset($page_status[1]) ? '/pages/view/1' : '#') ?>"><?= __('FAQ') ?></a></li>
                    <li><a href="<?= (isset($page_status[4]) ? '/pages/view/4' : '#') ?>"><?= __('Safety Tips') ?></a></li>
                    <li><a href="<?= (isset($page_status[5]) ? '/pages/view/5' : '#') ?>"><?= __('Press') ?></a></li>
                    <li><a href="#"><?= __('Jobs') ?></a></li>
                    <li><a href="<?= (isset($page_status[8]) ? '/pages/view/8' : '#') ?>"><?= __('Prohibited Items') ?></a></li>
                    <li><a href="#"><?= __('Sitemap') ?></a></li>
                </ul>
            </div>
        </div>
        <div class="bottom-band">
            <p><?= __('Buy & Sell secondhand products in United States quickly, safely and locally on the free Name app. Start selling your used stuff today!') ?></p>
            <div class="copy">
                <p>&copy; 2020. <?= __('All rights reserved') ?>. <?= __('Name') ?> <a href="<?= (isset($page_status[3]) ? '/pages/view/3' : '#') ?>"><?= __('Terms & conditions') ?></a> <?= __('and') ?> <a href="<?= (isset($page_status[2]) ? '/pages/view/2' : '#') ?>"><?= __('Privacy Policy') ?></a></p>
            </div>
        </div>

    </footer>
    <?= $this->Html->script('main') ?>
    <script>
        function initializePlaceSearch() {
            // Create the autocomplete object, restricting the search predictions to
            // geographical location types.
            autocomplete = new google.maps.places.Autocomplete(
                document.getElementById("product-search-location"), {
                    types: ["geocode"]
                }
            );
            autocomplete.addListener("place_changed", () => {
                const place = autocomplete.getPlace();
                $('#product-search-location-lat').val(place.geometry.location.lat());
                $('#product-search-location-lng').val(place.geometry.location.lng());
                $('#queryForm').submit();
            });
        }
        google.maps.event.addDomListener(window, 'load', initializePlaceSearch);
    </script>
    <script>
        function translateLanguage(val) {
            setCookie('lang', val);
            window.location.reload();
        }
    </script>
</body>

</html>
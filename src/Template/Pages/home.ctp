<?= $this->Html->css('https://cdn.jsdelivr.net/npm/flickity@latest/dist/flickity.min.css') ?>
<?= $this->Html->script('https://cdn.jsdelivr.net/npm/flickity@2.2.1/dist/flickity.pkgd.min.js') ?>
<?php
$emptyImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXy8vK8vLz19fW5ubm2trbl5eXZ2dni4uLDw8PJycnW1tbr6+vNzc3R0dHp6ene3t6up7FsAAAFqklEQVR4nO2bi5Ksqg6GJUFugrz/254E8NKzp3p6T3fXHHv/X82yFDHwQxJsdU0TAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC8C6I/bvtL+/Tt7jOk8ho7Bw93jIqjqd627/Oy7a5lfUl3DL/CzIk1z4+2bQNRscu5yNldlrf1Ff0hY15h5sS/6Fj1Mo/8ReF++FKFpH/UAmP4GB0RSrQX687hhP1oFGwhRd76U5Ut2A6zJ9t6dVdINK4WhdvpofDo0xMK1+DnYIInZ0yYtYU1GjYtQmiJxqQpej0oct5t7ZELlKSgtoLcL6AihsJmvZmRyKIc2tT4ILPmg+GgtikWGgqT1FNDqrDIabXZFS7ZaM+eVWgjZzGcOWfWFldrksus07taOcXBirBFaqRo4zYfhaMpxViNO8PFFbmAUuSYh/FFzfRSvV4k2YmqDc5FCUGJw9gVSnl2Sdsgx8GUZGyirnBlLnKm/F5iV8i8ElURJ1ttJ4uvkHRLot4Y2RUBUhq1lqSGuisM4kPeZr1gprF7eKlmESl1UrCwKJoWFkmB1e2zCtsULqJAG4lN4dTaU3evUlubF/O/T6tjDjMdWxkv77UXVbo962hOTXfvB008JlEEqEe3414mvW8d24zPlZpucc2oikSrpBcdi5ZCD4VVZZAJfThaONam0Ot4U2v6SYWJjm3R7DHXJJE1S1Mt9y/SUuVcnXPVhF1hCy4Z+rk5YZ+0k8LNjO9DJBPSEop3JfJ5DiXYpSxwU9hsal9UYeKibbptVH+v0N0opNlYGyT85u6pXaFj0xmJRPRMQ6F25huFq5gxYkazlPSxzYQMlJVEE88KxQutCanP4dTb6wrL1uaLFU5s5uGlYw61RrXN6YiOMNsUbnOY7Y2XGjMPL22n2vwsNixfvFSGsUy7l7Y5VINtDlt8H22+SuEIPo0z33eTZJp1BENMu5duCpeeYMUYnRbqfoH00rc+16D5s7bppvMcykGr37209mvWEYetsSX+fvH/VqHmHHFVFi/tuVR2NYxGLvXTV4WS7n1Lvq4pHAOuuokkUbfkYQK7tsql5h63CsXwlJtC1vZa6m2GWvNi/plcyt0nbuJQlqsSbLKy1EoHg6xHPNbDcqxNm0LtzWJslFM6k7IIboE6zHDwbVrGgLDUZFlL165QQne2uhBHWZRlDrONLQ91Z5CxlZXapieW/JSkU0mDbUl+35YQisRHnCX1uVzmpQea3LDE4/7Cp26hjlP9dodq3n8upBCymumGx9yqbdlGOVG7EZpjiFVupDLNZXLattRb06pZNocQ52fu206/0U7bHtv6b522NW0vPq49LEznFLTXOcycio9C2o2cy3ZDdKr+Phb9haMh+M5G/hSJk6D3nvWt4/inUI0m5PVzBU7vDwQAAAAAAPBG1vkhlove8dFq+EHC8rO5/0OW7Snfz3C44ixSEoXxIYxh/9fd/QWkD7LpIRKzu+AkNoWP1by6QprW5e7v48srLJbZ3HuGc3GFlFtKvfeY6uIK57Fm3HnUeG2F+7svvvm+hM5Hn6LwnFfbu+7j6NoKt3em509iyLPhQ+K1FU5TV3h+Ba0CzUnixRXqe0W98Tyd8EP0JvHiCidaUozuVO63O/JN4tUVfnnkfwjcJV5f4U2pP/+m6hI/SuGtwCHxMxSG9mb/q8Au8SMUBmb9EugfAkVi/Yjfh0EXjPKdwPa53/UVBu4O+d2Dm49QGO49kvoEhXcFfoLC+wKvr/D4NvJjFX6bXqDwGvxnFJbwA/Xqd20/PtK/+J33Q/2+rMLCnJaHiOaSCqfKj74hNcZe8hUpxYdfkfIz32j/IeTiT4m0E5/5f2d/ymPvR/FpKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAODz+B9bJTiKWY5GtAAAAABJRU5ErkJggg==';
?>
<style>
    <?php if ($is_mobile) { ?>.bottom-banner img {
        width: 100% !important;
        height: calc(100vw / 1.2) !important;
    }

    .carousel img {
        width: 100% !important;
        height: calc(100vw / 3.5) !important;
    }

    .carousel .slide {
        width: 100%;
        height: calc(100vw / 3.5) !important;
    }

    .bottom-banner .slide {
        width: 100%;
        height: calc(100vw / 1.2) !important;
    }

    .filter-search-sort {
        padding-bottom: 0;
        margin-top: -40px;
    }

    .list-grid {
        overflow: auto !important;
        width: 100%;
        height: auto;
        overflow: visible;
        white-space: nowrap;
    }

    .no-paading-top {
        padding-top: 0px;
    }

    .no-padding-lr {
        padding-left: 0px;
        padding-right: 0px;
    }

    <?php } else {  ?>.carousel img {
        width: 100% !important;
        height: calc(100vw / 5.2) !important;
    }

    .carousel .slide {
        width: 100%;
        height: calc(100vw / 5.2) !important;
    }

    .listings .item img[data-src] {
        margin: 70px auto;
    }

    .listings .item .content {
        text-align: center;
    }

    .carousel img[data-src] {
        border: unset !important;
        border-radius: unset !important;
        border-top: unset !important;
        -webkit-animation: unset !important;
        animation: unset !important;
    }

    #top-listings::-webkit-scrollbar {
        display: none;
    }

    #top-listings {
        -ms-overflow-style: none;
        /* IE and Edge */
        scrollbar-width: none;
        /* Firefox */
    }

    ul.sub-categories {
        display: none;
    }

    ul.sub-categories li {
        margin-left: 35px;
        border-bottom: 1px solid #ddd;
        padding-bottom: 5px;
        color: #1470d6;
    }

    <?php } ?>
</style>
<div id="kmDialog" style="line-height: 1.15;
     direction: ltr;
     text-align: left;
     text-size-adjust: 100%;
     -webkit-font-smoothing: antialiased;
     font-family: Arial,Helvetica,sans-serif;
     -webkit-tap-highlight-color: transparent;
     color: #002f34;
     position: fixed;
     top: 100px;
     left: 50%;
     transform: translateX(-50%);
     width: auto;
     animation: _1RXhN .3s ease;
     z-index: 9999;
     min-width: 160px;
     display: none;">
    <button style="direction: ltr;
            text-size-adjust: 100%;
            -webkit-font-smoothing: antialiased;
            -webkit-tap-highlight-color: transparent;
            margin: 0;
            font-family: Arial,Helvetica,sans-serif;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            box-sizing: border-box;
            background: none;
            outline: none;
            text-transform: lowercase;
            font-weight: 700;
            cursor: pointer;
            position: relative;
            overflow: hidden;
            line-height: normal;
            text-decoration: none;
            font-size: 14px;
            height: 40px;
            width: 100%;
            color: #002f34;
            padding: 0 10px;
            border-radius: 50px;
            box-shadow: 0 2px 8px 0 rgba(0,0,0,.15);
            background-color: #fff;
            border: none;">
        <span style="direction: ltr;
              text-size-adjust: 100%;
              -webkit-font-smoothing: antialiased;
              -webkit-tap-highlight-color: transparent;
              font-family: Arial,Helvetica,sans-serif;
              text-transform: lowercase;
              font-weight: 700;
              cursor: pointer;
              line-height: normal;
              font-size: 14px;
              top:0;
              color: #002f34;" id="kmDialogContent">Back to top</span>
    </button>
</div>
<!-- <div class="banner">
    <div class="container">
        <div class="row">
            <div class="text">
                <h1>Get the free app!</h1>
                <p>Make money selling what you don't need and <br />find great deals nearby</p>
                <div class="playstore">
                    <a class="g-play" href="#"><img src="images/google-play.png" alt=""></a>
                    <a class="app-store" href="#"><img src="images/app-store.png" alt=""></a>
                </div>
            </div>

        </div>
    </div>
</div> -->
<div class="product-listing no-paading-top">
    <div class="container no-padding-lr">
        <div class="carousel">
            <?php if ($topBanner->count()) { ?>
                <?php foreach ($topBanner as $banner) { ?>
                    <div class="slide"><a href="<?= $banner->link ?>" target="_BLANK" title="<?= $banner->title ?>"><?= $this->Html->image($banner->image, ['style' => '', 'lazy' => false]) ?></a></div>
                <?php } ?>
            <?php } else { ?>
                <div class="slide"><a href="https://www.google.com" target="_BLANK"><?= $this->Html->image('ban1.png', ['style' => '']) ?></a></div>
                <div class="slide"><a href="https://www.youtube.com" target="_BLANK"><?= $this->Html->image('ban2.png', ['style' => '']) ?></a></div>
                <div class="slide"><a href="https://www.facebook.com" target="_BLANK"><?= $this->Html->image('ban3.png', ['style' => '']) ?></a></div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="product-listing filter-search-sort">
    <div class="container">
        <div class="sort-filter">
            <form action="#">
                <?php if ($is_mobile) { ?>
                    <div class="row">
                        <div class="col-6">
                            <h4><?= __('Sort') ?>:</h4>
                            <?=
                            $this->Form->control('sort_type', ['options' => [
                                'DESC' => __('Newset'),
                                'ASC' => __('Oldest'),
                            ], 'default' => @$filter['sort_type'], 'class' => 'sort sortByRedirect', 'date-sortByRedirect' => true, 'search-type' => 'sort_type', 'label' => false])
                            ?>
                        </div>
                        <div class="col-6">
                            <h4><?= __('Within') ?>:</h4>
                            <?=
                            $this->Form->control('max_distance', ['options' => [
                                '10' => '10 ' . __('Miles'),
                                '20' => '20 ' . __('Miles'),
                                '50' => '50 ' . __('Miles'),
                                '100' => '100 ' . __('Miles'),
                            ], 'default' => @$filter['max_distance'], 'class' => 'sort sortByRedirect', 'date-sortByRedirect' => true, 'search-type' => 'max_distance', 'label' => false])
                            ?>
                        </div>
                        <div class="col-6">
                            <h4><?= __('Min Price') ?></h4>
                            <input type="text" class="sortByAjax" search-type="min_price" name="" id="" placeholder="&#8362; <?= __('Min') ?>">
                        </div>
                        <div class="col-6">
                            <h4><?= __('Max Price') ?></h4>
                            <input type="text" class="sortByAjax" search-type="max_price" name="" id="" placeholder="&#8362; <?= __('Max') ?>">
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="left">
                        <div class="input">
                            <h4><?= __('Sort') ?>:</h4>
                            <?=
                            $this->Form->control('sort_type', ['options' => [
                                'DESC' => __('Newset'),
                                'ASC' => __('Oldest'),
                            ], 'default' => @$filter['sort_type'], 'class' => 'sort sortByRedirect', 'date-sortByRedirect' => true, 'search-type' => 'sort_type', 'label' => false])
                            ?>
                        </div>

                        <div class="input">
                            <h4><?= __('Within') ?>:</h4>
                            <?=
                            $this->Form->control('max_distance', ['options' => [
                                '10' => '10 ' . __('Miles'),
                                '20' => '20 ' . __('Miles'),
                                '50' => '50 ' . __('Miles'),
                                '100' => '100 ' . __('Miles'),
                            ], 'default' => @$filter['max_distance'], 'class' => 'sort sortByRedirect', 'date-sortByRedirect' => true, 'search-type' => 'max_distance', 'label' => false])
                            ?>
                        </div>
                    </div>

                    <div class="right">
                        <div class="input">
                            <h4><?= __('Price') ?></h4>
                            <input type="text" class="sortByAjax" search-type="min_price" name="" id="" placeholder="&#8362; <?= __('Min') ?>">
                        </div>
                        <div class="input">
                            <h4><?= __('to') ?></h4>
                            <input type="text" class="sortByAjax" search-type="max_price" name="" id="" placeholder="&#8362; <?= __('Max') ?>">
                        </div>
                    </div>
                <?php } ?>
            </form>
        </div>
        <div class="filter">
            <div class="box">
                <h4><?= __('Top Listings') ?></h4>
                <div class="list-grid" id="top-listings" style="max-height: 250px;overflow-y: scroll;">
                    <?php
                    foreach ($topListings as $topListing) {
                        if (!isset($topListing['images'][0]['image_url']) || empty($topListing['images'][0]['image_url'])) {
                            continue;
                        }
                    ?>
                        <?= $this->Html->link("<i style='margin-top:30px;' data-src='" . ($topListing['images'][0]['image_url'] != '' ? $topListing['images'][0]['image_url'] : $emptyImage) . "' lazy='1' parent='a'></i>", '/product/' . $topListing['id'], ['escape' => false, 'style' => 'text-align:center']) ?>
                    <?php } ?>
                </div>
            </div>
            <div class="box">
                <h4><?= __('Categories') ?></h4>
                <ul class="categories">
                    <?php foreach ($categories as $key => $category) { ?>
                        <?php
                        if ($category['category_name'] == 'See All') {
                            echo '<li>';
                            echo $this->Html->link(
                                "<img src='{$category['category_image']}' alt=''> " . (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $category['herbew_category_name'] : $category['category_name']),
                                [
                                    '?' => array_merge(['parent_category' => ''], $filter)
                                ],
                                ['escape' => false]
                            );
                            if (empty($filterdCategories)) {
                                echo '<p style="float: right;margin-bottom: 0;margin-right: 27px;color: #2a80c7;">&#10004;</p>';
                            }
                            echo '</li>';
                        } else {
                            if (!empty($category['sub_categories'])) {
                                echo "<li onclick=\"loadSubCat('#sub-{$key}')\">";
                                echo $this->Html->link(
                                    "<img src='{$category['category_image']}' alt=''> " . (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $category['herbew_category_name'] : $category['category_name']),
                                    'javascript:void(0)',
                                    ['escape' => false]
                                );
                                echo $this->Html->image('chevron-down-solid.svg', ['style' => 'height:15px;float:right;cursor: pointer;']);
                                foreach ($category['sub_categories'] as $sub_categories) {
                                    if (in_array($sub_categories['name'], $filterdCategories)) {
                                        echo '<p style="float: right;margin-bottom: 0;height: 20px; margin-top: -5px; margin-right: 10px;color: #2a80c7;"> &#10004; </p> ';
                                        break;
                                    }
                                }
                                echo "<ul class='sub-categories' id='sub-{$key}'>";
                                $first = true;
                                foreach ($category['sub_categories'] as $sub_categories) {
                                    if ($first) {
                                        $first = false;
                                        echo "<li style='margin-top:10px'>";
                                    } else {
                                        echo "<li>";
                                    }
                                    if (in_array($sub_categories['name'], $filterdCategories)) {
                                        $sub_cat_url =  [
                                            '?' => array_merge(['parent_category' => implode(',', array_diff($filterdCategories, (array) $sub_categories['name']))], $filter)
                                        ];
                                    } else {
                                        $sub_cat_url = [
                                            '?' => array_merge(['parent_category' => implode(',', array_unique(array_merge((array) trim($sub_categories['name']), $filterdCategories)))], $filter)
                                        ];
                                    }
                                    echo $this->Html->link(
                                        (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $sub_categories['herbew_name'] : $sub_categories['name']),
                                        $sub_cat_url,
                                        ['escape' => false]
                                    );
                                    if (in_array($sub_categories['name'], $filterdCategories)) {
                                        echo '<p style="float: right;margin-bottom: 0;margin-right: 27px;color: #2a80c7;">&#10004;</p>';
                                    }
                                    echo "</li>";
                                }
                                echo "</ul>";
                                echo '</li>';
                            }
                        }
                        ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="box">
                <h4><?= __('Top Cities') ?></h4>
                <ul class="cities">
                    <?php foreach ($top_cities as $cities) { ?>
                        <li><?= $this->Html->link((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $cities->herbew_name : $cities->name), ['?' => ['address' => (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $cities->herbew_name : $cities->name), 'product_search_lat' => $cities->lat, 'product_search_lng' => $cities->lng]]) ?></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="box carousel bottom-banner" style="padding:0">
                <?php if ($bottomBanner->count()) { ?>
                    <?php foreach ($bottomBanner as $banner) { ?>
                        <div class="slide"><a href="<?= $banner->link ?>" target="_BLANK" title="<?= $banner->title ?>"><?= $this->Html->image($banner->image) ?></a></div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="slide"><?= $this->Html->image('bottom-banner-1.png') ?></div>
                    <div class="slide"><?= $this->Html->image('bottom-banner-2.png') ?></div>
                    <div class="slide"><?= $this->Html->image('bottom-banner-3.png') ?></div>
                    <div class="slide"><?= $this->Html->image('bottom-banner-4.png') ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="listings">
            <div class="search-product">
                <div class="name">
                    <h4>
                        <?php if (isset($filter['name'])) { ?>
                            <?= $filter['name'] . $this->Html->link($this->Html->image('cross.png', ['style' => 'height:15px;margin-left: 10px;margin-top: -5px;']), ['?' => ['parent_category' => implode(',', $filterdCategories)]], ['escape' => false]) ?>
                        <?php } ?>
                    </h4>
                    <?php
                    if (!empty($filterdCategories)) {
                        foreach ($filterdCategories as $filterdCategorie) {
                            echo "<h4 style='padding-left: 10px;'>" . $filterdCategorie . "</h4>" . $this->Html->link($this->Html->image('cross.png', ['style' => 'height:15px;margin-left: 10px;margin-top: -5px;']), ['?' => array_merge(['parent_category' => implode(',', array_diff($filterdCategories, (array) $filterdCategorie))], $filter)], ['escape' => false]);
                        }
                    }
                    ?>
                    <?php
                    if ((!isset($filter['name']) || empty($filter['name'])) && empty($filterdCategories)) {
                        echo '<h4><a href="#">' . __('See All') . '</a></h4>';
                    }
                    ?>
                    <p><?= isset($filter['address']) ? __('near') . ' ' . $filter['address'] : '' ?></p>
                </div>
            </div>
            <div class="grid" id="loadMoreItem">
                <?= $this->element('filtered_product') ?>
            </div>
            <?php if (count($products) > 19) { ?>
                <div class="see-more">
                    <a href="javascript:void(0)" onclick="loadMoreItem(this)"><?= __('See More') ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    $('.carousel').flickity({
        autoPlay: true,
        accessibility: true,
        adaptiveHeight: true,
        draggable: '>1',
        pageDots: false,
        prevNextButtons: false,

    });
</script>
<script>
    var previewDialog;
    $(document).ready(function() {
        $('#top-listings a').each(function() {
            $(this).css('height', $(this).width());
        });
    });
    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop();
        if (scroll < 350) {
            previewDialog = false;
        }
        $('.item').each(function() {
            var scrollItem = $(this).position().top;
            if (scrollItem < scroll) {
                previewDialog = getDistanceFromLatLonInKm($(this).attr('data-lat'), $(this).attr('data-lng'), LOCAL_INFORMATION['latitude'], LOCAL_INFORMATION['longitude']).toFixed(0);
            }
        })
        if (previewDialog) {
            $('#kmDialog').show();
            if (lang == 'en') {
                $('#kmDialogContent').html(previewDialog + ' KM From You.');
            } else {
                $('#kmDialogContent').html(previewDialog + ' KM ק״מ ממך.');
            }
        } else {
            $('#kmDialog').hide();
        }
    });

    function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1); // deg2rad below
        var dLon = deg2rad(lon2 - lon1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        if (d < 10) {
            d = 10;
        }
        return d;
    }

    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }
</script>
<?php
$cakeDescription = 'Bingalo';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css') ?>
    <?= $this->Html->css('https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css') ?>
    <?= $this->Html->css('https://seiyria.com/bootstrap-slider/css/bootstrap-slider.css') ?>
    <?= $this->Html->script('jquery.min') ?>
    <?= $this->Html->script('https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js') ?>
    <?= $this->Html->script('https://seiyria.com/bootstrap-slider/js/bootstrap-slider.js') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css') ?>
    <?= $this->Html->script('https://code.jquery.com/ui/1.12.1/jquery-ui.js') ?>
    <?= $this->Html->script('https://www.google.com/recaptcha/api.js') ?>
    <?= $this->Html->script('https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=' . $googleApiKey) ?>
    <script>
        var BASE_URL = '<?= $this->Url->build('/', true) ?>';
        var LOCAL_INFORMATION = <?= json_encode($localInformation) ?>;
        var RECENT_ITEMS = <?= json_encode(!empty($searchRecentWise) ? $searchRecentWise : []) ?>;
        var TRENDING_ITEMS = <?= json_encode(!empty($searchRankWise) ? $searchRankWise : []) ?>;
        var IS_LOGIN = <?= isset($authUser) ? 1 : 0 ?>;
        var FIREBASE_SETUP = <?= json_encode($firebaseSetup) ?>
    </script>
    <style>
        @font-face {
            font-family: avenir-regular;
            src: url(../fonts/AvenirNextLTPro-Regular.otf);
        }

        @font-face {
            font-family: avenir-bold;
            src: url(../fonts/AvenirNextLTPro-Bold.otf);
        }

        body,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p,
        a {
            font-family: avenir-regular !important;
        }

        h6 {
            font-size: 0.7rem;
        }

        .login-button {
            font-size: 3vw;
        }

        .pages-link {
            font-size: .6rem;
            padding: 4px;
            text-decoration: none;
            color: #000;
        }

        .language-list {
            display: none;
        }

        .active-lang {
            display: block !important;
        }

        .search-bar-container {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            display: none;
            z-index: 9999999;
            background: #fff;
        }

        .pac-container.pac-logo.hdpi {
            z-index: 999999999999;
        }

        .category-container {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            display: none;
            z-index: 9999999;
            background: #fff;
            overflow: auto;
        }

        .ui-autocomplete {
            z-index: 9999999999;
            border: 0 !important;
            left: 20px !important;
        }

        .g-recaptcha>div {
            margin: auto;
        }

        #loginForm div.form-group.checkbox {
            display: inline-block;
        }

        img {
            max-width: unset;
        }

        .social-login a {
            background: #2196F3;
            display: block;
            text-align: center;
            border-radius: 6px;
            padding: 8px;
            margin-bottom: 10px;
        }

        .social-login a h4 {
            font-size: 15px !important;
            padding: unset;
            position: relative;
        }

        .social-login a.fb-login {
            background: #3b5998;
        }

        .social-login a.apple-login {
            background: #000000;
        }

        .social-login a.apple-login:hover {
            background: #000000;
        }

        .social-login a {
            background: #2196F3;
            display: block;
            text-align: center;
            border-radius: 30px;
            margin-bottom: 10px;
        }

        .social-login a h4 {
            color: #fff;
            font-size: 16px;
            margin-bottom: 0;
        }

        .social-login a.fb-login {
            background: #3b5998;
        }


        a.google-login {
            background: #fff;
            border: 2px solid black;
        }

        a.email-login {
            background: rgba(0, 0, 0, .2);
        }

        a.twitter-login {
            background: #319BEB;
        }

        a.google-login:hover {
            background: #fff;
        }

        #loginForm .form-control,
        #forgot-form .form-control,
        #signup-form .form-control {
            height: 40px;
            margin: 10px 0;
        }

        a {
            text-decoration: none;
        }

        .g-recaptcha>div {
            margin: auto;
        }

        ul.sub-categories {
            display: none;
        }

        ul.sub-categories li {
            margin-left: 35px;
            border-bottom: 1px solid #ddd;
            padding-bottom: 5px;
            color: #1470d6;
        }

        ul li {
            list-style: none;
        }

        .category-container ul li {
            margin-bottom: 10px;
        }

        .category-container ul li a {
            font-size: 15px;
            opacity: 0.6;
            font-weight: bold;
            display: inline-block;
            color: #000;
        }

        .category-container ul li a img {
            width: 25px;
            height: 20px;
            object-fit: contain;
            margin-right: 5px;
        }

        .modal {
            z-index: 9999999999999999999;
        }

        .slider-handle {
            background: #fff;
        }


        .slider-tick {
            height: 10px;
            width: 10px;
        }

        .slider-handle {
            height: 15px;
            width: 15px;
            margin-top: 2px;
        }

        .slider.slider-horizontal .slider-track {
            height: 4px;
            margin-top: -2px;
            margin-left: -5px;
        }

        .slider.slider-horizontal .slider-tick-label-container {
            font-size: 10px;
            margin-top: -17px;
            margin-left: -20px !important;
        }

        .slider.slider-horizontal .slider-tick-container {
            top: 5px;
        }

        .slider.slider-horizontal {
            margin-top: 22px;
        }

        .slider-tick.in-selection {
            background: #0073bc;
        }

        .slider-selection.tick-slider-selection {
            background: #0073bc;
        }

        #filter-short h4.active-sort {
            color: #0073bc;
        }

        #filter-short h4.active-sort p {
            display: block !important;
        }
    </style>
    <script>
        $(document).ready(function() {
            setTimeout(() => {
                $('[lazy]').each(function() {
                    var tagName = $(this).prop("tagName").toLowerCase();
                    if (tagName === 'img') {
                        $(this).attr('src', $(this).attr('data-src'));
                    } else {
                        $(this).css('background-image', 'url(' + $(this).attr('data-src') + ')');
                    }
                    $(this).removeAttr('data-src');
                });
            }, 100);
            $('[login]').click(function(ev) {
                if (!IS_LOGIN) {
                    ev.preventDefault();
                    $('#login-form').modal('show');
                }
            });
            $('#searh-bar-input').on('click', function(ev) {
                ev.preventDefault();
                $('.search-bar-container').show();
                $('body').css('overflow', 'hidden');
                $('#search-autocomplete').focus();
            });

            $('#category-show-button').on('click', function(ev) {
                ev.preventDefault();
                $('.category-container').show();
                $('body').css('overflow', 'hidden');
            });
            $('#search-content-clean').on('click', function() {
                $('#search-autocomplete').val('');
            });
            $('#data-search').on('click', function() {
                window.location.href = '/?name=' + $('#search-autocomplete').val();
            });
            var $inputAutocomplete = $("#search-autocomplete").autocomplete({
                source: '/search-keyword',
                minLength: 0,
                response: function(event, ui) {
                    if (RECENT_ITEMS.length > 0) {
                        ui.content.push({
                            'label': 'RECENT SEARCHES',
                            'value': ''
                        });
                        $.each(RECENT_ITEMS, function(k, recent_item) {
                            ui.content.push({
                                'label': recent_item,
                                'value': recent_item
                            });
                        });
                    }
                    if (TRENDING_ITEMS.length > 0) {
                        ui.content.push({
                            'label': 'TRENDING SEARCHES',
                            'value': ''
                        });
                        $.each(TRENDING_ITEMS, function(k, trending_item) {
                            ui.content.push({
                                'label': trending_item,
                                'value': trending_item
                            });
                        });
                    }
                },
                select: function(event, ui) {
                    window.location.href = '/?name=' + ui.item.label;
                }
            });
            $inputAutocomplete.data("ui-autocomplete")._renderItem = function(ul, item) {
                if (item.value == '') {
                    return $('<li class="ui-state-disabled"><div>' + item.label + '</div></li>').appendTo(ul);
                } else {
                    return $("<li>")
                        .append(`<div>${item.label}</div>`)
                        .appendTo(ul);
                }
            };

            $inputAutocomplete.on('focus', function() {
                $(this).keydown();
            });
            $('#signup-model').on('click', function() {
                $('#login-form').modal('hide');
                $('#signup-form').modal('show');
            });
            $('#forgot-login').on('click', function() {
                $('#login-form').modal('hide');
                setTimeout(function() {
                    $('#forgot-form').modal('show');
                }, 30);
            });
            $('#login-model').on('click', function() {
                $('#login-form').modal('show');
                $('#signup-form').modal('hide');
            });

            $('#back-to-login').on('click', function() {
                $('#login-form').modal('show');
                $('#forgot-form').modal('hide');
            });
            $('#show-password').on('click', function() {
                var text = $(this).text();
                if (text == 'Show Password') {
                    $(this).text('Hide Password');
                    $('#show-password-image').attr('src', '/images/visible.png');
                    $('#loginForm #password').attr('type', 'text');
                } else {
                    $('#show-password-image').attr('src', '/images/invisible.png');
                    $(this).text('Show Password');
                    $('#loginForm #password').attr('type', 'password');
                }
            });

            $('#filter-short h4').on('click', function() {
                $('#filter-short h4').removeClass('active-sort');
                $(this).addClass('active-sort')
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#image-preview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }

            $("#image").change(function() {
                readURL(this);
            });
        });
    </script>
</head>

<body>
    <div class="container-fluid category-container">
        <div class="row p-2">
            <div class="col-12" style="position: relative;">
                <h4 style="font-family: 'avenir-bold' !important; margin-top: 15px;">Categories</h4>
                <?= $this->Html->image('icon_coss.png', ['style' => 'width: 15px; position: absolute; right: 12px; top: 20px;', 'onclick' => "$('.category-container').hide();$('body').css('overflow', 'unset');"]) ?>
                <ul class="categories" style="padding-left: 0;">
                    <?php foreach ($categories as $key => $category) { ?>
                        <?php
                        if ($category['category_name'] == 'See All') {
                            echo '<li>';
                            echo $this->Html->link(
                                "<img src='{$category['category_image']}' alt=''> {$category['category_name']}",
                                [
                                    '?' => array_merge(['parent_category' => ''], $filter)
                                ],
                                ['escape' => false]
                            );
                            if (empty($filterdCategories)) {
                                echo '<p style="float: right;margin-bottom: 0;margin-right: 27px;color: #000;">&#10004;</p>';
                            }
                            echo '</li>';
                        } else {
                            if (!empty($category['sub_categories'])) {
                                echo "<li onclick=\"loadSubCat('#sub-{$key}')\">";
                                echo $this->Html->link(
                                    "<img src='{$category['category_image']}' alt=''> {$category['category_name']}",
                                    'javascript:void(0)',
                                    ['escape' => false]
                                );
                                echo $this->Html->image('chevron-down-solid.svg', ['style' => 'height:15px;float:right;cursor: pointer;']);
                                foreach ($category['sub_categories'] as $sub_categories) {
                                    if (in_array($sub_categories['name'], $filterdCategories)) {
                                        echo '<p style="float: right;margin-bottom: 0;height: 20px; margin-top: -5px; margin-right: 10px;color: #000;"> &#10004; </p> ';
                                        break;
                                    }
                                }
                                echo "<ul class='sub-categories' id='sub-{$key}'>";
                                $first = true;
                                foreach ($category['sub_categories'] as $sub_categories) {
                                    if ($first) {
                                        $first = false;
                                        echo "<li style='margin-top:10px'>";
                                    } else {
                                        echo "<li>";
                                    }
                                    if (in_array($sub_categories['name'], $filterdCategories)) {
                                        $sub_cat_url =  [
                                            '?' => array_merge(['parent_category' => implode(',', array_diff($filterdCategories, (array) $sub_categories['name']))], $filter)
                                        ];
                                    } else {
                                        $sub_cat_url = [
                                            '?' => array_merge(['parent_category' => implode(',', array_unique(array_merge((array) trim($sub_categories['name']), $filterdCategories)))], $filter)
                                        ];
                                    }
                                    echo $this->Html->link(
                                        " {$sub_categories['name']}",
                                        $sub_cat_url,
                                        ['escape' => false]
                                    );
                                    if (in_array($sub_categories['name'], $filterdCategories)) {
                                        echo '<p style="float: right;margin-bottom: 0;margin-right: 27px;color: #000;">&#10004;</p>';
                                    }
                                    echo "</li>";
                                }
                                echo "</ul>";
                                echo '</li>';
                            }
                        }
                        ?>
                    <?php } ?>
                </ul>
                <?= $this->Html->link('Sell Your Stuff', '/sell-your-stuff', ['login' => true, 'escape' => false, 'class' => 'btn btn-primary w-100 p-1 rounded-pill', 'style' => 'background:#0073bc;border-color:#0073bc']) ?>
                <ul class="categories" style="padding-left: 0;margin-top: 10px;">
                    <li>
                        <a href="javascript:void(0)">
                            <?= $this->Html->image('icon_info.png') ?>
                            About Bingalo
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <?= $this->Html->image('icon_help.png') ?>
                            Help center
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <?= $this->Html->image('icon_voice.png') ?>
                            Advertise on Bingalo
                        </a>
                    </li>
                </ul>
                <hr>
                <ul class="categories" style="padding-left: 0;">
                    <li>
                        <a href="javascript:$('#signup-form').modal('show');">
                            <?= $this->Html->image('icon_user.png') ?>
                            Sign up
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" login="1">
                            <?= $this->Html->image('icon_logout.png') ?>
                            Log in
                        </a>
                    </li>
                </ul>
                <hr>
                <h5 style="font-family: 'avenir-bold' !important; margin-top: 15px;">Download the Bingalo app.</h5>
                <?= $this->Html->image((stripos($_SERVER['HTTP_USER_AGENT'], "Android") ? 'btn_google_play.png' : 'btn_appstore.png'), ['style' => 'height:50px;']) ?>
            </div>
        </div>
    </div>
    <div class="container-fluid search-bar-container">
        <div class="row">
            <div class="col-12 text-end">
                <?= $this->Html->image('btn_icon_cross.png', ['style' => 'width:30px;', 'onclick' => "$('.search-bar-container').hide();$('body').css('overflow', 'unset');"]) ?>
            </div>
            <div class="col-12" style="position: relative;">
                <input type="text" value="<?= isset($filter['name']) ? $filter['name'] : '' ?>" id="search-autocomplete" style="width: 100%;margin-top: 20px;border-radius: 20px;border-color: rgba(0,0,0,.4);padding: 5px 60px 5px 20px;">
                <?= $this->Html->image('btn_icon_cross.png', ['style' => 'width:30px;position:absolute;top:24px;right:43px;', 'id' => 'search-content-clean']) ?>
                <?= $this->Html->image('btn_search.png', ['style' => 'width: 15px;position:absolute;top: 32px;right: 24px;', 'id' => 'data-search']) ?>
            </div>
        </div>
    </div>
    <?= $this->Html->link('<img src="/images/btn_sell_stuff.png" style="height: 10vw; margin-left: 3px ; margin-top: -3px;">', '/sell-your-stuff', ['login' => true, 'style' => 'position: fixed; bottom: 20px ; left: 50%;transform: translateX(-50%); z-index: 999;', 'escape' => false]) ?>

    <?php if (isset($authUser)) { ?>

    <?php } else { ?>
        <div class="modal fade" id="login-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="popup modal-dialog modal-dialog-centered p-0" role="document">
                <div class="modal-content">
                    <div class="modal-header d-block ads4-container">
                        <div class="row">
                            <div class="col-2 m-auto text-start" onclick="$('.ads4-container').remove();"><?= $this->Html->image('btn_icon_cross.png', ['style' => 'width:35px']) ?></div>
                            <div class="col-6 ms-0 ps-0">
                                <?= $this->Html->image('install_applogo.png', ['style' => 'height:38px']) ?>
                            </div>
                            <div class="col-4 text-end"><?= $this->Html->image('btn_install.png', ['style' => 'height:30px;', 'class' => 'mt-2']) ?></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="close text-end" style="margin-top: -10px;"><?= $this->Html->image('cross.png', ['style' => 'height:10px', 'onclick' => "$('#login-form').modal('hide');"]) ?></div>
                        <p class="text-center" style="font-size: 17px;">Don't have an account? <?= $this->Html->link('Sign up', '#', ['id' => 'signup-model', 'class' => 'font-weight-bold', 'style' => 'color: #000; text-decoration: none; font-family: \'avenir-bold\' !important;']) ?></p>
                        <hr>
                        <h5 class="text-center font-weight-bold pb-3 m-auto" style="font-family: avenir-bold !important;">Login</h5>
                        <div class="social-login">
                            <?php
                            echo $this->Form->postLink(
                                '<h4 style="color: #000;"><img src="/images/icon_google.png" style="position: absolute; left: 0px; height: 20px;">Continue with Google</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Google'], '_ssl' => true],
                                ['escape' => false, 'class' => 'google-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/icon_fb.png" style="position: absolute; left: 0px; height: 20px;">Continue with Facebook</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Facebook'], '_ssl' => true],
                                ['escape' => false, 'class' => 'fb-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/iconn_twitter.png" style="position: absolute; left: 0px; height: 20px;">Continue with Twitter</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Twitter'], '_ssl' => true],
                                ['escape' => false, 'class' => 'twitter-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/icon_apple.png" style="position: absolute; left: 0px; height: 20px;">Continue with Apple</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Apple'], '_ssl' => true],
                                ['escape' => false, 'class' => 'apple-login']
                            );
                            ?>
                        </div>
                        <div class="signup-form show">
                            <?= $this->Form->create(null, ['url' => '/login', 'id' => 'loginForm']) ?>
                            <?= $this->Form->control('email', ['placeholder' => 'Your Email', 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->control('password', ['placeholder' => 'Password', 'label' => false, 'required' => true]) ?>
                            <label for="remember-me" style="cursor: pointer;font-size: 12px;"><?= $this->Form->control('remember_me', ['type' => 'checkbox', 'label' => false, 'style' => 'width:15px;height:15px;']) ?> Remember me</label>
                            <label class="float-end" style="cursor: pointer;font-size: 12px;"><?= $this->Html->image('invisible.png', ['style' => 'height: 15px;', 'id' => 'show-password-image']) ?> <div id="show-password" class="d-inline-block">Show Password</div></label>
                            <div class="text-center my-2">
                                <div class="g-recaptcha" data-sitekey="6LcJ-jccAAAAAKLmfa65fV74PDwTBP6lTGZsfGsO"></div>
                            </div>
                            <div class="input">
                                <?= $this->Form->submit('Login', ['class' => 'btn btn-primary w-100 p-1 rounded-pill', 'style' => 'background: #0073bc;']) ?>
                            </div>
                            <?= $this->Form->end() ?>
                            <div class="mt-1 text-center mb-5">
                                <?= $this->Html->link('Forgot Password?', '#', ['class' => 'text-dark font-weight-bold', 'style' => "font-family: 'avenir-bold' !important; font-size: 12px;", 'id' => 'forgot-login']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="signup-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="popup modal-dialog modal-dialog-centered p-0" role="document">
                <div class="modal-content">
                    <div class="modal-header d-block ads3-container">
                        <div class="row">
                            <div class="col-2 m-auto text-start" onclick="$('.ads3-container').remove();"><?= $this->Html->image('btn_icon_cross.png', ['style' => 'width:35px']) ?></div>
                            <div class="col-6 ms-0 ps-0">
                                <?= $this->Html->image('install_applogo.png', ['style' => 'height:38px']) ?>
                            </div>
                            <div class="col-4 text-end"><?= $this->Html->image('btn_install.png', ['style' => 'height:30px;', 'class' => 'mt-2']) ?></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="close text-end" style="margin-top: -10px;"><?= $this->Html->image('cross.png', ['style' => 'height:10px', 'onclick' => "$('#signup-form').modal('hide');"]) ?></div>
                        <p class="text-center" style="font-size: 17px;">Already a member? <?= $this->Html->link('Log in', '#', ['id' => 'login-model', 'class' => 'font-weight-bold', 'style' => 'color: #000; text-decoration: none; font-family: \'avenir-bold\' !important;']) ?></p>
                        <hr>
                        <h5 class="text-center font-weight-bold pb-3 m-auto" style="font-family: avenir-bold !important;">Sign up</h5>
                        <div class="social-login">
                            <?php
                            echo $this->Form->postLink(
                                '<h4 style="color: #000;"><img src="/images/icon_google.png" style="position: absolute; left: 0px; height: 20px;">Continue with Google</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Google'], '_ssl' => true],
                                ['escape' => false, 'class' => 'google-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/icon_fb.png" style="position: absolute; left: 0px; height: 20px;">Continue with Facebook</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Facebook'], '_ssl' => true],
                                ['escape' => false, 'class' => 'fb-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/iconn_twitter.png" style="position: absolute; left: 0px; height: 20px;">Continue with Twitter</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Twitter'], '_ssl' => true],
                                ['escape' => false, 'class' => 'twitter-login']
                            );
                            echo $this->Form->postLink(
                                '<h4><img src="/images/icon_apple.png" style="position: absolute; left: 0px; height: 20px;">Continue with Apple</h4>',
                                ['controller' => 'authenticated', '?' => ['provider' => 'Apple'], '_ssl' => true],
                                ['escape' => false, 'class' => 'apple-login']
                            );
                            ?>
                            <hr>
                            <?php
                            echo $this->Html->link(
                                '<h4 style="color: #000;">Continue with Email</h4>',
                                '#',
                                ['escape' => false, 'class' => 'email-login', 'onclick' => "$('#signup-by-email').show();"]
                            ); ?>
                        </div>
                        <div class="signup-form show" style="display: none;" id="signup-by-email">
                            <?= $this->Form->create(null, ['url' => '/signup', 'type' => 'file']) ?>
                            <div class="d-none">
                                <?= $this->Form->control('image', ['type' => 'file']) ?>
                            </div>
                            <div style="text-align: center;width: 100%;">
                                <label for="image" style="border-radius: 100%;cursor: pointer;">
                                    <?= $this->Html->image('profile-upload.png', ['style' => 'height: 100px;width: 100px;border-radius: 100%;', 'id' => 'image-preview']) ?>
                                </label>
                            </div>
                            <?= $this->Form->control('email', ['placeholder' => 'Email address', 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->control('password', ['placeholder' => 'Password', 'label' => false, 'required' => true]) ?>
                            <?= $this->Form->control('full_name', ['placeholder' => 'Full Name', 'label' => false, 'required' => true]) ?>
                            <div class="input">
                                <?= $this->Form->submit('Sign Up', ['style' => 'background: #2170b7; color: #fff; padding-top: 7px; cursor: pointer; width: 100%; padding: 5px 10px; border: 1px solid #ddd; margin-bottom: 10px; border-radius: 4px; font-size: 14px;']) ?>
                            </div>
                            <?= $this->Form->end() ?>
                            <div class="terms">
                                <p>By clicking on "Sign Up", you agree to the name <a href="<?= (isset($page_status[3]) ? '/pages/view/3' : '#') ?>">Terms & Conditions</a> and <a href="<?= (isset($page_status[2]) ? '/pages/view/2' : '#') ?>">Privacy Policy</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="forgot-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="popup modal-dialog modal-dialog-centered p-0" role="document">
                <div class="modal-content border-0" style="height: calc(100vh - 100px)">
                    <div class="modal-header d-block ads2-container">
                        <div class="row">
                            <div class="col-2 m-auto text-start" onclick="$('.ads2-container').remove();"><?= $this->Html->image('btn_icon_cross.png', ['style' => 'width:35px']) ?></div>
                            <div class="col-6 ms-0 ps-0">
                                <?= $this->Html->image('install_applogo.png', ['style' => 'height:38px']) ?>
                            </div>
                            <div class="col-4 text-end"><?= $this->Html->image('btn_install.png', ['style' => 'height:30px;', 'class' => 'mt-2']) ?></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="close text-end" style="margin-top: -10px;"><?= $this->Html->image('cross.png', ['style' => 'height:10px', 'onclick' => "$('#forgot-form').modal('hide');"]) ?></div>
                        <h3 class="text-center font-weight-bold" style="font-size: 25px;margin-top: 30px;font-family: avenir-bold !important;">
                            Forgot Password
                        </h3>
                        <p class="text-center font-weight-bold" style="font-size: 17px;">Please enter a valid e-mail address. we will send you further instruction to change your Password</p>
                        <hr>
                        <?= $this->Form->create(null, ['url' => 'forgot-password']) ?>
                        <?= $this->Form->control('email', ['placeholder' => 'Email address', 'label' => false, 'required' => true]) ?>
                        <div class="input">
                            <?= $this->Form->submit('Send reset link',  ['class' => 'btn btn-dark w-100 p-2 rounded-pill mb-3', 'style' => 'background: black;color:white']) ?>
                        </div>
                        <?= $this->Form->end() ?>
                        <p class="text-center font-weight-bold mb-5"><?= $this->Html->link('Back to login', '#', ['id' => 'back-to-login', 'class' => 'text-dark', 'style' => "font-family: avenir-bold !important;"]) ?></p>
                    </div>
                </div>
            </div>
        </div>
        <script>
            document.getElementById("loginForm").addEventListener("submit", function(evt) {

                var response = grecaptcha.getResponse();
                if (response.length == 0) {
                    //reCaptcha not verified
                    alert("Bingalo.com cannot Log you in as you have not Filled out the \"I'm not a Robot\" Field.");
                    evt.preventDefault();
                    return false;
                }
                //captcha verified
                //do the rest of your validations here

            });
        </script>
    <?php } ?>

    <div class="modal fade" id="filter-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="popup modal-dialog modal-dialog-centered p-0" role="document">
            <div class="modal-content border-0" style="height: calc(100vh - 10px);">
                <div class="modal-header d-block ads2-container pb-0">
                    <div class="row">
                        <div class="col-3 text-start" onclick="$('#filter-form').modal('hide')"><?= $this->Html->image('icon_back.png', ['style' => 'width:20px']) ?></div>
                        <div class="col-6 text-center">
                            <h5 style="font-family: 'avenir-bold' !important;">Categories</h5>
                        </div>
                        <div class="col-3 text-end">
                            <p style="font-family: 'avenir-bold' !important;color:#2196F3;font-size:15px">Reset</p>
                        </div>
                    </div>
                </div>
                <div class="modal-body" style="overflow-x:auto">
                    <div class="row">
                        <div class="col-12">
                            <lable style="color:rgba(0,0,0,.5)">
                                LOCATION
                            </lable>
                            <div style="position:relative;border: 1px solid rgba(0,0,0,.5);height: 35px;border-radius: 5px;padding: 3px;margin-top: 7px;margin-bottom:7px;">
                                <span id="current-location" style="margin-top: 2px;color:#0073bc;font-family: 'avenir-bold' !important;overflow: hidden;display: block;width: 55vw;white-space: nowrap;font-size: 15px;"><?= @$filter['address'] ?></span>
                                <span style="position:absolute;right: 5px;top: 9px;font-family: 'avenir-bold' !important;font-size: 10px;" onclick="$('#filter-form').modal('hide');setTimeout(function(){$('#filter-location-form').modal('show');},40);">Change Location</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <lable style="color:rgba(0,0,0,.5)">
                                PRICE
                            </lable>
                            <div style="position:relative;border: 1px solid rgba(0,0,0,.5);height: 40px;border-radius: 5px;padding: 6px;margin-top: 7px;margin-bottom:7px;text-align: center;">
                                <label style="font-family: 'avenir-bold';color: #2196F3;font-size: 13px;">MIN ₪ <input type="number" id="min-price" value="<?= @$filter['min_price'] ?>" style="width: 3.5rem;border-radius: 20px;color: #0073bc;text-align: center;background: rgba(0,0,0,.05);border: 1px solid rgba(0,0,0,.2);"></label>
                                <label style="font-family: 'avenir-bold';color: #2196F3;font-size: 13px;">MAX ₪ <input type="number" id="max-price" value="<?= @$filter['max_price'] ?>" style="width: 3.5rem;border-radius: 20px;color: #0073bc;text-align: center;background: rgba(0,0,0,.05);border: 1px solid rgba(0,0,0,.2);"></label>
                            </div>
                        </div>
                        <div class="col-12">
                            <lable style="color:rgba(0,0,0,.5)">
                                DISTANCE
                            </lable>
                            <div style="position:relative;border: 1px solid rgba(0,0,0,.5);height: 55px;border-radius: 5px;margin-top: 7px;margin-bottom:7px;text-align: center;">
                                <input id="ex1" data-slider-value="<?= ["5" => 1, "10" => 2, "25" => 3, "50" => 4, "100" => 5, "250" => 6, "400" => 7, "500" => 8][$filter['max_distance']] ?>" type="text" data-slider-tooltip="hide" data-slider-ticks="[1, 2, 3, 4, 5, 6, 7, 8]" data-slider-ticks-labels='["5", "10", "25", "50", "100", "250", "400", "500"]' data-slider-lock-to-ticks="true" />
                            </div>
                        </div>
                        <div class="col-12">
                            <lable style="color:rgba(0,0,0,.5)">
                                SORT BY
                            </lable>
                            <div id="filter-short" style="position:relative;border: 1px solid rgba(0,0,0,.5);border-radius: 5px;margin-top: 7px;text-align: center;">
                                <h4 data-sort="1" class="<?= (@$filter['sort_type'] == '1' || empty(@$filter['sort_type'])) ? 'active-sort' : '' ?>" style="border-bottom: 1px solid rgba(0,0,0,.4); padding: 15px; text-align: left; font-size: 16px;">
                                    Closest<p style="float: right;margin-bottom: 0;margin-right: 27px;color: #0073bc;display:none">✔</p>
                                </h4>
                                <h4 data-sort="2" class="<?= @$filter['sort_type'] == '2' ? 'active-sort' : '' ?>" style="border-bottom: 1px solid rgba(0,0,0,.4); padding: 15px; text-align: left; font-size: 16px;">
                                    Newest<p style="float: right;margin-bottom: 0;margin-right: 27px;color: #0073bc;display:none">✔</p>
                                </h4>
                                <h4 data-sort="3" class="<?= @$filter['sort_type'] == '3' ? 'active-sort' : '' ?>" style="border-bottom: 1px solid rgba(0,0,0,.4); padding: 15px; text-align: left; font-size: 16px;">
                                    Price: High to Low<p style="float: right;margin-bottom: 0;margin-right: 27px;color: #0073bc;display:none">✔</p>
                                </h4>
                                <h4 data-sort="4" class="<?= @$filter['sort_type'] == '4' ? 'active-sort' : '' ?>" style="padding: 15px; text-align: left; font-size: 16px;">
                                    Price: Low to High<p style="float: right;margin-bottom: 0;margin-right: 27px;color: #0073bc;display:none">✔</p>
                                </h4>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <?= $this->Html->link('Apply Filter', 'javascript:filtersubmit()', ['escape' => false, 'class' => 'btn btn-primary w-100 mt-4 p-1 rounded-pill', 'style' => 'background: #0073bc; border-color: #0073bc; width: 70%; font-size: 12px;']) ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="filter-location-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="popup modal-dialog modal-dialog-centered p-0" role="document">
            <div class="modal-content border-0" style="height: calc(100vh - 10px);">
                <div class="modal-header d-block ads2-container pb-0">
                    <div class="row">
                        <div class="col-3 text-start" onclick="$('#filter-location-form').modal('hide');setTimeout(function(){$('#filter-form').modal('show')},40);"><?= $this->Html->image('icon_back.png', ['style' => 'width:20px']) ?></div>
                        <div class="col-6 text-center">
                            <h5 style="font-family: 'avenir-bold' !important;">Location</h5>
                        </div>
                    </div>
                </div>
                <div class="modal-body" style="overflow-x:auto">
                    <div class="row">
                        <div class="col-12">
                            <lable style="color:rgba(0,0,0,.5)">
                                ENTER A ZIP CODE OR CITY
                            </lable>
                            <div style="position:relative;padding: 3px;margin-top: 7px;margin-bottom:7px;">
                                <img src="/images/btn_search.png" style="position: absolute;right: 15px;top: 10px;height:15px;" alt="">
                                <input type="hidden" value="<?= empty(@$filter['lat']) ? $localInformation['latitude'] : $filter['lat'] ?>" id="lat">
                                <input type="hidden" value="<?= empty(@$filter['lng']) ? $localInformation['longitude'] : $filter['lng'] ?>" id="lng">
                                <input type="text" value="" id="autocomplete-filter-address" style="font-size: 12px;background:#f6f6f6;width: 100%;padding-right: 30px;" name="search-item" class="form-control text rounded-pill" placeholder="Search for items to buy or sell...">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="kmDialog" style="line-height: 1.15;
     direction: ltr;
     text-align: left;
     text-size-adjust: 100%;
     -webkit-font-smoothing: antialiased;
     font-family: Arial,Helvetica,sans-serif;
     -webkit-tap-highlight-color: transparent;
     color: #002f34;
     position: fixed;
     top: 20px;
     left: 50%;
     transform: translateX(-50%);
     width: auto;
     animation: _1RXhN .3s ease;
     z-index: 9999;
     min-width: 115px;
     display: none;">
        <button style="direction: ltr;
            text-size-adjust: 100%;
            -webkit-font-smoothing: antialiased;
            -webkit-tap-highlight-color: transparent;
            margin: 0;
            font-family: Arial,Helvetica,sans-serif;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            box-sizing: border-box;
            background: none;
            outline: none;
            text-transform: lowercase;
            font-weight: 700;
            cursor: pointer;
            position: relative;
            overflow: hidden;
            line-height: normal;
            text-decoration: none;
            font-size: 14px;
            height: 30px;
            width: 100%;
            color: #002f34;
            padding: 0 10px;
            border-radius: 50px;
            box-shadow: 0 2px 8px 0 rgba(0,0,0,.15);
            background-color: #fff;
            border: none;">
            <span style="direction: ltr;
              text-size-adjust: 100%;
              -webkit-font-smoothing: antialiased;
              -webkit-tap-highlight-color: transparent;
              font-family: Arial,Helvetica,sans-serif;
              text-transform: lowercase;
              font-weight: 700;
              cursor: pointer;
              line-height: normal;
              font-size: 10px;
              top:0;
              color: #002f34;" id="kmDialogContent">Back to top</span>
        </button>
    </div>
    <div class="container-fluid">
        <div class="row mt-3 ads1-container">
            <div class="col-2 m-auto text-start" onclick="$('.ads1-container').remove();"><?= $this->Html->image('btn_icon_cross.png', ['style' => 'width:35px']) ?></div>
            <div class="col-6 ms-0 ps-0">
                <?= $this->Html->image('install_applogo.png', ['style' => 'height:38px']) ?>
            </div>
            <div class="col-4 text-end"><?= $this->Html->image('btn_install.png', ['style' => 'height:30px;', 'class' => 'mt-2']) ?></div>
        </div>
        <hr class="ads1-container">
        <div class="row mt-3">
            <div class="col-1 text-center">
                <?= $this->Html->image('btn_side_menu.png', ['style' => 'height: 15px; margin-top: 10px;', 'id' => 'category-show-button']) ?>
            </div>
            <div class="col-4 pe-0">
                <?= $this->Html->image('logo.png', ['style' => 'height:40px']) ?>
            </div>
            <div class="col-7 pe-2 text-end mt-1">
                <?= $this->Html->link('Login', '#', ['login' => true, 'class' => 'btn btn-secondary rounded-pill login-button', 'style' => 'background:#ebebeb;border-color:#ebebeb;color:#5a5454;font-weight:bolder;padding: 3px 10px;']) ?>
                <?= $this->Html->link('Sign up', '#', ['class' => 'btn btn-secondary rounded-pill login-button', 'onclick' => "$('#signup-form').modal('show');", 'style' => 'background:#ebebeb;border-color:#ebebeb;color:#5a5454;font-weight:bolder;padding: 3px 10px;']) ?>
                <?= $this->Html->link('...', '#', ['login' => true, 'class' => 'btn btn-secondary rounded-pill login-button', 'style' => 'background:#ebebeb;border-color:#ebebeb;color:#5a5454;font-weight:bolder;padding: 3px 8px;padding-bottom:5px;padding-top:1px;']) ?>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-9 pe-0">
                <div class="form-group text" style="position: relative;">
                    <?= $this->Html->image('btn_search.png', ['style' => 'position: absolute;left: 9px;top: 9px;height:15px']) ?>
                    <input type="text" value="<?= isset($filter['name']) ? $filter['name'] : '' ?>" style="padding-left: 30px;font-size: 12px;background:#f6f6f6;width: 105%;" id="searh-bar-input" name="search-item" class="form-control text rounded-pill" placeholder="Search for items to buy or sell..." id="search-item">
                </div>
            </div>
            <div class="col-3 text-end mt-1 ps-0" onclick="$('#filter-form').modal('show');setTimeout(function(){slider.refresh()},300);">
                <?= $this->Html->image('btn_filter.png', ['style' => 'height:10px;height: 15px;']) ?> <span style="font-size: 13px; font-weight: bold;">Filter</span>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-12">
                <div class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <?php if ($topBanner->count()) { ?>
                            <?php foreach ($topBanner as $key => $banner) { ?>
                                <div class="carousel-item <?= $key === 0 ? 'active' : '' ?>" onclick="window.open('<?= $banner->link ?>')">
                                    <?= $this->Html->image($banner->image, ['style' => 'height: 140px;object-fit: cover;', 'class' => 'd-block w-100 rounded-3']) ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="fw-bolder" style="font-family: avenir-bold !important;">Top Listings <?= $this->Html->image('btn_top.png', ['style' => 'height:17px']) ?></h4>
        <div class="row mt-3">
            <div class="col-12 pe-0">
                <div class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner row">
                        <?php
                        $count = 0;
                        $total_length = count($topListings);
                        foreach ($topListings as $key => $product) {
                            if (!isset($product['images'][0]['image_url']) || empty($product['images'][0]['image_url'])) {
                                continue;
                            }
                        ?>
                            <?php if ($count === 0) { ?>
                                <div class="carousel-item <?= ($key === 0 ? 'active' : '') ?>">
                                    <div class="row">
                                    <?php
                                }
                                $count++;
                                    ?>
                                    <div class="col-6 pe-0">
                                        <div class="card" style="border: 0;" onclick="window.location.href = '<?= '/product/' . $product['id'] ?>'">
                                            <?= $this->Html->image($product['images'][0]['image_url'], ['class' => 'card-img-top', 'style' => 'height:200px;object-fit:cover;border-radius:5px 5px 0 0;background: #F0F1F3;']) ?>
                                            <div class="card-body" style="padding: 10px 0;">
                                                <h6 style="font-weight: bold;color: #000;margin-bottom: 2px;"><?= $product['product_title'] ?></h6>
                                                <p style="font-weight: bold;color:#1470d6;font-size: .8rem;"><?= $product['product_price'] ?>₪</p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (($count === 2) || ($key === $total_length)) {
                                        $count = 0; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <h6 style="font-weight: bolder;font-size: .7rem;margin-bottom:2px;font-family: avenir-bold !important;">Make Money Selling</h6>
                                <h6 style="font-weight: bolder;font-size: .7rem;margin-bottom:2px;font-family: avenir-bold !important;">Thing You Don't Need.</h6>
                                <p style="font-size: 10px;font-weight: 600;margin-bottom: 0;">Download Our App</p>
                            </div>
                            <div class="col-6">
                                <?= $this->Html->image((stripos($_SERVER['HTTP_USER_AGENT'], "Android") ? 'btn_google_play.png' : 'btn_appstore.png'), ['style' => 'height:40px;']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="fw-bolder" style="font-family: avenir-bold !important;">New</h4>
        <div class="row mt-3">
            <?php
            foreach ($products as $key => $product) {
                if (!isset($product['images'][0]['image_url']) || empty($product['images'][0]['image_url'])) {
                    continue;
                }
            ?>
                <div class="col-6 <?= ($key % 2 !== 0 ? 'ps-0' : '') ?> mb-3 product-item" data-lat="<?= $product['product_lat'] ?>" data-lng="<?= $product['product_lng'] ?>">
                    <div class="card" style="border: 0;background: #F0F1F3;" onclick="window.location.href = '<?= '/product/' . $product['id'] ?>'">
                        <?= $this->Html->image($product['images'][0]['image_url'], ['class' => 'card-img-top', 'style' => 'height:200px;object-fit:cover;border-radius:5px 5px 0 0;']) ?>
                        <div class="card-body" style="padding: 10px;">
                            <h6 style="font-weight: bold;color: #000;margin-bottom: 2px;"><?= $product['product_title'] ?></h6>
                            <p style="font-weight: bold;color:#1470d6;margin-bottom:0;font-size: .8rem;"><?= $product['product_price'] ?>₪</p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <footer>
        <div class="container-fluid" style="background:rgba(0,0,0,.05);">
            <div class="row">
                <div class="col-12 text-center my-2">
                    <?= $this->Html->image('logo.png', ['style' => 'height:40px']) ?>
                </div>
                <div class="col-12 text-center mb-2">
                    <?= $this->Html->image('btn_fb.png', ['style' => 'height:15px;padding: 0 5px;']) ?>
                    <?= $this->Html->image('btn_twitter.png', ['style' => 'height:15px;padding: 0 5px;']) ?>
                    <?= $this->Html->image('btn_instagram.png', ['style' => 'height:15px;padding: 0 5px;']) ?>
                </div>
                <div class="col-12 text-center mb-2">
                    <a href="<?= (isset($page_status[1]) ? '/pages/view/1' : '#') ?>" class="pages-link">FAQ</a>
                    <a href="<?= (isset($page_status[4]) ? '/pages/view/4' : '#') ?>" class="pages-link">Safety Tips</a>
                    <a href="<?= (isset($page_status[5]) ? '/pages/view/5' : '#') ?>" class="pages-link">Press</a>
                    <a href="#" class="pages-link">Jobs</a>
                    <a href="<?= (isset($page_status[8]) ? '/pages/view/8' : '#') ?>" class="pages-link">Prohibited Items</a>
                    <a href="#" class="pages-link">Sitemap</a>
                </div>
                <div class="col-12 text-center mb-2">
                    <div id="language-selection" class="language-option" style="padding:5px;min-height: 35px;width: 130px;background: rgba(0,0,0,.1);border-radius: 7px;border: 1px solid rgba(0,0,0,.2);margin:auto">
                        <div id="english-lang" class="language-list active-lang">
                            <?= $this->Html->image('https://cdn-icons-png.flaticon.com/512/206/206626.png', ['style' => 'height:15px;']) ?><a href="#" style="color: rgba(0,0,0,.7); text-decoration: none; font-weight: bold; font-size: 12px;margin-left: 9px;">English</a>
                            <i class="fa fa-chevron-down language-icon-status" aria-hidden="true" style="font-size: 10px; margin-left: 10 px ; color: rgba(0,0,0,.7);"></i>
                        </div>
                        <div id="herbew-lang" class="language-list">
                            <?= $this->Html->image('https://upload.wikimedia.org/wikipedia/commons/d/d4/Flag_of_Israel.svg', ['style' => 'height: 12px; margin-left: -9px;']) ?><a href="#" style="color: rgba(0,0,0,.7); text-decoration: none; font-weight: bold; font-size: 12px;margin-left: 9px;">Hebrew</a>
                            <i class="fa fa-chevron-down language-icon-status" aria-hidden="true" style="font-size: 10px; margin-left: 10 px ; color: rgba(0,0,0,.7);"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="background:rgba(0,0,0,.1)">
            <div class="row">
                <div class="col-12 text-center my-2" style="color: rgba(0,0,0,.7);">
                    <h6 style="font-weight: bold;">Buy & Sell secondhand products in United States Quickly,
                        safely and locally on the free Bingalo app. Start selling your
                        used stuff today!
                    </h6>
                    <br>
                    <p style="font-size: 7px;">© 2020. All right reserved. Name Terms & Conditions and Privacy Policy</p>
                </div>
            </div>
        </div>
    </footer>
    <script>
        var slider;

        function loadSubCat(id) {
            if ($(id).css('display') == 'none') {
                $('.sub-categories').hide();
                $(id).css('display', 'contents');
            } else {
                $(id).css('display', 'none');
            }
        }

        $(document).ready(function() {
            slider = new Slider('#ex1', {
                formatter: function(value) {
                    return 'Current value: ' + value;
                }
            });
            var currentLangId = localStorage.getItem('currentLang') || 'english-lang';
            $('.language-list').removeClass('active-lang');
            $('#' + currentLangId).addClass('active-lang');
            $('.language-list').on('click', function(ev) {
                ev.preventDefault();
                var $this = this;
                var totalActive = $('.language-list.active-lang').length;
                if (totalActive === 1) {
                    $('.language-list').addClass('active-lang');
                    $('.language-icon-status').removeClass('fa-chevron-down');
                    $('.language-icon-status').removeClass('fa-chevron-up');
                    $('#english-lang .language-icon-status').addClass('fa-chevron-up');
                } else {
                    $('.language-icon-status').removeClass('fa-chevron-down');
                    $('.language-icon-status').removeClass('fa-chevron-up');
                    $($this).find('.language-icon-status').addClass('fa-chevron-down');
                    $('.language-list').removeClass('active-lang');
                    $($this).addClass('active-lang');
                    localStorage.setItem('currentLang', $($this).attr('id'));
                }
            })
        });
    </script>
    <script>
        var previewDialog;
        $(window).on('scroll', function() {
            var scroll = $(window).scrollTop();
            if (scroll < 350) {
                previewDialog = false;
            }
            $('.product-item').each(function() {
                var scrollItem = $(this).position().top;
                if (scrollItem < scroll) {
                    previewDialog = getDistanceFromLatLonInKm($(this).attr('data-lat'), $(this).attr('data-lng'), LOCAL_INFORMATION['latitude'], LOCAL_INFORMATION['longitude']).toFixed(0);
                }
            })
            if (previewDialog) {
                $('#kmDialog').show();
                $('#kmDialogContent').html(previewDialog + ' KM From You.');
            } else {
                $('#kmDialog').hide();
            }
        });

        function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2 - lat1); // deg2rad below
            var dLon = deg2rad(lon2 - lon1);
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c; // Distance in km
            if (d < 10) {
                d = 10;
            }
            return d;
        }

        function deg2rad(deg) {
            return deg * (Math.PI / 180)
        }

        function filtersubmit() {
            let url = new URL(window.location.href);
            let params = new URLSearchParams(url.search.slice(1));
            let distance = 10;
            switch ($('#ex1').val()) {
                case 1:
                    distance = 5;
                    break;
                case 2:
                    distance = 10;
                    break;
                case 3:
                    distance = 25;
                    break;
                case 4:
                    distance = 50;
                    break;
                case 5:
                    distance = 100;
                case 6:
                    distance = 250;
                case 7:
                    distance = 400;
                case 8:
                    distance = 500;
                    break;
                default:
            }
            params.append('max_distance', distance);
            params.append('min_price', $('#min-price').val());
            params.append('max_price', $('#max-price').val());
            params.append('lat', $('#lat').val());
            params.append('lng', $('#lng').val());
            params.append('sort_type', $('#filter-short h4.active-sort').attr('data-sort'));
            window.location.href = url.search.slice(0) + '?' + params.toString()
        }

        function initialiseAutocompleteInput() {
            var input = document.getElementById('autocomplete-filter-address');
            var places = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(places, 'place_changed', function() {
                var place = places.getPlace();
                $('#current-location').html(place.formatted_address);
                $('#lat').val(place.geometry.location.lat());
                $('#lng').val(place.geometry.location.lng());
                $('#filter-location-form').modal('hide');
                setTimeout(function() {
                    $('#filter-form').modal('show')
                }, 40);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialiseAutocompleteInput);
    </script>
</body>

</html>
<html>

<head>
    <title>Bingalo</title>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>

    <link rel="stylesheet" href="css/style.css">
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/slick.css" />
    <!-- // Add the new slick-theme.css if you want the default styling -->
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css" />
    <?= $this->Html->css('alertify.min') ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <?= $this->Html->script('alertify.min') ?>
    <?= $this->Html->script('https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=' . $googleApiKey) ?>

    <script>
        var BASE_URL = '<?= $this->Url->build('/', true) ?>';
        var LOCAL_INFORMATION = <?= json_encode($localInformation) ?>;
        var RECENT_ITEMS = <?= json_encode(!empty($searchRecentWise) ? $searchRecentWise : []) ?>;
        var TRENDING_ITEMS = <?= json_encode(!empty($searchRankWise) ? $searchRankWise : []) ?>;
        var IS_LOGIN = <?= isset($authUser) ? 1 : 0 ?>;
        var FIREBASE_SETUP = <?= json_encode($firebaseSetup) ?>
    </script>

    <style>
        .wrap {
            padding: 50px 10px;
            border: 1px solid #E1E1E1;
        }

        #nlVal {
            display: inline-block;
        }

        #slider {
            width: 100%;
            height: 10px;
            background: lightgrey;
            border-radius: 50px;
        }

        #slider label {
            position: absolute;
            width: 20px;
            margin-top: -33px;
            margin-left: -10px;
            text-align: left;
            font-size: 13px;
        }

        #slider label:last-of-type::after {
            content: "+";
        }

        .ui-slider-handle.ui-state-default.ui-corner-all {
            border-radius: 100%;
            background: #fff;
            border: none;
            width: 25px;
            height: 25px;
            top: -10px;
            cursor: grab;
            box-shadow: 2px 2px 10px 2px #b3adad;
        }

        .ui-slider-handle.ui-state-active,
        .ui-slider-handle.ui-state-hover,
        .ui-slider-handle.ui-state-focus {
            border: none;
            outline: 0;
        }

        .ui-slider-handle.ui-state-active:after {
            content: "miles";
            position: absolute;
            top: -40px;
            left: -19px;
            background: red;
            color: white;
            font-size: 0.7em;
            padding: 5px 8px;
        }

        .ui-slider-handle.ui-state-active:before {
            content: "";
            background: red;
            position: absolute;
            width: 10px;
            height: 10px;
            top: -20px;
            transform: rotate(45deg);
        }

        ul.sub-cat {
            display: none;
            margin-left: 45px;
            margin-top: 10px;
        }

        ul.sub-cat li {
            white-space: nowrap;
            border-bottom: 1px solid #ddd;
            margin-bottom: 10px;
        }

        ul.sub-cat li a img {
            justify-self: right;
        }

        ul.sub-cat li a {
            grid-template-columns: 25px 1fr;
        }

        .account-popup .form-container .single-field label.checkbox::before {
            font-size: 25px;
            line-height: 25px;
            font-weight: bold;
        }

        .account-popup .form-container .single-field label.checkbox.checked::before {
            content: "✓";
        }

        .ui-widget-content {
            border: 0 !important;
            width: 100% !important;
        }

        li.filter-sort img {
            display: none;
        }

        .grid img {
            display: block !important;
        }
    </style>
</head>

<body>
    <div class="main-container">
        <?= $this->Flash->render() ?>
        <div class="site-notification">
            <div class="app-install">
                <span class="close-btn"><img src="images/Group 88.svg" alt=""></span>
                <div class="bingalo">
                    <div class="image">
                        <?= $this->Html->image('Image 8.svg') ?>
                    </div>
                    <div class="star">
                        <span>Bingalo for iOS</span>
                        <div>
                            <ul>
                                <li><a href="#"> <?= $this->Html->image('star (2).svg') ?></a></li>
                                <li><a href="#"> <?= $this->Html->image('star (2).svg') ?></a></li>
                                <li><a href="#"> <?= $this->Html->image('star (2).svg') ?></a></li>
                                <li><a href="#"> <?= $this->Html->image('star (2).svg') ?></a></li>
                                <li><a href="#"> <?= $this->Html->image('star (2).svg') ?></a></li>
                            </ul>
                            <span>4M</span>
                        </div>
                    </div>
                </div>
                <div class="button">
                    <button>INSTALL</button>
                </div>
            </div>

        </div>
        <!-- HEADER START -->
        <header>
            <div class="menu">
                <div class="mobile-menu-button">
                    <?= $this->Html->image('bars.svg', ['alt' => 'menu button']) ?>
                </div>

                <div class="menu-content hide-menu">
                    <h2>
                        Categories
                        <a class="close-btn" href="#">
                            <?= $this->Html->image('cancel.svg') ?>
                        </a>
                    </h2>

                    <ul>
                        <?php foreach ($categories as $key => $category) { ?>
                            <?php if ($category['category_name'] == 'See All') { ?>
                                <li>
                                    <a href="<?= $this->Url->build(['?' => array_merge(['parent_category' => ''], $filter)]) ?>">
                                        <?= $this->Html->image($category['category_image'], ['style' => 'height:20px']) ?>
                                        <span><?= $category['category_name'] ?></span>
                                        <?php if (empty($filterdCategories)) { ?>
                                            <?= $this->Html->image('checked (3).svg') ?>
                                        <?php } ?>
                                    </a>
                                </li>
                            <?php } else if (!empty($category['sub_categories'])) { ?>
                                <li class="category-item">
                                    <a href="#">
                                        <?= $this->Html->image($category['category_image'], ['style' => 'height:20px']) ?>
                                        <span><?= $category['category_name'] ?></span>
                                        <?php
                                        $existsAcitveFilter = false;
                                        foreach ($category['sub_categories'] as $sub_categories) {
                                            if (in_array($sub_categories['name'], $filterdCategories)) {
                                                $existsAcitveFilter = true;
                                                break;
                                            }
                                        } ?>
                                        <?php if ($existsAcitveFilter) { ?>
                                            <?= $this->Html->image('checked (3).svg') ?>
                                        <?php } else { ?>
                                            <?= $this->Html->image('arrow-right.svg') ?>
                                        <?php } ?>
                                    </a>
                                    <ul class="sub-cat">
                                        <?php foreach ($category['sub_categories'] as $sub_categories) { ?>
                                            <?php
                                            if (in_array($sub_categories['name'], $filterdCategories)) {
                                                $sub_cat_url =  [
                                                    '?' => array_merge(['parent_category' => implode(',', array_diff($filterdCategories, (array) $sub_categories['name']))], $filter)
                                                ];
                                            } else {
                                                $sub_cat_url = [
                                                    '?' => array_merge(['parent_category' => implode(',', array_unique(array_merge((array) trim($sub_categories['name']), $filterdCategories)))], $filter)
                                                ];
                                            }
                                            ?>
                                            <li>
                                                <a href="<?= $this->Url->build($sub_cat_url) ?>">
                                                    <span><?= $sub_categories['name'] ?></span>
                                                    <?php if (in_array($sub_categories['name'], $filterdCategories)) { ?>
                                                        <?= $this->Html->image('checked (3).svg') ?>
                                                    <?php } ?>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>

                    <div class="button">
                        <button>Sell Your Stuff</button>
                    </div>

                    <ul class="have-border">
                        <li>
                            <a href="#">
                                <?= $this->Html->image('information.svg') ?>
                                <span>About Bingalo</span>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <?= $this->Html->image('question (1).svg') ?>
                                <span>Help center</span>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <?= $this->Html->image('megaphone.svg') ?>
                                <span>Advertise on Bingalo</span>
                            </a>
                        </li>
                    </ul>

                    <ul class="have-border account-form">
                        <li>
                            <a class="signup-btn" style="background: inherit;">
                                <?= $this->Html->image('add-user.svg') ?>
                                <span>Sign up</span>
                            </a>
                        </li>

                        <li>
                            <a class="login-btn">
                                <?= $this->Html->image('enter.svg') ?>
                                <span>Log in</span>
                            </a>
                        </li>
                    </ul>

                    <div class="app-link">
                        <h2>Download the Bingalo app.</h2>

                        <a href="#">
                            <?= $this->Html->image('app-link.svg') ?>
                        </a>
                    </div>
                </div>
            </div>

            <h1 class="logo">
                <a href="#">Bingola</a>
            </h1>

            <div class="right-section account-form">
                <a class="login-btn">Login</a>
                <a class="signup-btn">Signup</a>
                <span class="more-btn">...</span>
            </div>
        </header>
        <!-- HEADER END -->

        <div class="filter-section">
            <div class="search-container">
                <?= $this->Html->image('search.svg', ['src' => 'images/search.svg', 'alt' => 'search icon', 'class' => 'search-icon']) ?>
                <input id="searh-bar-input" value="<?= isset($filter['name']) ? $filter['name'] : '' ?>" type="search" placeholder="Search for items to buy or sell...">
            </div>

            <div class="filter-container">
                <span>
                    <?= $this->Html->image('filter.svg', ['alt' => 'filter icon']) ?>
                    Filter
                </span>
            </div>

            <div class="search-result">
                <p>iPhone 11</p>
                <p>iPhone 11 pro max</p>
                <p>iPhone 11 case</p>
                <p>iPhone 11 pro max case</p>
            </div>
        </div>



        <div class="home-banner">
            <div class="banner-image">
                <img src="./images/banner.svg" alt="">

                <div class="address-btn">
                    <button>
                        <img style="vertical-align: inherit;" src="./images/map-icon.png" alt="">
                        1km from you
                    </button>
                </div>
            </div>
        </div>

        <div class="slider-section">
            <div class="heading">
                <h2>
                    Top Listings
                </h2>

                <img src="images/top-games-star.svg" alt="">
            </div>

            <div class="slider customer-logos">
                <?php foreach ($topListings as $key => $product) {
                    if (!isset($product['images'][0]['image_url']) || empty($product['images'][0]['image_url'])) {
                        continue;
                    }
                ?>
                    <div class="slide single-item" onclick="window.location.href = '<?= '/product/' . $product['id'] ?>'">
                        <div class="product-image">
                            <span class="fav-btn">
                                <?= $this->Html->image('heart.svg') ?>
                            </span>
                            <?= $this->Html->image($product['images'][0]['image_url'], ['style' => 'height:160px;width:100%;object-fit:cover;padding-left: 5px;padding-right: 5px;']) ?>
                        </div>

                        <div class="single-item-content">
                            <h3><?= $product['product_title'] ?></h3>
                            <span><?= $product['product_price'] ?>₪</span>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="appstore">
            <div class="appstore-box">
                <div class="box-content">
                    <h3>
                        Make Money Selling Thing You Don't Need.
                    </h3>
                    <a href="#">
                        Download Our App
                    </a>
                </div>
                <?= $this->Html->image('download-on-app-store-png.svg') ?>
            </div>
        </div>

        <div class="row-content">

            <div class="row-section">
                <h2>
                    New
                </h2>
                <div class="new-items">
                    <?php
                    foreach ($products as $key => $product) {
                        if (!isset($product['images'][0]['image_url']) || empty($product['images'][0]['image_url'])) {
                            continue;
                        }
                    ?>
                        <div class="new-single-items">
                            <div class="new-item-image">
                                <div class="text-heart">
                                    <p>New</p>
                                    <span class="fav-btn">
                                        <?= $this->Html->image('heart.svg') ?>
                                    </span>

                                </div>
                                <div class="laptop-pic">
                                    <?= $this->Html->image($product['images'][0]['image_url'], ['style' => 'height: 86px;width:100%;object-fit:cover']) ?>
                                </div>

                            </div>
                            <div class="new-item-content">
                                <h3>
                                    <?= $product['product_title'] ?>
                                </h3>
                                <span>
                                    <?= $product['product_price'] ?>₪
                                </span>
                                <p>
                                    <?= $product['product_description'] ?>
                                </p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="new-item-btn">
                    <button>
                        Sell Your Stuff
                        <img src="./images/cam.svg" alt="">
                    </button>
                </div>
            </div>
            <div class="bottom-btn">
                <button>
                    Load more
                </button>
            </div>

        </div>
        <!-- <div class="banner">
                <img src="./images/banner.png" alt="">
                <button>
                    <i class="fa fa-map-marker"></i>
                    1km from you
                </button>
            </div> -->


        <!-- FOOTER START -->
        <footer>
            <div class="footer-upper">
                <div class="logo">
                    <img src="images/logo.svg" alt="" />
                </div>

                <ul>
                    <li><a href="#"><img src="images/f-facebook.svg" alt="" /></a></li>
                    <li><a href="#"><img src="images/f-twitter.svg" alt="" /></a></li>
                    <li><a href="#"><img src="images/f-instagram.svg" alt="" /></a></li>
                </ul>

                <ul class="footer-menu">
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Safety Tips</a></li>
                    <li><a href="#">Press</a></li>
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">Prohibited Items</a></li>
                    <li><a href="#">Sitemap</a></li>
                </ul>
                <div class="footer-btn">
                    <select>
                        <div>
                            <img src="./images/england-flag.svg" alt="">
                            <option>
                                English
                            </option>
                        </div>

                        <option>
                            <img src="./images/israel.svg" alt="">
                            Hebrew
                        </option>
                    </select>
                </div>
            </div>

            <div class="footer-bottom">
                <p>
                    Buy & Sell secondhand products in United States Quickly, safely and locally on the free Bingalo app. Start selling your used stuff today!
                </p>
                <span>
                    © 2020. All right reserved. Name <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a>
                </span>
            </div>
        </footer>
        <!-- FOOTER END -->
    </div>

    <div class="filter-content popup-box hide-popup-box">
        <div class="popup-header">
            <span class="close-filter">
                <img src="images/arrow.png" alt="">
            </span>
            <h4>Filter</h4>
            <a href="/">Reset</a>
        </div>

        <div class="filter-box">
            <div class="single-box">
                <h2>LOCATION</h2>
                <div class="box location-box">
                    <p id="current-location"><?= @$filter['address'] ?></p>
                    <input type="hidden" id="lat">
                    <input type="hidden" id="lng">
                    <siv class="arrow-icon">
                        <a href="#">Change Location</a>
                        <img src="./images/arrow-right.png" alt="">
                    </siv>

                </div>
            </div>

            <div class="single-box">
                <h2>PRICE</h2>
                <div class="box prize">
                    <div class=" input">
                        <span>MIN ₪</span>
                        <input type="text" id="min-price" placeholder="000">
                    </div>
                    <div class="input">
                        <span>MAX ₪</span>
                        <input type="text" id="max-price" placeholder="1000">
                    </div>
                </div>
            </div>

            <div class="single-box">
                <h2>DISTANCE</h2>
                <div id="form-wrapper">
                    <img src="images/ico-location.svg" class="img-location">
                    <form action="" method="GET">
                        <div id="debt-amount-slider">
                            <input type="radio" name="debt-amount" id="1" value="1" required <?= (@$filter['max_distance'] == '5' || empty(@$filter['max_distance'])) ? 'checked' : '' ?>>
                            <label for="1" data-debt-amount="5"></label>
                            <input type="radio" name="debt-amount" id="2" value="2" required <?= (@$filter['max_distance'] == '10') ? 'checked' : '' ?>>
                            <label for="2" data-debt-amount="10"></label>
                            <input type="radio" name="debt-amount" id="3" value="3" required <?= (@$filter['max_distance'] == '25') ? 'checked' : '' ?>>
                            <label for="3" data-debt-amount="25"></label>
                            <input type="radio" name="debt-amount" id="4" value="4" required <?= (@$filter['max_distance'] == '50') ? 'checked' : '' ?>>
                            <label for="4" data-debt-amount="50"></label>
                            <input type="radio" name="debt-amount" id="5" value="5" required <?= (@$filter['max_distance'] == '100') ? 'checked' : '' ?>>
                            <label for="5" data-debt-amount="100"></label>
                            <div id="debt-amount-pos"></div>
                        </div>
                    </form>
                    <img src="images/ico-car.svg" class="img-car">
                </div>
            </div>

            <div class="single-box">
                <h2>SORT BY</h2>
                <div class="box sort">
                    <div class="menu">
                        <ul>
                            <li class="filter-sort <?= (@$filter['sort_type'] == '1' || empty(@$filter['sort_type'])) ? 'grid' : '' ?>" data-sort="1">
                                <a href="javascript:applySort(1)">Closest</a>
                                <img src="./images/check (1).svg" alt="">
                            </li>
                            <li class="filter-sort <?= (@$filter['sort_type'] == '2') ? 'grid' : '' ?>" data-sort="2">
                                <a href="javascript:applySort(2)">Newest</a>
                                <img src="./images/check (1).svg" alt="">
                            </li>
                            <li class="filter-sort <?= (@$filter['sort_type'] == '3') ? 'grid' : '' ?>" data-sort="3">
                                <a href="javascript:applySort(3)">Price: High to Low</a>
                                <img src="./images/check (1).svg" alt="">
                            </li>
                            <li class="filter-sort <?= (@$filter['sort_type'] == '4') ? 'grid' : '' ?>" data-sort="4">
                                <a href="javascript:applySort(4)">Price: Low to High</a>
                                <img src="./images/check (1).svg" alt="">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="button">
            <button onclick="filtersubmit();">Apply Filter</button>
        </div>
    </div>

    <div class="location-content popup-box hide-popup-box">
        <div class="popup-header">
            <span class="close-location">
                <img src="images/arrow.png" alt="">
            </span>

            <h4>Location</h4>

            <span class="close-location">
                <img src="images/Group 116.png" alt="">
            </span>
        </div>

        <div class="zip">
            <span>ENTER A ZIP CODE OR CITY</span>
        </div>

        <div class="input filter-section">
            <div class="search-container">
                <input type="search" value="<?= @$filter['address'] ?>" id="autocomplete-filter-address" placeholder="Search for items to buy or sell...">
                <img class=" search" src="images/search.svg" alt="search icon" />
            </div>
            <div class="search-result">
                <p>SA, Itly</p>
                <p>SA lika, London</p>
            </div>

        </div>
        <div class="span">
            <span>OR</span>
        </div>
        <div class="button">
            <button onclick="getCurrentLocation()">
                <img src="images/Path 320.png" alt="">
                Get my location
            </button>
        </div>
    </div>

    <div class="account-popup login-popup popup-box hide-popup-box">
        <div class="account-header account-form">
            <h2>Don’t have an account? <a class="signup-btn" style="background: inherit;">Sign up</a></h2>
            <a class="close-btn" href="#"><img src="./images/cancel.svg" alt=""></a>
        </div>

        <h2>Login</h2>

        <div class="buttons-container have-border-bottom">
            <?= $this->Form->postLink(
                '<img src="./images/google.svg" alt="">
                Continue with Google',
                ['controller' => 'authenticated', '?' => ['provider' => 'Google'], '_ssl' => true],
                ['escape' => false, 'class' => 'have-border white-bg']
            ) ?>

            <?= $this->Form->postLink(
                '<img src="./images/facebook.svg" alt="">
                Continue with Facebook',
                ['controller' => 'authenticated', '?' => ['provider' => 'Facebook'], '_ssl' => true],
                ['escape' => false, 'class' => 'facebook-btn']
            ) ?>

            <?= $this->Form->postLink(
                '<img src="./images/twitter.svg" alt="">
                Continue with Twitter',
                ['controller' => 'authenticated', '?' => ['provider' => 'Twitter'], '_ssl' => true],
                ['escape' => false, 'class' => 'twitter-btn']
            ) ?>

            <?= $this->Form->postLink(
                '<img src="./images/apple.svg" alt="">
                Continue with Apple',
                ['controller' => 'authenticated', '?' => ['provider' => 'Apple'], '_ssl' => true],
                ['escape' => false, 'class' => 'apple-btn']
            ) ?>
        </div>

        <form action="/login" class="form-container" method="POST" id="form-login">
            <div class="single-field">
                <input type="email" name="email" placeholder="Email">
            </div>

            <div class="single-field">
                <input type="password" name="password" placeholder="Password">
            </div>

            <div class="single-field have-grid">
                <label class="checkbox">
                    <input type="checkbox" name="remeber_me"> Remember me
                </label>

                <span class="show-password">
                    <img src="images/visibility.svg" alt="" /> Show Password</span>
            </div>

            <div class="buttons-container">
                <button>
                    Login
                </button>
            </div>
        </form>

        <p><a href="./forget-password.html">Forgot Password?</a></p>
    </div>

    <div class="account-popup signup-popup popup-box hide-popup-box">
        <div class="account-header account-form">
            <h2>Already a member? <a class="login-btn">Login</a></h2>
            <a class="close-btn" href="#"><img src="./images/cancel.svg" alt=""></a>
        </div>

        <h2>Signup</h2>

        <div class="buttons-container have-border-bottom">
            <a href="#" class="have-border white-bg">
                <img src="./images/google.svg" alt="">
                Continue with Google
            </a>

            <a href="#" class="facebook-btn">
                <img src="./images/facebook.svg" alt="">
                Continue with Facebook
            </a>

            <a href="#" class="twitter-btn">
                <img src="./images/twitter.svg" alt="">
                Continue with Twitter
            </a>

            <a href="#" class="apple-btn">
                <img src="./images/apple.svg" alt="">
                Continue with Apple
            </a>
        </div>

        <form class="form-container">
            <div class="buttons-container">
                <button class="dark-color">
                    Continue with Email
                </button>
            </div>
        </form>

        <p><a href="./forget-password.html">Forgot Password?</a></p>
    </div>

    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
        function applySort(id) {
            $('.menu ul li').removeClass('grid');
            $('li[data-sort="' + id + '"]').addClass('grid');
        }

        function filtersubmit() {
            let url = new URL(window.location.href);
            let params = new URLSearchParams(url.search.slice(1));
            params.append('max_distance', $('[name="debt-amount"]:checked').val() || 5);
            params.append('min_price', $('#min-price').val());
            params.append('max_price', $('#max-price').val());
            params.append('lat', $('#lat').val());
            params.append('lng', $('#lng').val());
            params.append('sort_type', $('.menu li.grid').attr('data-sort'));
            params.append('address', $('#current-location').html());
            window.location.href = url.search.slice(0) + '?' + params.toString()
        }
        $(document).ready(function() {
            $('#form-login input').on('keyup', function() {
                var needActive = true;
                $('#form-login input').each(function() {
                    if ($(this).val() == '') {
                        needActive = false;
                    }
                });
                if (needActive) {
                    $('#form-login button').css('background-color', '#0872bb');
                    $('#form-login button').attr('type', 'submit');
                } else {
                    $('#form-login button').css('background-color', '#EBEBEB');
                    $('#form-login button').attr('type', 'button');
                }
            });
            $('.category-item').on('click', function() {
                console.log(12);
                var content = $(this).find('ul.sub-cat');
                if (content.css('display') === 'block') {
                    content.css('display', 'none');
                } else {
                    content.css('display', 'block');
                }
            });

            $('.account-popup .form-container .single-field label.checkbox input').on('click', function() {
                var d = $(this).parents('label.checkbox');
                if (d.hasClass('checked')) {
                    d.removeClass('checked');
                } else {
                    d.addClass('checked');
                }
            });
            $('.show-password').on('click', function() {
                var text = $(this).text().trim();
                if (text == 'Show Password') {
                    $(this).html('<img src="/images/invisible.png" style="height:12px"> Hide Password');
                    $('#form-login input[name="password"]').attr('type', 'text');
                } else {
                    $(this).html('<img src="/images/visible.png" style="height:12px"> Show Password');
                    $('#form-login input[name="password"]').attr('type', 'password');
                }
            });
            $(document).ready(function() {
                var $inputAutocomplete = $("#searh-bar-input").autocomplete({
                    source: '/search-keyword',
                    minLength: 0,
                    response: function(event, ui) {
                        if (RECENT_ITEMS.length > 0) {
                            ui.content.push({
                                'label': 'RECENT SEARCHES',
                                'value': ''
                            });
                            $.each(RECENT_ITEMS, function(k, recent_item) {
                                ui.content.push({
                                    'label': recent_item,
                                    'value': recent_item
                                });
                            });
                        }
                        if (TRENDING_ITEMS.length > 0) {
                            ui.content.push({
                                'label': 'TRENDING SEARCHES',
                                'value': ''
                            });
                            $.each(TRENDING_ITEMS, function(k, trending_item) {
                                ui.content.push({
                                    'label': trending_item,
                                    'value': trending_item
                                });
                            });
                        }
                    },
                    select: function(event, ui) {
                        window.location.href = '/?name=' + ui.item.label;
                    }
                });
                $inputAutocomplete.data("ui-autocomplete")._renderItem = function(ul, item) {
                    if (item.value == '') {
                        return $('<li class="ui-state-disabled"><div>' + item.label + '</div></li>').appendTo(ul);
                    } else {
                        return $("<li>")
                            .append(`<div>${item.label}</div>`)
                            .appendTo(ul);
                    }
                };

                $inputAutocomplete.on('focus', function() {
                    $(this).keydown();
                });
            });
        });
        var geocoder;

        function initialiseAutocompleteInput() {
            geocoder = new google.maps.Geocoder();
            var input = document.getElementById('autocomplete-filter-address');
            var places = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(places, 'place_changed', function() {
                var place = places.getPlace();
                $('#current-location').html(place.formatted_address);
                $('#lat').val(place.geometry.location.lat());
                $('#lng').val(place.geometry.location.lng());
                $('.location-content').addClass('hide-popup-box');
            });
        }
        google.maps.event.addDomListener(window, 'load', initialiseAutocompleteInput);


        function setCookie(cname, cvalue, exdays) {
            const d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            let expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            let name = cname + "=";
            let decodedCookie = decodeURIComponent(document.cookie);
            let ca = decodedCookie.split(';');
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


        if ("geolocation" in navigator) { //check geolocation available 
            //try to get user current location using getCurrentPosition() method
            navigator.geolocation.getCurrentPosition(function(position) {
                var latlng = position.coords.latitude + "," + position.coords.longitude;
                var cokie = getCookie('Location');
                if ((cokie == '') || (cokie != latlng)) {
                    setCookie('Location', latlng, 365);
                    if (cokie == '') {
                        window.location.reload();
                    }
                }
            }, function(err) {
                console.warn(`ERROR(${err.code}): ${err.message}`);
            }, {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            });
        } else {
            console.log("Browser doesn't support geolocation!");
        }

        function codeAddress() {
            var address = document.getElementById('address').value;
            geocoder.geocode({
                'address': address
            }, function(results, status) {
                if (status == 'OK') {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

        function getCurrentLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        let latlng = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude,
                        };
                        geocoder
                            .geocode({
                                location: latlng
                            })
                            .then((response) => {
                                if (response.results[0]) {
                                    $('#current-location').html(response.results[0].formatted_address);
                                    $('#lat').val(latlng.lat);
                                    $('#lng').val(latlng.lng);
                                    $('.location-content').addClass('hide-popup-box');
                                } else {
                                    window.alert("No results found");
                                }
                            })
                            .catch((e) => window.alert("Geocoder failed due to: " + e));
                    },
                    () => {
                        console.log('No found');
                    }
                );
            } else {
                console.log('No found');
            }
        }
    </script>
</body>

</html>
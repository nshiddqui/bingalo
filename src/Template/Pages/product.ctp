<?php
$this->assign('bodyClass', 'detail-page');
$icons = [];
if (!empty($user_verfy)) {
    foreach ($user_verfy as $user_verification) {
        $icons[] = $this->Html->image($user_verification->verified_by . '-icon.png', ['style' => 'height:20px;margin: 0 1.8px']);
    }
}
$max_height = '678px';
$emptyImage = '/images/not-found.png';
?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css') ?>
<?= $this->Html->css('https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css') ?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js') ?>
<?= $this->Html->script('https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js') ?>
<?= $this->Html->script('https://js.braintreegateway.com/web/3.85.3/js/hosted-fields.js') ?>
<?= $this->Html->script('https://js.braintreegateway.com/web/3.85.3/js/client.js') ?>
<?= $this->Html->script('https://pay.google.com/gp/p/js/pay.js') ?>
<?= $this->Html->script('https://js.braintreegateway.com/web/3.85.3/js/google-payment.min.js') ?>
<input type="hidden" id="paid-amount" value="35">
<input type="hidden" id="payment-method" value="card">
<style>
    .toast {
        position: fixed;
        top: 15px;
        right: 15px;
        z-index: 9999;
    }

    .bootstrap-basic {
        background: white;
    }

    /* Braintree Hosted Fields styling classes*/
    .braintree-hosted-fields-focused {
        color: #495057;
        background-color: #fff;
        border-color: #80bdff;
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
    }

    .braintree-hosted-fields-focused.is-invalid {
        border-color: #dc3545;
        box-shadow: 0 0 0 0.2rem rgba(220, 53, 69, 0.25);
    }

    #desc-text {
        font-size: 14px;
        font-weight: bold;
        overflow: hidden;
        max-height: 200px;
        min-height: 0;
    }

    #image-review {
        background-image: url('<?= $data['images'][0]['image_url'] ?>');
        width: 100%;
        height: 350px;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        position: relative;
        /* filter: blur(2px); */
    }

    span {
        font-size: inherit;
        position: inherit;
        top: inherit;
    }

    .product-image-preview {
        width: 100%;
        padding-bottom: 100%;
        background-size: cover;
        background-position: center;
    }

    .report-item img {
        cursor: pointer;
        height: 60px;
    }

    .cursor-pointer {
        cursor: pointer;
    }

    /* .dist-tag {
        position: absolute;
        top: 20px;
        overflow: hidden;
        left: -40px;
        background: #0087ff;
        width: 140px;
        text-align: center;
        font-size: 18px;
        color: #fff;
        transform: rotate(310deg);
        padding-top: 2px;
    } */

    #preview-image-blur {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-image: url('<?= $data['images'][0]['image_url'] ?>');
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
    }

    .product-image-preview[data-src] {
        width: 2rem;
        height: 2rem;
        margin: 50px auto;
        padding: 0;
    }

    .image img[data-src] {
        margin: 20px auto;
        width: 80px !important;
        height: 80px !important;
        padding: 0;
    }

    img[data-src] {
        width: 2rem !important;
        height: 2rem !important;
    }

    .listings img[data-src] {
        width: 10rem !important;
        height: 10rem !important;
    }

    .amount-bump.active {
        border: 3px solid #2a80c7;
    }
</style>
<div class="pagination">
    <div class="container">
        <ul>
            <li><a href="/"><?= __('Home') ?></a></li>
            <?php if (!empty($data['product_category'])) { ?>
                <li><?= $this->Html->link((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? $data['herbew_category_name'] : $data['category_name']), '/?parent_category=' . $data['product_category']) ?></li>
            <?php } ?>
            <li class="active"><a href="#"><?= $data['product_title'] ?></a></li>
        </ul>
    </div>
</div>
<div class="details">
    <div class="container-xl">
        <div class="row">
            <div class="col-md-8 p-2">
                <div class="row">
                    <div class="col-md-12 py-2 px-0">
                        <div class="card bg-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 px-0" style="position: relative;">
                                        <div class="carousel" style="width: 100%;" data-flickity='{ "wrapAround": true }'>
                                            <?php foreach ($data['images'] as $key =>  $images) { ?>
                                                <div class="carousel-cell"><a href="<?= $images['image_url'] ?>" data-fancybox="images"><?= $this->Html->image($images['image_url'], ['style' => 'width: 100%; height: 500px; object-fit: cover;border-radius: 10px;']) ?></a></div>
                                            <?php } ?>
                                        </div>
                                        <?= $this->Html->image(($is_fav ? 'fav_selected.png' : 'fav_unselected.png'), ['url' => ['controller' => 'Authorized', 'action' => 'favourite', $data['id']], 'style' => 'position: absolute; top: 20px; right: 20px; height: 25px;', 'login' => '1']) ?>
                                    </div>
                                    <?= $this->Html->css('https://npmcdn.com/flickity@2/dist/flickity.css') ?>
                                    <?= $this->Html->script('https://npmcdn.com/flickity@2/dist/flickity.pkgd.js') ?>
                                    <style>
                                        .flickity-slider {
                                            transform: unset;
                                            right: 0;
                                        }

                                        .carousel-cell {
                                            transform: unset;
                                            right: 0;
                                        }
                                    </style>
                                    <script>
                                        $().fancybox({
                                            selector: '.carousel-cell a',
                                            hash: false,
                                            thumbs: {
                                                autoStart: true
                                            },
                                            buttons: [
                                                'zoom',
                                                'download',
                                                'close'
                                            ]
                                        });
                                    </script>
                                    <div class="col-12 mt-4">
                                        <p style="color:#808080;font-size: 12px;width: 100%;text-align: right;margin-top:5px"><?= __('Click on Image to see in Full Size') ?></p>
                                        <h6 style="font-weight: bold;color:#808080;"><span style="color: #1470d6;float:left"><?= __('Posted') ?>:</span> <?= $this->Time->timeAgoInWords($data['created_at'], ['accuracy' => array('month' => 'month'), 'end' => '1 year']) ?></h6>
                                        <h6 style="font-weight: bold;color:#808080;"><span style="color: #1470d6;"><?= __('Condition') ?>:</span> <?= explode('#', $data['product_condition'])[0] ?></h6>
                                        <?php if (!empty($data['product_specifications'])) { ?>
                                            <h6 style="font-weight: bold;color:#808080;"><span style="color: #1470d6;"><?= __('Specification') ?>:</span> <?= $data['product_specifications'] ?></h6>
                                        <?php } ?>
                                        <div class="desc" style="position:relative">
                                            <h6 style="font-weight: bold;color:#808080;"><span style="color: #1470d6;"><?= __('Description') ?>:</span></h6>
                                            <div id="desc-text" style="line-height:18px;color:#a29f9f;"><?= preg_replace('/^(?:<br\s*\/?>\s*)+/', '', nl2br($data['product_description'])) ?></div>
                                            <a href="javascript:void(0)" id="show-more" style="color: #007bff;"><?= __('Read More') ?>…</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 py-2 px-0">
                        <div class="card bg-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 px-0 mt-3">
                                        <h6 style="font-weight: bold;color:#808080;"><span style="color: #1470d6;"><?= __('Location') ?></span></h6>
                                        <div class="map" id="map" style="height:300px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 py-2 px-0">
                        <div class="card bg-white border-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 px-0 mt-3">
                                        <h6 style="font-weight: bold;color:#808080;text-align:center;"><span style="color: #1470d6;"><?= __('Share this Product') ?></span></h6>
                                        <ul class="list-inline text-center">
                                            <li class="list-inline-item"><a href="#"><?= $this->Html->image('mail.png') ?></a></li>
                                            <li class="list-inline-item"><a href="#"><?= $this->Html->image('facebook.png') ?></a></li>
                                            <li class="list-inline-item"><a href="#"><?= $this->Html->image('twitter-2.png') ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 p-2">
                <div class="row">
                    <div class="col-md-12 px-0 py-2">
                        <div class="card bg-white border-0">
                            <div class="card-body text-center">
                                <h6 style="font-weight: bold;color:#808080;"><?= rtrim($data['product_title'], '.') ?></h6>
                                <h2 style="font-weight: bold;color:#1470d6;">&#8362; <?= $this->Number->format($data['product_price']) ?></h2>
                                <?php if ($data['uploaded_by_user_id']['id'] != $authUser['id']) { ?>
                                    <a href="javascript:void(0)" class="btn btn-dark btn-round rounded-pill w-50 mt-4" download-app><?= __('Chat With Seller') ?></a>
                                <?php } else { ?>
                                    <div class="mt-4">
                                        <?php if ($data['product_is_sold'] === '0') {
                                            $max_height = '585px';
                                        ?>
                                            <a href="/mark-as-sold/<?= $data['id'] ?>" class="btn btn-primary btn-round rounded-pill w-50"><?= __('Mark as sold') ?></a>
                                            <a href="#bump-item" class="btn btn-info btn-round rounded-pill w-50 my-2" data-toggle="modal"><?= __('Bump this item') ?></a>
                                        <?php } ?>
                                        <a href="/sell-your-stuff/<?= $data['id'] ?>" class="btn btn-warning btn-round rounded-pill w-50 mb-2"><?= __('Edit this Product') ?></a>
                                        <a href="/delete-product/<?= $data['id'] ?>" class="btn btn-danger btn-round rounded-pill w-50"><?= __('Delete this Product') ?></a>
                                    <?php } ?>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="text-center" style="font-weight: bold;color:#54585d;margin-top: 20px;"><?= __('ABOUT THIS SELLER') ?></h5>
                    <div class="col-md-12 px-0 py-2">
                        <div class="card bg-white border-0">
                            <div class="card-body">
                                <div class="text-center">
                                    <?= $this->Html->image($data['uploaded_by_user_id']['image'] != '' ? $data['uploaded_by_user_id']['image'] : 'profile.png', ['class' => 'rounded-circle', 'style' => 'height:100px;width: 100px;object-fit: cover;', 'url' => '/profile/' . $data['uploaded_by_user_id']['id']]) ?>
                                    <h5 style="font-weight: bold;color:#54585d;margin-top: 20px;cursor:pointer" onclick="window.location.href='/profile/<?= $data['uploaded_by_user_id']['id'] ?>'"><?= $data['uploaded_by_user_id']['full_name'] ?></h5>
                                    <p style="font-size: 15px; color: #2678bb; margin-bottom: 0; font-weight: bold;"><?= __('Verified with') ?></p>
                                    <div>
                                        <?= implode(' ', $icons) ?>
                                    </div>
                                    <?php if ($data['uploaded_by_user_id']['id'] != $authUser['id']) { ?>
                                        <div class="row p-0 mt-3">
                                            <div class="col-md-6 p-1">
                                                <?= $this->Html->link(__('Report Listing'), '#report-listing', ['class' => 'btn btn-dark btn-round rounded-pill w-100', 'data-toggle' => 'modal', 'login' => true]) ?>
                                            </div>
                                            <div class="col-md-6 p-1">
                                                <?= $this->Html->link(__('Flag Seller'), '#flag-seller', ['class' => 'btn btn-primary btn-round rounded-pill w-100', 'data-toggle' => 'modal', 'login' => true]) ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="container-fluid mt-5 px-0">
                                    <div class="row">
                                        <div class="listings" style="max-height: <?= $max_height ?>;overflow:auto;width: 100%;">
                                            <div class="grid" style="grid-template-columns: 1fr 1fr;">
                                                <?= $this->element('filtered_product', ['bump' => false]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="report-listing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Report Listing') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-5">
                    <div class="row">
                        <div class="col-12">
                            <div class="card" style="border-color: #2a80c7;background: #F0F1F3;border-radius: 15px;">
                                <div class="card-body p-0">
                                    <div class="row">
                                        <div class="col-md-3 p-0" style="background: #E5E7EB;border-radius:15px 0px 0px 15px;height: 126px;overflow: hidden;">
                                            <div class="product-image-preview" style="background-image: url('<?= ($data['images'][0]['image_url'] != '' ? $data['images'][0]['image_url'] : $emptyImage) ?>');border-radius: 15px 0px 0px 15px;"></div>
                                        </div>
                                        <div class="col-md-9 py-4">
                                            <p class="font-weight-bold mb-0 text-dark" style="font-size: 15px;"><?= substr($data['product_title'], 0, 18) ?></p>
                                            <h6 style="font-weight: bold;color:#1470d6;"><?= $data['product_price'] ?>&#8362; </h6>
                                            <p class="font-weight-bold mb-0 text-secondary" style="font-size: 10px;height: 30px; overflow:hidden;"><?= $data['product_description']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 px-4 mt-3">
                            <h5 class="font-weight-bold">Reason</h5>
                            <div class="row text-center report-item">
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select1']) ?>
                                        <?= $this->Html->image('report_item1.png', ['select' => 'report-select1']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Prohibited item') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select2']) ?>
                                        <?= $this->Html->image('report_item2.png', ['select' => 'report-select2']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Counterfeit item') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select3']) ?>
                                        <?= $this->Html->image('report_item3.png', ['select' => 'report-select3']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Advertising/ soliciting') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select4']) ?>
                                        <?= $this->Html->image('report_item4.png', ['select' => 'report-select4']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Trade/offline transaction') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select5']) ?>
                                        <?= $this->Html->image('report_item5.png', ['select' => 'report-select5']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Inappropriate content') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select6']) ?>
                                        <?= $this->Html->image('report_item6.png', ['select' => 'report-select6']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Hate/offensive') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select7']) ?>
                                        <?= $this->Html->image('report_item7.png', ['select' => 'report-select7']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Digital item') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select8']) ?>
                                        <?= $this->Html->image('report_item8.png', ['select' => 'report-select8']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Stolen good') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select9']) ?>
                                        <?= $this->Html->image('report_item9.png', ['select' => 'report-select9']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Keyword stuffing') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select10']) ?>
                                        <?= $this->Html->image('report_item10.png', ['select' => 'report-select10']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Wrong brand/ category') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select11']) ?>
                                        <?= $this->Html->image('report_item11.png', ['select' => 'report-select11']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('No intent to sell') ?></p>
                                </div>
                                <div class="col-md-2 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select12']) ?>
                                        <?= $this->Html->image('report_item12.png', ['select' => 'report-select12']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Other') ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 px-4 mt-3">
                            <?= $this->Form->control('note', ['type' => 'textarea', 'rows' => '5', 'label' => false, 'placeholder' => __('Note'), 'style' => 'background: #E5E7EB; border-radius: 20px;']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center border-0 mb-4">
                <button type="button" class="btn btn-primary btn-round rounded-pill w-25 m-auto" id="submit-report-form"><?= __('Report Item') ?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="thank-you-report-listing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Report Listing') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-5">
                    <div class="row text-center">
                        <div class="col-12">
                            <?= $this->Html->image('report_selected.png') ?>
                            <p class="mt-3" style="font-size: 15px;"><?= __('Thank you for reporting.') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="bump-item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Bump an Item, Sell it faster') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-5">
                    <div class="row">
                        <div class="col-md-3 p-1">
                            <div class="card p-0" style="border-color: #2a80c7;background: #F0F1F3;border-radius: 15px; overflow: hidden;">
                                <div class="card-body p-0">
                                    <div class="row p-0">
                                        <div class="col-md-12 p-0" style="background: #E5E7EB">
                                            <?= $this->Html->image((isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'en_HB' ? 'he_img_featured.png' : 'img_featured.png'), ['class' => 'position-absolute']) ?>
                                            <div class="product-image-preview" style="background-image: url('<?= ($data['images'][0]['image_url'] != '' ? $data['images'][0]['image_url'] : $emptyImage) ?>');"></div>
                                        </div>
                                        <div class="col-md-12 p-2" style="height: 95px;">
                                            <p class="font-weight-bold mb-0 text-dark" style="font-size: 15px;"><?= substr($data['product_title'], 0, 18) ?></p>
                                            <h6 class="mb-0" style="font-weight: bold;color:#1470d6;"><?= $data['product_price'] ?>&#8362; </h6>
                                            <p class="font-weight-bold mb-0 text-secondary" style="font-size: 10px;"><?= substr($data['product_description'], 0, 60); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-12 pl-0">
                                    <?= $this->Html->image('img_bump.png', ['style' => 'height: 128px; padding-top: 14px;']) ?>
                                </div>
                                <div class="col-12 pl-0 mt-5">
                                    <h3 class="font-weight-bold" style="color:#2a80c7;font-size: 15px;"><?= __('Want to sell it quickly?') ?></h3>
                                    <p style="font-size: 15px;"><?= __('Highlight your item so that more people see it. Many More!') ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 p-1 mt-4">
                            <h4 class="font-weight-bold" style="color:#2a80c7;"><?= __('Select Package') ?></h4>
                        </div>
                        <div class="col-12 p-1 mt-2">
                            <div class="row text-center">
                                <div class="col-md-4 pl-0">
                                    <div class="amount-bump shadow p-5 mb-5 bg-white rounded w-100 align-middle cursor-pointer" onclick="loadBumpSeller(this,'25&#8362; ','3 days','','35&#8362; ');$('#paid-amount').val(25)">
                                        <h6 style="color:#2a80c7;">3 <?= __('days') ?></h6>
                                        <h2 class="font-weight-bold" style="color:#2a80c7;">25&#8362; </h2>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-0">
                                    <div class="amount-bump active shadow p-4 mb-5 bg-white rounded w-100 align-middle cursor-pointer" onclick="loadBumpSeller(this,'35&#8362; ','7 days','Save 46%','45&#8362; ');$('#paid-amount').val(35)">
                                        <h6 style="color:#2a80c7;">7 <?= __('days') ?></h6>
                                        <h2 class="font-weight-bold mt-3" style="color:#2a80c7;">35&#8362; </h2>
                                        <h6 style="color:#2a80c7;" class="mt-4"><?= __('Save') ?> 46%</h6>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-0">
                                    <div class="amount-bump shadow p-4 mb-5 bg-white rounded w-100 align-middle cursor-pointer" onclick="loadBumpSeller(this,'50&#8362; ','15 days','Save 62%','60&#8362; ');$('#paid-amount').val(50)">
                                        <h6 style="color:#2a80c7;">15 <?= __('days') ?></h6>
                                        <h2 class="font-weight-bold mt-3" style="color:#2a80c7;">50&#8362; </h2>
                                        <h6 style="color:#2a80c7;" class="mt-4"><?= __('Save') ?> 62%</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-1 mt-2">
                            <div class="row text-center justify-content-center">
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-primary w-100 btn-round rounded-pill mb-3" onclick="submitBumpPrice()"><?= __('Continue') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <a href="javascript: $('#bump-item').modal('hide');$('#promotio-work').modal();" class="btn btn-dark btn-round rounded-pill mb-3"><?= __('How does promoting work?') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="promotio-work" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('How Does It work') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12 my-4">
                            <div class="row">
                                <div class="col-md-3 text-right px-0">
                                    <?= $this->Html->image('help1.png') ?>
                                </div>
                                <div class="col-md-9 pl-0 mt-4">
                                    <h4 style="color: 2a80c7; font-size:15px;" class="font-weight-bold"><?= __('Get more views') ?></h4>
                                    <p style="font-size:15px;"><?= __('Promoted posts appear among the first spots buyer see and get an average of 14x views.') ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 my-4">
                            <div class="row">
                                <div class="col-md-3 text-right px-0">
                                    <?= $this->Html->image('help2.png') ?>
                                </div>
                                <div class="col-md-9 pl-0 mt-4">
                                    <h4 style="color: 2a80c7; font-size:15px;" class="font-weight-bold"><?= __('Add a promoted spot') ?></h4>
                                    <p style="font-size:15px;"><?= __('Your item appears a new post in Search results as well as in the Promoted spot.') ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 my-4">
                            <div class="row">
                                <div class="col-md-3 text-right px-3">
                                    <?= $this->Html->image('help3.png') ?>
                                </div>
                                <div class="col-md-9 pl-0 mt-4">
                                    <h4 style="color: 2a80c7; font-size:15px;" class="font-weight-bold"><?= __('Promoted spots are shared') ?></h4>
                                    <p style="font-size:15px;"><?= __('Don’t worry if you don’t see your Items at the top of the feed. Spots Are shared between sellers.') ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-4 text-center">
                            <a href="javascript: $('#promotio-work').modal('hide');$('#bump-item').modal();" class="btn btn-primary btn-round rounded-pill w-50"><?= __('Got it') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="bump-item-checkout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Bump an Item, Sell it faster') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-5">
                    <div class="row">
                        <div class="col-md-3 p-1">
                            <div class="card p-0" style="border-color: #2a80c7;background: #F0F1F3;border-radius: 15px; overflow: hidden;">
                                <div class="card-body p-0">
                                    <div class="row p-0">
                                        <div class="col-md-12 p-0" style="background: #E5E7EB">
                                            <?= $this->Html->image('img_featured.png', ['class' => 'position-absolute']) ?>
                                            <div class="product-image-preview" style="background-image: url('<?= ($data['images'][0]['image_url'] != '' ? $data['images'][0]['image_url'] : $emptyImage) ?>');"></div>
                                        </div>
                                        <div class="col-md-12 p-2" style="height: 95px;">
                                            <p class="font-weight-bold mb-0 text-dark" style="font-size: 15px;"><?= substr($data['product_title'], 0, 18) ?></p>
                                            <h6 class="mb-0" style="font-weight: bold;color:#1470d6;"><?= $data['product_price'] ?>&#8362; </h6>
                                            <p class="font-weight-bold mb-0 text-secondary" style="font-size: 10px;"><?= substr($data['product_description'], 0, 60); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-12 pl-0">
                                    <div class="shadow p-4 mb-5 bg-white rounded w-100 align-middle cursor-pointer">
                                        <h3 class="font-weight-bold text-secondary" style="font-size: 15px;"><?= __('Selected Package') ?></h3>
                                        <div class="card">
                                            <div class="row py-2">
                                                <div class="col-md-6">
                                                    <span class="font-weight-bold text-secondary" id="bump-days">7 days</span><br>
                                                    <span style="font-size: 13px;" class="text-secondary" id="bunmp-discount">Save 46%</span>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <span class="font-weight-bold text-secondary" id="bump-amount"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row py-2">
                                            <div class="col-md-6">
                                                <span class="text-secondary"><?= __('Service Fee') ?></span>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <span class="font-weight-bold text-secondary">10&#8362; </span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row py-2">
                                            <div class="col-md-6">
                                                <span style="color: #2a80c7;"><?= __('Total') ?></span>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <span style="color: #2a80c7;" class="font-weight-bold" id="bump-total">45&#8362; </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pl-0 pr-4">
                            <div class="shadow p-4 mb-5 rounded w-100 align-middle cursor-pointer" style="background: #E5E7EB;">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3 style="font-size: 15px;" class="font-weight-bold"><?= __('Current Payment Method') ?></h3>
                                        <p style="margin-bottom: 0px;font-size: 15px;"><?= $this->Html->image('cc.png', ['id' => 'payment-type-image', 'style' => 'height:30px']) ?> <span id="payment-type"><?= __('Credit Card') ?></span></p>
                                    </div>
                                    <div class="col-md-4 align-middle text-right mt-2">
                                        <h5 style="color: #2a80c7;" class="align-middle font-weight-bold"><?= __('Paying') ?> <span id="total-bump-amount"><span></span></span></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="font-size: 15px;" class="font-weight-bold"><?= __('Other Payment Method') ?></h3>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Html->image('cc.png', ['onclick' => "loadPayment('card')", 'style' => 'height: 60px;cursor: pointer;border-radius: 5px;0px 0px 10px 5px #0000004a', 'class' => 'cc-payment-method payment-method-type']) ?>
                            <?= $this->Html->image('paypal.png', ['onclick' => "loadPayment('paypal')", 'style' => 'height: 60px;cursor: pointer;border-radius: 5px;', 'class' => 'paypal-payment-method payment-method-type']) ?>
                            <?= $this->Html->image('gpay.png', ['onclick' => "loadPayment('gpay')", 'style' => 'height: 60px;cursor: pointer;border-radius: 5px;', 'class' => 'gpay-payment-method payment-method-type']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <a href="javascript: checkout();" class="btn btn-primary btn-round rounded-pill mb-3 px-5"><?= $this->Html->image('lock.png', ['style' => 'height: 20px; margin-top: -5px;']) ?><?= __('Secure Checkout') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="card-checkout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Enter New Card') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(null, ['id' => 'cardForm']) ?>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h6 class="text-secondary"><?= __("ENTER DETAILS FOR NEW CARD") ?></h6>
                        </div>
                        <div class="row">
                            <div class="col-12 mb-3">
                                <label for="cc-name"><?= __('Cardholder Name') ?></label>
                                <div class="form-control" id="cc-name"></div>
                                <small class="text-muted"><?= __('Full name as displayed on card') ?></small>
                                <div class="invalid-feedback">
                                    <?= __('Name on card is required') ?>
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="cc-number"><?= __("Credit card number") ?></label>
                                <div class="form-control" id="cc-number"></div>
                                <div class="invalid-feedback">
                                    <?= __('Credit card number is required') ?>
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <label for="cc-expiration"><?= __('Expiration') ?></label>
                                <div class="form-control" id="cc-expiration"></div>
                                <div class="invalid-feedback">
                                    <?= __('Expiration date required') ?>
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <label for="cc-expiration"><?= __('CVV') ?></label>
                                <div class="form-control" id="cc-cvv"></div>
                                <div class="invalid-feedback">
                                    <?= __('Security code required') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary btn-round rounded-pill mb-3 px-5"><?= $this->Html->image('lock.png', ['style' => 'height: 20px; margin-top: -5px;']) ?><?= __('Secure Checkout') ?></button>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="flag-seller" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Flag Seller') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-5">
                    <div class="row">
                        <div class="col-12">
                            <div class="row pl-3">
                                <div class="col-md-2 p-0">
                                    <?= $this->Html->image($data['uploaded_by_user_id']['image'] != '' ? $data['uploaded_by_user_id']['image'] : 'profile.png', ['class' => 'rounded-circle', 'style' => 'height:100px;width: 100px;']) ?>
                                </div>
                                <div class="col-md-10">
                                    <h5 style="font-weight: bold;color:#54585d;margin-top: 32px;"><?= $data['uploaded_by_user_id']['full_name'] ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 px-4 mt-3">
                            <h5 class="font-weight-bold"><?= __('Reason for reporting the seller?') ?></h5>
                            <div class="row text-center report-item">
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select21']) ?>
                                        <?= $this->Html->image('eye-glasses.png', ['select' => 'report-select21']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('They Missed our meeting') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select22']) ?>
                                        <?= $this->Html->image('Group 280.png', ['select' => 'report-select22']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Trouble at meetup') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select23']) ?>
                                        <?= $this->Html->image('unlike.png', ['select' => 'report-select23']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Rude or Inappropriate') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select24']) ?>
                                        <?= $this->Html->image('Group 2.png', ['select' => 'report-select24']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Problem with item') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select25']) ?>
                                        <?= $this->Html->image('Group 273.png', ['select' => 'report-select25']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Messaging Problem') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select26']) ?>
                                        <?= $this->Html->image('Line_2.png', ['select' => 'report-select26']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Made a lower offer') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select27']) ?>
                                        <?= $this->Html->image('Group 279.png', ['select' => 'report-select27']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('Shipping Problems') ?></p>
                                </div>
                                <div class="col-md-3 px-0">
                                    <div class="position-relative">
                                        <?= $this->Html->image('report_selected.png', ['style' => 'display:none;position:absolute', 'id' => 'report-select28']) ?>
                                        <?= $this->Html->image('Group 8.png', ['select' => 'report-select28']) ?>
                                    </div>
                                    <p class="font-weight-bold" style="font-size: 14px;"><?= __('None of the above') ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 px-4 mt-3">
                            <?= $this->Form->control('flag_note', ['type' => 'textarea', 'rows' => '5', 'label' => false, 'placeholder' => __('Note'), 'style' => 'background: #E5E7EB; border-radius: 20px;']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center border-0 mb-4">
                <button type="button" class="btn btn-primary btn-round rounded-pill w-25 m-auto" id="submit-flag-form"><?= __('Report User') ?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="thank-you-flag-seller" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h5 class="modal-title w-100 text-center font-weight-bold" id="exampleModalLabel"><?= __('Flag Seller') ?></h5>
                <button type="button" class="close p-0 m-0" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 32px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container px-5">
                    <div class="row text-center">
                        <div class="col-12">
                            <?= $this->Html->image('report_selected.png') ?>
                            <p class="mt-3" style="font-size: 15px;"><?= __('Thank you for reporting.') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function initialize() {
        const center = {
            lat: <?= $data['product_lat'] ?>,
            lng: <?= $data['product_lng'] ?>
        };
        // Create the map.
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: center,
            maxZoom: 14,
            mapTypeId: "terrain",
        });
        // Add the circle for this city to the map.
        const cityCircle = new google.maps.Circle({
            strokeColor: "#2a80c7",
            strokeOpacity: 0.7,
            strokeWeight: 2,
            fillColor: "#2a80c7",
            fillOpacity: 0.7,
            map,
            center: center,
            radius: 1000,
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    jQuery(document).ready(function() {
        jQuery('.item img').on('click', function() {
            jQuery('#image-review').css('background-image', "url('" + jQuery(this).attr('src') + "')");
            jQuery('#preview-image-blur').css('background-image', "url('" + jQuery(this).attr('src') + "')");
        });
    });
</script>
<script>
    $(document).ready(function() {
        var select_report = false;
        var select_flag = false;
        if ($("#desc-text").prop('scrollHeight') <= $("#desc-text").height()) {
            $('#show-more').hide();
        }
        $('#show-more').on('click', function() {
            if ($(this).html() == 'Read More…') {
                $('#desc-text').css('max-height', 'unset');
                $(this).css('color', 'red');
                $(this).html('Read Less');
            } else {
                $(this).html('Read More…');
                $('#desc-text').css('max-height', '200px');
                $(this).css('color', '#007bff');
            }
        });
        $('img[select]').on('click', function() {
            $('[id^="report-select"]').hide();
            select_report = $(this).attr('select').replace(/[^\d.]/g, '');
            select_flag = $(this).attr('select').replace(/[^\d.]/g, '');
            $('#' + $(this).attr('select')).show();
        });
        $('#submit-report-form').on('click', function() {
            if (select_report == false) {
                alert('Please select report reason.');
                return
            }
            select_report = false;
            $('#note').val('');
            $('[id^="report-select"]').hide();
            $('#report-listing').modal('hide');
            $('#thank-you-report-listing').modal();
        });
        $('#submit-flag-form').on('click', function() {
            if (select_flag == false) {
                alert('Please select report reason.');
                return
            }
            select_flag = false;
            $('#flag-note').val('');
            $('[id^="flag-select"]').hide();
            $('#flag-seller').modal('hide');
            $('#thank-you-flag-seller').modal();
        });
        $('#preview-image-blur').on('click', function() {
            var imgUrl = $('#image-review').css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");
            $.fancybox.open([{
                src: imgUrl
            }], {
                loop: false
            });
        });
    });

    var currentPrice = {
        amount: '35&#8362; ',
        days: '7 days',
        discount: 'Save 46%',
        total: '45&#8362; '
    };

    function loadBumpSeller(ev, amount, days, discount, total) {
        $('.amount-bump').removeClass('active');
        $(ev).addClass('active');
        currentPrice.amount = amount;
        currentPrice.days = days;
        currentPrice.discount = discount
        currentPrice.total = total;
    }

    function submitBumpPrice() {
        $('#bump-amount').html(currentPrice.amount);
        $('#bump-total').html(currentPrice.total);
        $('#total-bump-amount').html(currentPrice.total);
        $('#bump-discount').html(currentPrice.discount);
        $('#bump-days').html(currentPrice.days);
        $('#bump-item').modal('hide');
        setTimeout(function() {
            $('#bump-item-checkout').modal();
        }, 600);
    }

    function loadPayment(type) {
        $('.payment-method-type').css('box-shadow', 'unset');
        switch (type) {
            case 'card':
                $('#payment-method').val('card');
                $('#payment-type').text('Credit Card');
                $('#payment-type-image').attr('src', '/images/cc.png');
                $('.cc-payment-method').css('box-shadow', '0px 0px 10px 5px #0000004a');
                break;
            case 'paypal':
                $('#payment-method').val('paypal');
                $('#payment-type').text('Paypal');
                $('#payment-type-image').attr('src', '/images/paypal.png');
                $('.paypal-payment-method').css('box-shadow', '0px 0px 10px 5px #0000004a');
                break;
            case 'gpay':
                $('#payment-method').val('gpay');
                $('#payment-type').text('Gpay');
                $('#payment-type-image').attr('src', '/images/gpay.png');
                $('.gpay-payment-method').css('box-shadow', '0px 0px 10px 5px #0000004a');
                break;
            default:
        }
    }
    const CLIENT_AUTHORIZATION = 'sandbox_4x5zhgmj_8qvhbp66n65srzv5';

    function checkout() {
        var type = $('#payment-method').val();
        switch (type) {
            case 'paypal':
                window.location.href = '/payments/paypal/' + $('#paid-amount').val() + '/<?= $data['id'] ?>';
                break;
            case 'card':
                $('#bump-item-checkout').modal('hide');
                $('#card-checkout').modal();
                $('#card-amount').val($('#paid-amount').val());
                var form = $('#cardForm');

                braintree.client.create({
                    authorization: CLIENT_AUTHORIZATION
                }, function(err, clientInstance) {
                    if (err) {
                        console.error(err);
                        return;
                    }

                    braintree.hostedFields.create({
                        client: clientInstance,
                        styles: {
                            input: {
                                'font-size': '1rem',
                                color: '#495057'
                            }
                        },
                        fields: {
                            cardholderName: {
                                selector: '#cc-name',
                                placeholder: 'Name as it appears on your card'
                            },
                            number: {
                                selector: '#cc-number',
                                placeholder: '4111 1111 1111 1111'
                            },
                            cvv: {
                                selector: '#cc-cvv',
                                placeholder: '123'
                            },
                            expirationDate: {
                                selector: '#cc-expiration',
                                placeholder: 'MM / YY'
                            }
                        }
                    }, function(err, hostedFieldsInstance) {
                        if (err) {
                            console.error(err);
                            return;
                        }

                        function createInputChangeEventListener(element) {
                            return function() {
                                validateInput(element);
                            }
                        }

                        function setValidityClasses(element, validity) {
                            if (validity) {
                                element.removeClass('is-invalid');
                                element.addClass('is-valid');
                            } else {
                                element.addClass('is-invalid');
                                element.removeClass('is-valid');
                            }
                        }

                        function validateInput(element) {
                            // very basic validation, if the
                            // fields are empty, mark them
                            // as invalid, if not, mark them
                            // as valid

                            if (!element.val().trim()) {
                                setValidityClasses(element, false);

                                return false;
                            }

                            setValidityClasses(element, true);

                            return true;
                        }

                        var ccName = $('#cc-name');

                        ccName.on('change', function() {
                            validateInput(ccName);
                        });


                        hostedFieldsInstance.on('validityChange', function(event) {
                            var field = event.fields[event.emittedBy];

                            // Remove any previously applied error or warning classes
                            $(field.container).removeClass('is-valid');
                            $(field.container).removeClass('is-invalid');

                            if (field.isValid) {
                                $(field.container).addClass('is-valid');
                            } else if (field.isPotentiallyValid) {
                                // skip adding classes if the field is
                                // not valid, but is potentially valid
                            } else {
                                $(field.container).addClass('is-invalid');
                            }
                        });

                        hostedFieldsInstance.on('cardTypeChange', function(event) {
                            var cardBrand = $('#card-brand');
                            var cvvLabel = $('[for="cc-cvv"]');

                            if (event.cards.length === 1) {
                                var card = event.cards[0];

                                // change pay button to specify the type of card
                                // being used
                                cardBrand.text(card.niceType);
                                // update the security code label
                                cvvLabel.text(card.code.name);
                            } else {
                                // reset to defaults
                                cardBrand.text('Card');
                                cvvLabel.text('CVV');
                            }
                        });

                        form.submit(function(event) {
                            event.preventDefault();

                            var formIsInvalid = false;
                            var state = hostedFieldsInstance.getState();

                            // Loop through the Hosted Fields and check
                            // for validity, apply the is-invalid class
                            // to the field container if invalid
                            Object.keys(state.fields).forEach(function(field) {
                                if (!state.fields[field].isValid) {
                                    $(state.fields[field].container).addClass('is-invalid');
                                    formIsInvalid = true;
                                }
                            });

                            if (formIsInvalid) {
                                // skip tokenization request if any fields are invalid
                                return;
                            }

                            hostedFieldsInstance.tokenize(function(err, payload) {
                                if (err) {
                                    console.error(err);
                                    return;
                                }
                                window.location.href = '/card/?amount=' + $('#paid-amount').val() + '&payerID=' + payload.nonce + '&productId=<?= $data['id'] ?>';
                            });
                        });
                    });
                });
                break;
            case 'gpay':
                // Make sure to have https://pay.google.com/gp/p/js/pay.js loaded on your page

                // You will need a button element on your page styled according to Google's brand guidelines
                // https://developers.google.com/pay/api/web/guides/brand-guidelines
                var paymentsClient = new google.payments.api.PaymentsClient({
                    environment: 'TEST' // Or 'PRODUCTION'
                });

                braintree.client.create({
                    authorization: CLIENT_AUTHORIZATION
                }, function(clientErr, clientInstance) {
                    braintree.googlePayment.create({
                        client: clientInstance,
                        googlePayVersion: 2,
                        googleMerchantId: 'merchant-id-from-google' // Optional in sandbox; if set in sandbox, this value must be a valid production Google Merchant ID
                    }, function(googlePaymentErr, googlePaymentInstance) {
                        paymentsClient.isReadyToPay({
                            // see https://developers.google.com/pay/api/web/reference/object#IsReadyToPayRequest
                            apiVersion: 2,
                            apiVersionMinor: 0,
                            allowedPaymentMethods: googlePaymentInstance.createPaymentDataRequest().allowedPaymentMethods,
                            existingPaymentMethodRequired: true // Optional
                        }).then(function(response) {
                            if (response.result) {
                                var paymentDataRequest = googlePaymentInstance.createPaymentDataRequest({
                                    transactionInfo: {
                                        currencyCode: 'ILS',
                                        totalPriceStatus: 'FINAL',
                                        totalPrice: $('#paid-amount').val() // Your amount
                                    }
                                });

                                // We recommend collecting billing address information, at minimum
                                // billing postal code, and passing that billing postal code with all
                                // Google Pay card transactions as a best practice.
                                // See all available options at https://developers.google.com/pay/api/web/reference/object
                                var cardPaymentMethod = paymentDataRequest.allowedPaymentMethods[0];
                                cardPaymentMethod.parameters.billingAddressRequired = true;
                                cardPaymentMethod.parameters.billingAddressParameters = {
                                    format: 'FULL',
                                    phoneNumberRequired: true
                                };

                                paymentsClient.loadPaymentData(paymentDataRequest).then(function(paymentData) {
                                    googlePaymentInstance.parseResponse(paymentData, function(err, result) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        window.location.href = '/gpay/?amount=' + $('#paid-amount').val() + '&payerID=' + result.nonce + '&productId=<?= $data['id'] ?>';

                                        // Send result.nonce to your server
                                        // result.type may be either "AndroidPayCard" or "PayPalAccount", and
                                        // paymentData will contain the billingAddress for card payments
                                    });
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }
                        }).catch(function(err) {
                            // Handle errors
                        });
                    });

                    // Set up other Braintree components
                });
                break;
        }
    }
</script>
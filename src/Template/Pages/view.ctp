<?php
$this->assign('bodyClass', 'detail-page')
?>
<style>
    .page-content p {
        color: #121212;
        line-height: 1.5;
        font-size: 16px;
    }
</style>
<div class="pagination">
    <div class="container">
        <ul>
            <li><a href="/"><?= __('Home') ?></a></li>
            <li class="active"><a href="#"><?= $page->page_name ?></a></li>
        </ul>
    </div>
</div>
<div class="details">
    <div class="container small" style="max-width: 1280px;margin:auto">
        <div class="row" style="max-width: 66.666667%;margin:auto">
            <h2 style="width: 100%;font-weight: 900;"><?= $page->page_name ?></h2>
            <h6 style="margin-top: 20px;"><?= __('Last updated') ?>: <?= $page->created ?></h6>
            <div class="page-content" style="margin-top: 20px;">
                <?= $page->text ?>
            </div>
        </div>
    </div>
</div>
<?php

namespace App\View\Helper;

use Cake\View\Helper\HtmlHelper;

class MyHtmlHelper extends HtmlHelper {

    public function component($path, $type = 'css', array $options = array()) {
        $path = '/component/' . $path;
        return parent::{$type}($path, $options);
    }

    public function image($path, array $options = array()) {
        // $options['onerror'] = "this.onerror=null;this.src='/images/not-found.png';";
        if (is_string($path)) {
            $path = $this->Url->image($path, $options);
        } else {
            $path = $this->Url->build($path, $options);
        }
        if (isset($options['lazy']) && $options['lazy']) {
            $options['data-src'] = $path;
            $path = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCAAKAAoDASIAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAf/xAAUEAEAAAAAAAAAAAAAAAAAAAAA/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwC+gCv/2Q==';
        }
        return parent::image($path, $options);
    }
}

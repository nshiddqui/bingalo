<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NotficationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NotficationsTable Test Case
 */
class NotficationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\NotficationsTable
     */
    public $Notfications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Notfications',
        'app.Products',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Notfications') ? [] : ['className' => NotficationsTable::class];
        $this->Notfications = TableRegistry::getTableLocator()->get('Notfications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Notfications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportedContentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportedContentTable Test Case
 */
class ReportedContentTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReportedContentTable
     */
    public $ReportedContent;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ReportedContent',
        'app.Posts',
        'app.ReportedUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ReportedContent') ? [] : ['className' => ReportedContentTable::class];
        $this->ReportedContent = TableRegistry::getTableLocator()->get('ReportedContent', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportedContent);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SponsoredLocationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SponsoredLocationsTable Test Case
 */
class SponsoredLocationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SponsoredLocationsTable
     */
    public $SponsoredLocations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SponsoredLocations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SponsoredLocations') ? [] : ['className' => SponsoredLocationsTable::class];
        $this->SponsoredLocations = TableRegistry::getTableLocator()->get('SponsoredLocations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SponsoredLocations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TopCitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TopCitiesTable Test Case
 */
class TopCitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TopCitiesTable
     */
    public $TopCities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TopCities',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TopCities') ? [] : ['className' => TopCitiesTable::class];
        $this->TopCities = TableRegistry::getTableLocator()->get('TopCities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TopCities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

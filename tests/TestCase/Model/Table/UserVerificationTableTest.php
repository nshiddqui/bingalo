<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserVerificationTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserVerificationTable Test Case
 */
class UserVerificationTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserVerificationTable
     */
    public $UserVerification;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserVerification',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserVerification') ? [] : ['className' => UserVerificationTable::class];
        $this->UserVerification = TableRegistry::getTableLocator()->get('UserVerification', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserVerification);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

$(document).ready(function() {

    //MENU START
    $('.mobile-menu-button').on('click', function() {
        $('.menu-content').removeClass('hide-menu');
    });
    $('.menu-content .close-btn').on('click', function() {
        $('.menu-content').addClass('hide-menu');
    });
    //MENU END

    //LOGIN START
    $('.account-form .login-btn').on('click', function() {
        $('.signup-popup').addClass('hide-popup-box');
        $('.login-popup').removeClass('hide-popup-box');
    });
    $('.login-popup .close-btn').on('click', function() {
        $('.login-popup').addClass('hide-popup-box');
    });
    //LOGIN END

    //SIGNUP START
    $('.account-form .signup-btn').on('click', function() {
        $('.login-popup').addClass('hide-popup-box');
        $('.signup-popup').removeClass('hide-popup-box');
    });
    $('.signup-popup .close-btn').on('click', function() {
        $('.signup-popup').addClass('hide-popup-box');
    });
    //SIGNUP END

   
    //FILTER START
    $('.filter-container').on('click', function() {
        $('.filter-content').removeClass('hide-popup-box');
    });
    $('.filter-content .close-filter').on('click', function() {
        $('.filter-content').addClass('hide-popup-box');
    });
    //FILTER END
    
    //LOCAION START
    $('.location-box').on('click', function() {
        $('.location-content').removeClass('hide-popup-box');
    });
    $('.location-content .close-location').on('click', function() {
        $('.location-content').addClass('hide-popup-box');
    });
    //LOCATION END

    //NOTIFICATION
    $('.app-install .close-btn').on('click', function() {
        $('.site-notification').slideToggle();
    });
});

$(document).ready(function(){
    $('.slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
});
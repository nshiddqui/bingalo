function resizeGridItem(item) {
    grid = document.getElementsByClassName("grid")[0];
    if (grid && item.querySelector('.content')) {
        rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
        rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
        rowSpan = Math.ceil((item.querySelector('.content').getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));
        item.style.gridRowEnd = "span " + rowSpan;
    }
}

function resizeAllGridItems() {
    allItems = document.getElementsByClassName("item");
    for (x = 0; x < allItems.length; x++) {
        resizeGridItem(allItems[x]);
    }
}

function resizeInstance(instance) {
    item = instance.elements[0];
    resizeGridItem(item);
}

window.onload = resizeAllGridItems();
window.addEventListener("resize", resizeAllGridItems);
if ($.fn.lazy) {
    $('[lazy]').lazy({
        placeholder: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==',
        effect: "fadeIn",
        effectTime: 2000,
        threshold: 0,
        afterLoad: function(element) {
            if (element.attr('parent')) {
                var parentData = element.attr('parent');
                if (parentData == 'img') {
                    element.parent(parentData).attr('src', element.css('background-image'));
                } else {
                    element.parent(parentData).css('background-image', element.css('background-image'));
                }
                element.remove();
            }
            resizeAllGridItems();
        },
        onFinishedAll: function() {
            resizeAllGridItems();
        }
    });
} else {
    $(document).ready(function() {
        setTimeout(() => {
            $('[lazy]').each(function() {
                var tagName = $(this).prop("tagName").toLowerCase();
                if (tagName === 'img') {
                    $(this).attr('src', $(this).attr('data-src'));
                } else {
                    $(this).css('background-image', 'url(' + $(this).attr('data-src') + ')');
                }
                $(this).removeAttr('data-src');
            });
        }, 100);
    });

}


// Popup
$('[login]').click(function(ev) {
    if (!IS_LOGIN) {
        ev.preventDefault();
        $('.login-popup:not(.language-popup,.download-app)').addClass('visible');
        $('body').css('overflow-y', 'hidden');
        return false;
    }
});

$('[download-app]').click(function(ev) {
    ev.preventDefault();
    $('.download-app').addClass('visible');
    return false;
});

$('#registerd-modal').on('click', function() {
    $('.login-popup').removeClass('visible');
    $('body').css('overflow-y', 'auto');
    $('#signup-form').modal();
});
$('#login-model').on('click', function() {
    $('.login-popup:not(.language-popup,.download-app)').addClass('visible');
    $('body').css('overflow-y', 'hidden');
    $('#signup-form').modal('hide');
});

$('#back-to-login').on('click', function() {
    $('.login-popup:not(.language-popup,.download-app)').addClass('visible');
    $('body').css('overflow-y', 'hidden');
    $('#forgot-form').modal('hide');
});

$('#forgot-login').on('click', function() {
    $('.login-popup').removeClass('visible');
    $('body').css('overflow-y', 'auto');
    $('#forgot-form').modal();
});

$('#update-profile').on('click', function(ev) {
    ev.preventDefault();
    $('.login-popup:not(.language-popup,.download-app)').addClass('visible');
    $('body').css('overflow-y', 'hidden');
});

if ($('.login-popup').hasClass('visible')) {
    $('body').css('overflow-y', 'hidden');
} else {
    $('body').css('overflow-y', 'auto');
}

$('.login-popup .overlay, .login-popup .close').click(function() {
    $('.login-popup').removeClass('visible');
    $('body').css('overflow-y', 'auto');
});


$('a.sign-up').click(function() {

    $('.sign-in-up a').removeClass('active');
    $(this).addClass('active');
    $('.signup-form').addClass('show');
    $('.signin-form').removeClass('show');
});

$('a.sign-in').click(function() {
    $('.sign-in-up a').removeClass('active');
    $(this).addClass('active');
    $('.signin-form').addClass('show');
    $('.signup-form').removeClass('show');
});
var nextPage = 2;

function loadMoreItem(ev) {
    $(ev).attr('onclick', '');
    var url = new URL(window.location.href);
    url.searchParams.set('page', nextPage);
    nextPage = nextPage + 1;
    window.history.pushState("", "", url);
    $.get(url, function(response) {
        $(ev).attr('onclick', 'loadMoreItem(this)');
        $('#loadMoreItem').append(response);
        allItems = document.getElementsByClassName("item");
        for (x = 0; x < allItems.length; x++) {
            imagesLoaded(allItems[x], resizeInstance);
        }
    });
}

function loadSubCat(id) {
    if ($(id).css('display') == 'none') {
        $('.sub-categories').hide();
        $(id).css('display', 'contents');
    } else {
        $(id).css('display', 'none');
    }
}

$(document).ready(function() {
    if (localStorage.getItem("language") === null && window.location.pathname == '/') {
        $('.login-popup.language-popup').addClass('visible');
    }

    if ($(window).width() > 992) {
        $('header').addClass("fixed-top");
        $('body').css('margin-top', '93px');
    }
    var $inputAutocomplete = $("#searchItem").autocomplete({
        source: '/search-keyword',
        minLength: 0,
        response: function(event, ui) {
            if (RECENT_ITEMS.length > 0) {
                if (lang == 'en') {
                    ui.content.push({
                        'label': 'RECENT SEARCHES',
                        'value': ''
                    });
                } else {
                    ui.content.push({
                        'label': 'חיפש לאחרונה',
                        'value': ''
                    });
                }
                $.each(RECENT_ITEMS, function(k, recent_item) {
                    ui.content.push({
                        'label': recent_item,
                        'value': recent_item
                    });
                });
            }
            if (TRENDING_ITEMS.length > 0) {
                if (lang == 'en') {
                    ui.content.push({
                        'label': 'TRENDING SEARCHES',
                        'value': ''
                    });
                } else {
                    ui.content.push({
                        'label': 'חיפושים פופולריים',
                        'value': ''
                    });
                }
                $.each(TRENDING_ITEMS, function(k, trending_item) {
                    ui.content.push({
                        'label': trending_item,
                        'value': trending_item
                    });
                });
            }
        },
        select: function(event, ui) {
            window.location.href = '/?name=' + ui.item.label;
        }
    });
    $inputAutocomplete.data("ui-autocomplete")._renderItem = function(ul, item) {
        if (item.value == '') {
            return $('<li class="ui-state-disabled"><div>' + item.label + '</div></li>').appendTo(ul);
        } else {
            return $("<li>")
                .append(`<div>${item.label}</div>`)
                .appendTo(ul);
        }
    };

    $inputAutocomplete.on('focus', function() {
        $(this).keydown();
    });
});

$('[date-sortByRedirect]').on('change', function() {
    var queryType = $(this).attr('search-type');
    var url = new URL(window.location.href);
    url.searchParams.set(queryType, $(this).val());
    window.history.pushState("", "", url);
    window.location.href = url;
});
$('.sortByAjax').on('blur', function() {
    var queryType = $(this).attr('search-type');
    var url = new URL(window.location.href);
    url.searchParams.set(queryType, $(this).val());
    window.history.pushState("", "", url);
    $.get(url, function(response) {
        $('#loadMoreItem').html(response);
        allItems = document.getElementsByClassName("item");
        for (x = 0; x < allItems.length; x++) {
            imagesLoaded(allItems[x], resizeInstance);
        }
    });
});

$('.choose-language').on('click', function() {
    localStorage.setItem('language', $(this).html());
    $('.close').click();
});
$('#language-change').on('click', function(ev) {
    ev.preventDefault();
    $('.login-popup.language-popup').addClass('visible');
});
$('.chat-screen .chat .right .right-nav a').click(function() {
    $('.pop-edit').toggleClass('active');
});
$('#click-profile-dropdown, #pop-dropdown-home li').click(function() {
    $('#pop-dropdown-home').toggleClass('active');
});

$('#show-password').on('click', function() {
    var current = $(this).attr('current');
    var lang = $(this).attr('lang');
    if (current == 'show') {
        if (lang == 'en') {
            $(this).html('<img src="/images/visible.png" style="height:12px"> Hide Password');
        } else {
            $(this).html('<img src="/images/visible.png" style="height:12px"> הסתר סיסמא');
        }
        $(this).attr('current', 'hide');
        $('#loginForm #password').attr('type', 'text');
    } else {
        if (lang == 'en') {
            $(this).html('<img src="/images/invisible.png" style="height:12px"> Show Password');
        } else {
            $(this).html('<img src="/images/invisible.png" style="height:12px"> הראה סיסמה');
        }
        $(this).attr('current', 'show');
        $('#loginForm #password').attr('type', 'password');
    }
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#image-preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}

$("#image").change(function() {
    readURL(this);
});

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

if ("geolocation" in navigator) { //check geolocation available 
    //try to get user current location using getCurrentPosition() method
    navigator.geolocation.getCurrentPosition(function(position) {
        var latlng = position.coords.latitude + "," + position.coords.longitude;
        var cokie = getCookie('Location');
        if ((cokie == '') || (cokie != latlng)) {
            setCookie('Location', latlng, 365);
            if (cokie == '') {
                window.location.reload();
            }
        }
    }, function(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }, {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    });
} else {
    console.log("Browser doesn't support geolocation!");
}

window.onload = function() {
    var txtPassword = document.getElementById("new-password");
    var txtConfirmPassword = document.getElementById("confirm-password");
    if (txtPassword) {
        txtPassword.onchange = ConfirmPassword;
        txtConfirmPassword.onkeyup = ConfirmPassword;

        function ConfirmPassword() {
            txtConfirmPassword.setCustomValidity("");
            if (txtPassword.value != txtConfirmPassword.value) {
                txtConfirmPassword.setCustomValidity("Passwords do not match.");
            }
        }
    }
}

$('#form-change-email').on('submit', function(ev) {
    ev.preventDefault();
    $.ajax({
        url: '/authorized/send-o-t-p/' + $('#change-email-id').val(),
        type: 'GET',
        success: function(res) {
            if (res == '0') {
                alertify
                    .alert('Error', 'Email already exists.')
            } else {
                $('#form-change-email').hide();
                $('#otp-change-email').show();
            }
        }
    });
});

var formSingupSubmit = false;

$('.signup-form.show form').on('submit', function() {
    if (formSingupSubmit === false) {
        var val = $('.signup-form.show #email').val();
        if (val !== '') {
            $.get('/Pages/checkEmail/' + val, function(res) {
                if (res == '1') {
                    formSingupSubmit = true;
                    $('.signup-form.show form').submit();
                } else {
                    alert("Please user another email address, this email already exists in our database.");
                }
            });
        }
        return false;
    }
    return true;
});